// To parse this JSON data, do
//
//     final productsFillter = productsFillterFromJson(jsonString);

import 'dart:convert';

ProductsFillter productsFillterFromJson(String str) =>
    ProductsFillter.fromJson(json.decode(str));

String productsFillterToJson(ProductsFillter data) =>
    json.encode(data.toJson());

class ProductsFillter {
  ProductsFillter({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  List<Products> data;
  List<Error> error;

  factory ProductsFillter.fromJson(Map<String, dynamic> json) =>
      ProductsFillter(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null
            ? null
            : List<Products>.from(json["data"].map((x) => Products.fromJson(x))),
        error: json["error"] == null ? null : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
      };
}

class Products {
  Products({
    this.id,
    this.name,
    this.details,
    this.photos,
    this.price,
    this.available,
    this.shopId,
    this.shop,
    this.categoryId,
    this.category,
    this.createdAt,
  });

  int id;
  String name;
  String details;
  List<Photo> photos;
  String price;
  int available;
  int shopId;
  String shop;
  int categoryId;
  String category;
  DateTime createdAt;

  factory Products.fromJson(Map<String, dynamic> json) => Products(
        id: json["id"],
        name: json["name"],
        details: json["details"],
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        price: json["price"],
        available: json["available"],
        shopId: json["shop_id"],
        shop: json["shop"],
        categoryId: json["category_id"],
        category: json["category"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "details": details,
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "price": price,
        "available": available,
        "shop_id": shopId,
        "shop": shop,
        "category_id": categoryId,
        "category": category,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Photo {
  Photo({
    this.id,
    this.productId,
    this.photo,
    this.createdAt,
  });

  int id;
  int productId;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        productId: json["product_id"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "product_id": productId,
        "photo": photo,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
