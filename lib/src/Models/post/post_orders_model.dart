// To parse this JSON data, do
//
//     final postOrdersModel = postOrdersModelFromJson(jsonString);

import 'dart:convert';

PostOrdersModel postOrdersModelFromJson(String str) =>
    PostOrdersModel.fromJson(json.decode(str));

String postOrdersModelToJson(PostOrdersModel data) =>
    json.encode(data.toJson());

class PostOrdersModel {
  PostOrdersModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  Data data;
  final List<Error> error;

  factory PostOrdersModel.fromJson(Map<String, dynamic> json) =>
      PostOrdersModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
      };
}

class Data {
  Data({
    this.id,
    this.userId,
    this.user,
    this.userPhone,
    this.userPhoto,
    this.driverId,
    this.driver,
    this.driverPhone,
    this.driverPhoto,
    this.carType,
    this.shopId,
    this.shop,
    this.shopPhone,
    this.shopPhoto,
    this.orderDetails,
    this.addressDetails,
    this.orderLatitude,
    this.orderLongitude,
    this.latitude,
    this.longitude,
    this.price,
    this.orderPrice,
    this.totalPrice,
    this.status,
    this.paid,
    this.productsCart,
    this.createdAt,
  });

  int id;
  int userId;
  String user;
  String userPhone;
  String userPhoto;
  dynamic driverId;
  dynamic driver;
  dynamic driverPhone;
  dynamic driverPhoto;
  dynamic carType;
  int shopId;
  String shop;
  String shopPhone;
  String shopPhoto;
  String orderDetails;
  String addressDetails;
  String orderLatitude;
  String orderLongitude;
  String latitude;
  String longitude;
  dynamic price;
  String orderPrice;
  String totalPrice;
  int status;
  dynamic paid;
  List<ProductsCart> productsCart;
  DateTime createdAt;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        userId: json["user_id"],
        user: json["user"],
        userPhone: json["user_phone"],
        userPhoto: json["user_photo"],
        driverId: json["driver_id"],
        driver: json["driver"],
        driverPhone: json["driver_phone"],
        driverPhoto: json["driver_photo"],
        carType: json["car_type"],
        shopId: json["shop_id"],
        shop: json["shop"],
        shopPhone: json["shop_phone"],
        shopPhoto: json["shop_photo"],
        orderDetails: json["order_details"],
        addressDetails: json["address_details"],
        orderLatitude: json["order_latitude"],
        orderLongitude: json["order_longitude"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        price: json["price"],
        orderPrice: json["order_price"],
        totalPrice: json["total_price"],
        status: json["status"],
        paid: json["paid"],
        productsCart: List<ProductsCart>.from(
            json["products_cart"].map((x) => ProductsCart.fromJson(x))),
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": user,
        "user_phone": userPhone,
        "user_photo": userPhoto,
        "driver_id": driverId,
        "driver": driver,
        "driver_phone": driverPhone,
        "driver_photo": driverPhoto,
        "car_type": carType,
        "shop_id": shopId,
        "shop": shop,
        "shop_phone": shopPhone,
        "shop_photo": shopPhoto,
        "order_details": orderDetails,
        "address_details": addressDetails,
        "order_latitude": orderLatitude,
        "order_longitude": orderLongitude,
        "latitude": latitude,
        "longitude": longitude,
        "price": price,
        "order_price": orderPrice,
        "total_price": totalPrice,
        "status": status,
        "paid": paid,
        "products_cart":
            List<dynamic>.from(productsCart.map((x) => x.toJson())),
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class ProductsCart {
  ProductsCart({
    this.id,
    this.productId,
    this.productName,
    this.photos,
    this.userId,
    this.userName,
    this.shopId,
    this.shopName,
    this.orderId,
    this.price,
    this.quantity,
    this.state,
    this.createdAt,
  });

  int id;
  int productId;
  String productName;
  List<Photo> photos;
  int userId;
  String userName;
  int shopId;
  String shopName;
  int orderId;
  String price;
  int quantity;
  String state;
  DateTime createdAt;

  factory ProductsCart.fromJson(Map<String, dynamic> json) => ProductsCart(
        id: json["id"],
        productId: json["product_id"],
        productName: json["product_name"],
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        userId: json["user_id"],
        userName: json["user_name"],
        shopId: json["shop_id"],
        shopName: json["shop_name"],
        orderId: json["order_id"],
        price: json["price"],
        quantity: json["quantity"],
        state: json["state"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "product_id": productId,
        "product_name": productName,
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "user_id": userId,
        "user_name": userName,
        "shop_id": shopId,
        "shop_name": shopName,
        "order_id": orderId,
        "price": price,
        "quantity": quantity,
        "state": state,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Photo {
  Photo({
    this.id,
    this.productId,
    this.photo,
    this.createdAt,
  });

  int id;
  int productId;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        productId: json["product_id"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "product_id": productId,
        "photo": photo,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
