// To parse this JSON data, do
//
//     final postServiceModel = postServiceModelFromJson(jsonString);

import 'dart:convert';

PostServiceModel postServiceModelFromJson(String str) =>
    PostServiceModel.fromJson(json.decode(str));

String postServiceModelToJson(PostServiceModel data) =>
    json.encode(data.toJson());

class PostServiceModel {
  PostServiceModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  List<Datum> data;
  List<Error> error;

  factory PostServiceModel.fromJson(Map<String, dynamic> json) =>
      PostServiceModel(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
      };
}

class Datum {
  Datum({
    this.id,
    this.userId,
    this.user,
    this.name,
    this.details,
    this.phoneNumber,
    this.photo,
    this.longitude,
    this.latitude,
    this.state,
    this.distance,
    this.serviceCategoryId,
    this.serviceCategory,
    this.createdAt,
  });

  int id;
  int userId;
  String user;
  String name;
  String details;
  String phoneNumber;
  String photo;
  String longitude;
  String latitude;
  int state;
  double distance;
  int serviceCategoryId;
  String serviceCategory;
  DateTime createdAt;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        user: json["user"] == null ? null : json["user"],
        name: json["name"] == null ? null : json["name"],
        details: json["details"] == null ? null : json["details"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        photo: json["photo"] == null ? null : json["photo"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        state: json["state"] == null ? null : json["state"],
        serviceCategoryId: json["service_category_id"] == null
            ? null
            : json["service_category_id"],
        serviceCategory:
            json["service_category"] == null ? null : json["service_category"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "user": user == null ? null : user,
        "name": name == null ? null : name,
        "details": details == null ? null : details,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "photo": photo == null ? null : photo,
        "longitude": longitude == null ? null : longitude,
        "latitude": latitude == null ? null : latitude,
        "state": state == null ? null : state,
        "service_category_id":
            serviceCategoryId == null ? null : serviceCategoryId,
        "service_category": serviceCategory == null ? null : serviceCategory,
        "created_at": createdAt == null
            ? null
            : "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
