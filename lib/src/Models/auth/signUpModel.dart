// To parse this JSON data, do
//
//     final signUpModel = signUpModelFromJson(jsonString);

import 'dart:convert';

SignUpModel signUpModelFromJson(String str) =>
    SignUpModel.fromJson(json.decode(str));

String signUpModelToJson(SignUpModel data) => json.encode(data.toJson());

class SignUpModel {
  SignUpModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  Data data;
  List<Error> error;

  factory SignUpModel.fromJson(Map<String, dynamic> json) => SignUpModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
      };
}

class Data {
  Data({
    this.id,
    this.name,
    this.phoneNumber,
    this.countryCode,
    this.photo,
    this.photos,
    this.active,
    this.type,
    this.longitude,
    this.latitude,
    this.address,
    this.cityId,
    this.city,
    this.regionId,
    this.region,
    this.departmentId,
    this.department,
    this.productsNumber,
    this.viewCount,
    this.driverPrice,
    this.commissionStatus,
    this.apiToken,
    this.createdAt,
    this.carForm,
    this.identity,
    this.license,
    this.details,
    this.favorite,
    this.closeTime,
    this.openTime,
    this.carType,
  });

  int id;
  String name;
  String phoneNumber;
  int countryCode;
  String photo;
  dynamic photos;
  int active;
  int type;
  String longitude;
  String latitude;
  String address;
  int cityId;
  String city;
  int regionId;
  String region;
  int departmentId;
  String department;
  int productsNumber;
  dynamic viewCount;
  int commissionStatus;
  String apiToken;
  String identity;
  String license;
  String carForm;
  String driverPrice;
  DateTime createdAt;
    String openTime;
    String closeTime;
  dynamic details;

  int favorite;

    int carType;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        name: json["name"],
        phoneNumber: json["phone_number"],
        countryCode: json["country_code"],
        photo: json["photo"],
        photos: json["photos"],
        active: json["active"],
        type: json["type"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        address: json["address"],
        cityId: json["city_id"],
        city: json["city"],
                carType: json["car_type"] == null ? null : json["car_type"],
        details: json["details"],
        regionId: json["region_id"],
        region: json["region"],
        identity: json["identity"] == null ? null : json["identity"],
        license: json["license"] == null ? null : json["license"],
        carForm: json["car_form"] == null ? null : json["car_form"],
        departmentId: json["department_id"],
        department: json["department"],
        productsNumber: json["products_number"],
           openTime: json["open_time"] == null ? null : json["open_time"],
        closeTime: json["close_time"] == null ? null : json["close_time"],
        viewCount: json["view_count"],
        driverPrice: json["driver_price"] == null ? null : json["driver_price"],
        commissionStatus: json["commission_status"],
        favorite: json["favorite"] == null ? null : json["favorite"],
        apiToken: json["api_token"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phone_number": phoneNumber,
        "country_code": countryCode,
        "photo": photo,
        "photos": photos,
        "active": active,
        "type": type,
        "longitude": longitude,
        "latitude": latitude,
        "address": address,
        "city_id": cityId,
        "city": city,
        "details": details,
        "region_id": regionId,
        "region": region,
        "department_id": departmentId,
        "department": department,
        "products_number": productsNumber,
        "favorite": favorite == null ? null : favorite,
        "view_count": viewCount,
        "driver_price": driverPrice == null ? null : driverPrice,
        "commission_status": commissionStatus,
        "api_token": apiToken,
        "identity": identity == null ? null : identity,
        "license": license == null ? null : license,
        "car_form": carForm == null ? null : carForm,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
