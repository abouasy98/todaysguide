// To parse this JSON data, do
//
//     final settingModel = settingModelFromJson(jsonString);

import 'dart:convert';

SettingModel settingModelFromJson(String str) => SettingModel.fromJson(json.decode(str));

String settingModelToJson(SettingModel data) => json.encode(data.toJson());

class SettingModel {
    SettingModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
    dynamic error;

    factory SettingModel.fromJson(Map<String, dynamic> json) => SettingModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: Data.fromJson(json["data"]),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.phoneNumber,
        this.welcomeText,
        this.orderDuration,
        this.cancelDuration,
        this.pullLimit,
        this.chargeLimit,
    });

    String phoneNumber;
    String welcomeText;
    int orderDuration;
    int cancelDuration;
    String pullLimit;
    String chargeLimit;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        phoneNumber: json["phone_number"],
        welcomeText: json["welcome_text"],
        orderDuration: json["order_duration"],
        cancelDuration: json["cancel_duration"],
        pullLimit: json["pull_limit"],
        chargeLimit: json["charge_limit"],
    );

    Map<String, dynamic> toJson() => {
        "phone_number": phoneNumber,
        "welcome_text": welcomeText,
        "order_duration": orderDuration,
        "cancel_duration": cancelDuration,
        "pull_limit": pullLimit,
        "charge_limit": chargeLimit,
    };
}
