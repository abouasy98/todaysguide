// To parse this JSON data, do
//
//     final productCartCountModel = productCartCountModelFromJson(jsonString);

import 'dart:convert';

ProductCartCountModel productCartCountModelFromJson(String str) => ProductCartCountModel.fromJson(json.decode(str));

String productCartCountModelToJson(ProductCartCountModel data) => json.encode(data.toJson());

class ProductCartCountModel {
    ProductCartCountModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
    dynamic error;

    factory ProductCartCountModel.fromJson(Map<String, dynamic> json) => ProductCartCountModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: Data.fromJson(json["data"]),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.productCartCount,
    });

    int productCartCount;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        productCartCount: json["product-cart-count"],
    );

    Map<String, dynamic> toJson() => {
        "product-cart-count": productCartCount,
    };
}
