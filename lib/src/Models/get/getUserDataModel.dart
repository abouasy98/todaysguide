// To parse this JSON data, do
//
//     final getUserDataModel = getUserDataModelFromJson(jsonString);

import 'dart:convert';

GetUserDataModel getUserDataModelFromJson(String str) =>
    GetUserDataModel.fromJson(json.decode(str));

String getUserDataModelToJson(GetUserDataModel data) =>
    json.encode(data.toJson());

class GetUserDataModel {
  GetUserDataModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  UserData data;
  List<Error> error;

  factory GetUserDataModel.fromJson(Map<String, dynamic> json) =>
      GetUserDataModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null : UserData.fromJson(json["data"]),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
      };
}

class UserData {
  UserData(
      {this.id,
      this.name,
      this.phoneNumber,
      this.countryCode,
      this.photo,
      this.photos,
      this.active,
      this.type,
      this.longitude,
      this.latitude,
      this.address,
      this.cityId,
      this.city,
      this.regionId,
      this.region,
      this.departmentId,
      this.department,
      this.productsNumber,
      this.viewCount,
      this.commissionStatus,
      this.apiToken,
      this.createdAt,
      this.carForm,
      this.driverPrice,
      this.identity,
      this.license,
      this.closeTime,
      this.carType,
      this.openTime});

  int id;
  String name;
  String phoneNumber;
  int countryCode;
  String photo;
  List<Photo> photos;
  int active;
  int type;
  String longitude;
  String latitude;
  String address;
  int cityId;
  String city;
  int regionId;
  String openTime;
  String closeTime;
  String region;
  int departmentId;
  String department;
  String identity;
  String license;
  String carForm;
  int commissionStatus;
  dynamic driverPrice;
  String apiToken;
  DateTime createdAt;
  int carType;
  int productsNumber;
  int viewCount;
  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        id: json["id"],
        name: json["name"],
        photos: json["photos"] == null
            ? null
            : List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        phoneNumber: json["phone_number"],
        countryCode: json["country_code"],
        photo: json["photo"],
        active: json["active"],
        carType: json["car_type"] == null ? null : json["car_type"],
        type: json["type"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        address: json["address"],
        openTime: json["open_time"] == null ? null : json["open_time"],
        closeTime: json["close_time"] == null ? null : json["close_time"],
        cityId: json["city_id"],
        city: json["city"],
        regionId: json["region_id"],
        region: json["region"],
        departmentId: json["department_id"],
        department: json["department"],
        productsNumber: json["products_number"],
        carForm: json['car_form'],
        driverPrice: json['driver_price'],
        identity: json['identity'],
        license: json['license'],
        viewCount: json["view_count"],
        commissionStatus: json["commission_status"],
        apiToken: json["api_token"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phone_number": phoneNumber,
        "country_code": countryCode,
        "photo": photo,
        "photos": photos,
        "active": active,
        "type": type,
        "longitude": longitude,
        "latitude": latitude,
        "address": address,
        "city_id": cityId,
        "city": city,
        "region_id": regionId,
        "region": region,
        "department_id": departmentId,
        "department": department,
        "products_number": productsNumber,
        "view_count": viewCount,
        "commission_status": commissionStatus,
        "api_token": apiToken,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Photo {
  Photo({
    this.id,
    this.shopId,
    this.photo,
    this.createdAt,
  });

  int id;
  int shopId;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"] == null ? null : json["id"],
        shopId: json["shop_id"] == null ? null : json["shop_id"],
        photo: json["photo"] == null ? null : json["photo"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "shop_id": shopId == null ? null : shopId,
        "photo": photo == null ? null : photo,
        "created_at": createdAt == null
            ? null
            : "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
