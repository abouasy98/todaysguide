// To parse this JSON data, do
//
//     final myServicesModel = myServicesModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

MyServicesModel myServicesModelFromJson(String str) => MyServicesModel.fromJson(json.decode(str));

String myServicesModelToJson(MyServicesModel data) => json.encode(data.toJson());

class MyServicesModel {
    MyServicesModel({
        @required this.mainCode,
        @required this.code,
        @required this.data,
        @required this.error,
    });

    int mainCode;
    int code;
    List<MyServicesItem> data;
    List<Error> error;

    factory MyServicesModel.fromJson(Map<String, dynamic> json) => MyServicesModel(
        mainCode: json["mainCode"] == null ? null : json["mainCode"],
        code: json["code"] == null ? null : json["code"],
        data: json["data"] == null ? null : List<MyServicesItem>.from(json["data"].map((x) => MyServicesItem.fromJson(x))),
        error: json["error"] == null ? null : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode == null ? null : mainCode,
        "code": code == null ? null : code,
        "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error == null ? null : List<dynamic>.from(error.map((x) => x.toJson())),
    };
}

class MyServicesItem {
    MyServicesItem({
        @required this.id,
        @required this.userId,
        @required this.user,
        @required this.name,
        @required this.details,
        @required this.phoneNumber,
        @required this.photo,
        @required this.longitude,
        @required this.latitude,
        @required this.state,
        @required this.serviceCategoryId,
        @required this.serviceCategory,
        @required this.createdAt,
         this.distance,
    });

    int id;
    int userId;
    String user;
    String name;
    String details;
    String phoneNumber;
    String photo;
    String longitude;
    String latitude;
    int state;
    int serviceCategoryId;
    String serviceCategory;
    double distance;
    DateTime createdAt;

    factory MyServicesItem.fromJson(Map<String, dynamic> json) => MyServicesItem(
        id: json["id"] == null ? null : json["id"],
        userId: json["user_id"] == null ? null : json["user_id"],
        user: json["user"] == null ? null : json["user"],
        name: json["name"] == null ? null : json["name"],
        details: json["details"] == null ? null : json["details"],
        phoneNumber: json["phone_number"] == null ? null : json["phone_number"],
        photo: json["photo"] == null ? null : json["photo"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        state: json["state"] == null ? null : json["state"],
        serviceCategoryId: json["service_category_id"] == null ? null : json["service_category_id"],
        serviceCategory: json["service_category"] == null ? null : json["service_category"],
        createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "user_id": userId == null ? null : userId,
        "user": user == null ? null : user,
        "name": name == null ? null : name,
        "details": details == null ? null : details,
        "phone_number": phoneNumber == null ? null : phoneNumber,
        "photo": photo == null ? null : photo,
        "longitude": longitude == null ? null : longitude,
        "latitude": latitude == null ? null : latitude,
        "state": state == null ? null : state,
        "service_category_id": serviceCategoryId == null ? null : serviceCategoryId,
        "service_category": serviceCategory == null ? null : serviceCategory,
        "created_at": createdAt == null ? null : "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

class Error {
    Error({
        @required this.key,
        @required this.value,
    });

    String key;
    String value;

    factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
    );

    Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
    };
}
