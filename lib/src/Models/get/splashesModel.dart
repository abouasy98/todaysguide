// To parse this JSON data, do
//
//     final splashesModel = splashesModelFromJson(jsonString);

import 'dart:convert';

SplashesModel splashesModelFromJson(String str) => SplashesModel.fromJson(json.decode(str));

String splashesModelToJson(SplashesModel data) => json.encode(data.toJson());

class SplashesModel {
    SplashesModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    List<Datum> data;
    dynamic error;

    factory SplashesModel.fromJson(Map<String, dynamic> json) => SplashesModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
    };
}

class Datum {
    Datum({
        this.id,
        this.photo,
        this.providerId,
        this.provider,
        this.url,
        this.createdAt,
    });

    int id;
    String photo;
    dynamic providerId;
    dynamic provider;
    String url;
    DateTime createdAt;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        photo: json["photo"],
        providerId: json["provider_id"],
        provider: json["provider"],
        url: json["url"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "photo": photo,
        "provider_id": providerId,
        "provider": provider,
        "url": url,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}
