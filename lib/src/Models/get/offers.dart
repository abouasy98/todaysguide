// To parse this JSON data, do
//
//     final offers = offersFromJson(jsonString);

import 'dart:convert';

OffersModel offersFromJson(String str) => OffersModel.fromJson(json.decode(str));

String offersToJson(OffersModel data) => json.encode(data.toJson());

class OffersModel {
  OffersModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  List<Offers> data;
  List<Error> error;

  factory OffersModel.fromJson(Map<String, dynamic> json) => OffersModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null
            ? null
            : List<Offers>.from(json["data"].map((x) => Offers.fromJson(x))),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "error": error,
      };
}

class Offers {
  Offers({
    this.id,
    this.userId,
    this.user,
    this.userPhone,
    this.driverId,
    this.driver,
    this.driverPhoto,
    this.driverPrice,
    this.carType,
    this.driverPhone,
    this.orderId,
    this.status,
    this.createdAt,
  });

  int id;
  int userId;
  String user;
  String userPhone;
  int driverId;
  String driver;
  String driverPhoto;
  String driverPrice;
  String carType;
  String driverPhone;
  int orderId;
  String status;
  DateTime createdAt;

  factory Offers.fromJson(Map<String, dynamic> json) => Offers(
        id: json["id"],
        userId: json["user_id"],
        user: json["user"],
        userPhone: json["user_phone"],
        driverId: json["driver_id"],
        driver: json["driver"],
        driverPhoto: json["driver_photo"],
        driverPrice: json["driver_price"],
        carType: json["car_type"],
        driverPhone: json["driver_phone"],
        orderId: json["order_id"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "user": user,
        "user_phone": userPhone,
        "driver_id": driverId,
        "driver": driver,
        "driver_photo": driverPhoto,
        "driver_price": driverPrice,
        "car_type": carType,
        "driver_phone": driverPhone,
        "order_id": orderId,
        "status": status,
        "created_at":
            "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}
