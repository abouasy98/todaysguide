// To parse this JSON data, do
//
//     final productByIdModel = productByIdModelFromJson(jsonString);

import 'dart:convert';

ProductByIdModel productByIdModelFromJson(String str) => ProductByIdModel.fromJson(json.decode(str));

String productByIdModelToJson(ProductByIdModel data) => json.encode(data.toJson());

class ProductByIdModel {
    ProductByIdModel({
        this.mainCode,
        this.code,
        this.data,
        this.error,
    });

    int mainCode;
    int code;
    Data data;
    dynamic error;

    factory ProductByIdModel.fromJson(Map<String, dynamic> json) => ProductByIdModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: Data.fromJson(json["data"]),
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "mainCode": mainCode,
        "code": code,
        "data": data.toJson(),
        "error": error,
    };
}

class Data {
    Data({
        this.id,
        this.name,
        this.details,
        this.photos,
        this.price,
        this.available,
        this.shopId,
        this.shop,
        this.categoryId,
        this.category,
        this.createdAt,
    });

    int id;
    String name;
    String details;
    List<Photo> photos;
    String price;
    int available;
    int shopId;
    String shop;
    int categoryId;
    String category;
    DateTime createdAt;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        name: json["name"],
        details: json["details"],
        photos: json["photos"]==null?null: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        price: json["price"],
        available: json["available"],
        shopId: json["shop_id"],
        shop: json["shop"],
        categoryId: json["category_id"],
        category: json["category"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "details": details,
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "price": price,
        "available": available,
        "shop_id": shopId,
        "shop": shop,
        "category_id": categoryId,
        "category": category,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}

class Photo {
    Photo({
        this.id,
        this.productId,
        this.photo,
        this.createdAt,
    });

    int id;
    int productId;
    String photo;
    DateTime createdAt;

    factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        productId: json["product_id"],
        photo: json["photo"],
        createdAt: DateTime.parse(json["created_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "product_id": productId,
        "photo": photo,
        "created_at": "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
    };
}
