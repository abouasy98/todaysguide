// To parse this JSON data, do
//
//     final editAccountModel = editAccountModelFromJson(jsonString);

import 'dart:convert';

EditAccountModel editAccountModelFromJson(String str) =>
    EditAccountModel.fromJson(json.decode(str));

class EditAccountModel {
  EditAccountModel({
    this.mainCode,
    this.code,
    this.data,
    this.error,
  });

  int mainCode;
  int code;
  Data data;
  List<Error> error;

  factory EditAccountModel.fromJson(Map<String, dynamic> json) =>
      EditAccountModel(
        mainCode: json["mainCode"],
        code: json["code"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
        error: json["error"] == null
            ? null
            : List<Error>.from(json["error"].map((x) => Error.fromJson(x))),
      );
}

class Data {
  Data(
      {this.id,
      this.name,
      this.phoneNumber,
      this.countryCode,
      this.photo,
      this.active,
      this.type,
      this.commissionStatus,
      this.apiToken,
      this.createdAt,
      this.address,
      this.city,
      this.cityId,
      this.department,
      this.departmentId,
      this.details,
      this.favorite,
      this.latitude,
      this.longitude,
      this.openTime,
      this.closeTime,
      this.photos,
      this.productsNumber,
      this.region,
      this.regionId,
      this.viewCount,
      this.carForm,
      this.carType,
      this.driverPrice,
      this.identity,
      this.license});

  int id;
  String name;
  String phoneNumber;
  int countryCode;
  String photo;
  int active;
  int type;
  int commissionStatus;
  String apiToken;
  DateTime createdAt;
  List<Photo> photos;
  String longitude;
  String latitude;
  String address;
  dynamic details;
  int cityId;
  String city;
  int regionId;
  String region;
  int departmentId;
  String department;
  int productsNumber;
  int viewCount;
  String openTime;
  String closeTime;
  int favorite;

  int carType;

  String identity;
  String license;
  String carForm;

  String driverPrice;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        name: json["name"],
        phoneNumber: json["phone_number"],
        countryCode: json["country_code"],
        photo: json["photo"],
        active: json["active"],
        carType: json["car_type"] == null ? null : json["car_type"],
        identity: json["identity"] == null ? null : json["identity"],
        license: json["license"] == null ? null : json["license"],
        carForm: json["car_form"] == null ? null : json["car_form"],
        driverPrice: json["driver_price"] == null ? null : json["driver_price"],
        type: json["type"],
        commissionStatus: json["commission_status"],
        apiToken: json["api_token"],
        createdAt: DateTime.parse(json["created_at"]),
        photos: json["photos"] == null
            ? null
            : List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        longitude: json["longitude"] == null ? null : json["longitude"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        address: json["address"] == null ? null : json["address"],
        details: json["details"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        city: json["city"] == null ? null : json["city"],
        regionId: json["region_id"] == null ? null : json["region_id"],
        region: json["region"] == null ? null : json["region"],
        departmentId:
            json["department_id"] == null ? null : json["department_id"],
        openTime: json["open_time"] == null ? null : json["open_time"],
        closeTime: json["close_time"] == null ? null : json["close_time"],
        department: json["department"] == null ? null : json["department"],
        productsNumber:
            json["products_number"] == null ? null : json["products_number"],
        viewCount: json["view_count"] == null ? null : json["view_count"],
        favorite: json["favorite"] == null ? null : json["favorite"],
      );
}

class Error {
  Error({
    this.key,
    this.value,
  });

  String key;
  String value;

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        key: json["key"] == null ? null : json["key"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toJson() => {
        "key": key == null ? null : key,
        "value": value == null ? null : value,
      };
}

class Photo {
  Photo({
    this.id,
    this.shopId,
    this.photo,
    this.createdAt,
  });

  int id;
  int shopId;
  String photo;
  DateTime createdAt;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"] == null ? null : json["id"],
        shopId: json["shop_id"] == null ? null : json["shop_id"],
        photo: json["photo"] == null ? null : json["photo"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "shop_id": shopId == null ? null : shopId,
        "photo": photo == null ? null : photo,
        "created_at": createdAt == null
            ? null
            : "${createdAt.year.toString().padLeft(4, '0')}-${createdAt.month.toString().padLeft(2, '0')}-${createdAt.day.toString().padLeft(2, '0')}",
      };
}
