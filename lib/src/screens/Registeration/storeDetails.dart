import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/provider/auth/signUpProvider.dart';
import 'package:todaysguide/src/provider/get/RegionsProvider.dart';
import 'package:todaysguide/src/provider/get/citiesProvider.dart';
import 'package:todaysguide/src/provider/get/departmentsProvider.dart';
import 'package:todaysguide/src/screens/HomePages/more/internal/myPlaces/pickPlace.dart';
import 'package:todaysguide/src/screens/MainWidgets/labeled_bottom_sheet.dart';

class StoreDaitels extends StatefulWidget {
  @override
  _StoreDaitelsState createState() => _StoreDaitelsState();
}

class _StoreDaitelsState extends State<StoreDaitels>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  bool city = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // RegisterTextField(
        //   icon: Icons.label,
        //   onChange: (v) {
        //    // Provider.of<SignUpProvider>(context, listen: false).idNumber = v;
        //   },
        //   label: 'رقم الهوية',
        //   type: TextInputType.text,
        // ),
        // SizedBox(height: 20),
        // RegisterTextField(
        //   icon: Icons.label,
        //   onChange: (v) {
        //    // Provider.of<SignUpProvider>(context, listen: false).job = v;
        //   },
        //   label: 'الوظيفة',
        //   type: TextInputType.text,
        // ),
        // SizedBox(
        //   height: 20,
        // ),
        // RegisterTextField(
        //   icon: Icons.label,
        //   onChange: (v) {
        //   //  Provider.of<SignUpProvider>(context, listen: false).bank = v;
        //   },
        //   label: 'البنك',
        //   type: TextInputType.text,
        // ),
        // SizedBox(height: 20),
        // RegisterTextField(
        //   icon: Icons.label,
        //   onChange: (v) {
        //   //  Provider.of<SignUpProvider>(context, listen: false).bankAccount = v;
        //   },
        //   label: 'الحساب البنكي',
        //   type: TextInputType.text,
        // ),
        // DateCard(onDate: (v) {
        // // Provider.of<SignUpProvider>(context, listen: false).barithDay = v;
        // }),
        LabeledBottomSheet(
          label: '-- إختر المنطقة --',
          onChange: (v) {
            // Provider.of<SignUpProvider>(context, listen: false).regionId =
            //     v.id.toString();
            Provider.of<CitiesProvider>(context, listen: false)
                .getCities(v.id.toString(),context);
            setState(() {
              city = true;
            });
          },
          data: Provider.of<RegionsProvider>(context, listen: true).bottomSheet,
        ),
        LabeledBottomSheet(
          label: '-- إختر المدينة --',
          onChange: (v) {
            Provider.of<SignUpProvider>(context, listen: false).cityId =
                v.id.toString();
          },
          ontap: city,
          data: Provider.of<CitiesProvider>(context, listen: true).cotiesSheet,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10, right: 10, top: 20),
          child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => PickPlace(
                    
                      )));
            },
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.grey)),
              padding: EdgeInsets.all(10),
              child: Padding(
                padding: const EdgeInsets.only(right: 8.0, left: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.arrow_drop_down),
                    Text(Provider.of<SignUpProvider>(context, listen: false)
                                .address !=
                            null
                        ? Provider.of<SignUpProvider>(context, listen: false)
                            .address.length>50?Provider.of<SignUpProvider>(context, listen: false)
                            .address.substring(0,40):Provider.of<SignUpProvider>(context, listen: false)
                            .address
                        : 'حدد موقع المتجر')
                  ],
                ),
              ),
            ),
          ),
        ),
        LabeledBottomSheet(
          label: '-- إختر القسم --',
          onChange: (v) {
            Provider.of<SignUpProvider>(context, listen: false).departmentId =
                v.id.toString();
          },
          data: Provider.of<DepartMentProvider>(context, listen: true)
              .cotiesSheet,
        ),
        // LabeledBottomSheet(
        //   label: '-- إختر الجنسية --',
        //   onChange: (v) {
        //     Provider.of<SignUpProvider>(context, listen: false).nationalityId =
        //         v.id.toString();
        //   },
        //   data: Provider.of<NationalitiesProvider>(context, listen: true)
        //       .bottomSheet,
        // ),
        // LabeledBottomSheet(
        //   label: '-- إختر نوع السيارة --',
        //   onChange: (v) {
        //     Provider.of<SignUpProvider>(context, listen: false).carTypeId =
        //         v.id.toString();
        //   },
        //   data: Provider.of<CarTypeProvider>(context, listen: true).bottomSheet,
        // ),
        // LabeledBottomSheet(
        //   label: '-- إختر نوع الهوية --',
        //   onChange: (v) {
        //     Provider.of<SignUpProvider>(context, listen: false).identityTypeId =
        //         v.id.toString();
        //   },
        //   data: Provider.of<IdentituTypeProvider>(context, listen: true)
        //       .bottomSheet,
        // ),
        // LabeledBottomSheet(
        //   label: '-- إختر التوصيل  --',
        //   onChange: (v) {
        //  //   Provider.of<SignUpProvider>(context, listen: false).deliveryType =
        //         v.id.toString();
        //   },
        //   data: [
        //     BottomSheetModel(
        //       id: 0,
        //       name: "سيارو عادية",
        //       realID: "0",
        //     ),
        //     BottomSheetModel(
        //       id: 1,
        //       name: "سيارو بيك اب",
        //       realID: "1",
        //     ),
        //   ],
        // ),
        // SizedBox(
        //   height: 10,
        // ),
      ],
    );
  }
}
