import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/auth/signUpProvider.dart';
import 'package:todaysguide/src/provider/get/RegionsProvider.dart';
import 'package:todaysguide/src/provider/get/departmentsProvider.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_button_with_icon.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_secure_text_field.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';
import 'package:todaysguide/src/screens/MainWidgets/terms_dialog.dart';
import 'package:todaysguide/src/screens/Registeration/documentsStroe.dart';
import 'package:todaysguide/src/screens/Registeration/widget/appIcon.dart';

class SignUpStoreScreen extends StatefulWidget {
  @override
  _SignUpStoreScreenState createState() => _SignUpStoreScreenState();
}

class _SignUpStoreScreenState extends State<SignUpStoreScreen> {
  bool _accept = false;

  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;
  String hour1 = "اختار وقت بداية عمل المتجر";
  String hour2 = "اختار وقت انتهاء المتجر";
  @override
  void initState() {
    //   Provider.of<MapHelper>(context, listen: false).getLocation();
    //  Provider.of<CarTypeProvider>(context, listen: false).getCarsType();
    // Provider.of<DepartMentProvider>(context, listen: false).getDepartements();
    // Provider.of<IdentituTypeProvider>(context, listen: false).getIdentities();
    // Provider.of<NationalitiesProvider>(context, listen: false).getNationalities();
    // Provider.of<TermsProvider>(context, listen: false).getTerms(context);
    Provider.of<RegionsProvider>(context, listen: false).getRegions(context);
    Provider.of<DepartMentProvider>(context, listen: false)
        .getDepartements(context);
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });

      print('The device Token is :' + _deviceToken);
    });
    super.initState();
  }

  // Future<void> getUserLocation() async {
  //   var location = Geolocator();

  //   var currentLocation = await Geolocator.getCurrentPosition(
  //     desiredAccuracy: LocationAccuracy.medium,
  //   );
  //   Provider.of<SignUpProvider>(context, listen: false).latitude =
  //       currentLocation.latitude;
  //   Provider.of<SignUpProvider>(context, listen: false)
  //     ..longitude = currentLocation.longitude;
  //   print('lat: ${currentLocation.latitude}, lon:${currentLocation.longitude}');
  // }

  final _form = GlobalKey<FormState>();
  bool autoError = false;
  int _radioValue1 = -1;
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        key: _globalKey,
        resizeToAvoidBottomInset: true,
        body: Form(
          key: _form,
          autovalidateMode: autoError ? AutovalidateMode.always : AutovalidateMode.disabled,
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                primary: false,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: mediaQuery.height * 0.1,
                    ),
                    AppIcon(),
                    Container(
                      width: mediaQuery.width,
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.only(right: 18),
                      child: Text(
                        'تسجيل جديد كمتجر',
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    RegisterTextField(
                      icon: Icons.person,
                      onChange: (v) {
                        Provider.of<SignUpProvider>(context, listen: false)
                            .name = v;
                      },
                      label: 'اسم المتجر',
                      type: TextInputType.text,
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    RegisterTextField(
                      icon: Icons.details,
                      onChange: (v) {
                        Provider.of<SignUpProvider>(context, listen: false)
                            .details = v;
                      },
                      label: 'تفاصيل المتجر',
                      type: TextInputType.text,
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    DocumentsStore(),
                    SizedBox(height: mediaQuery.height * 0.04),
                    CustomButtonClock(
                      lable: "من",
                      onConfirm: (time) {
                        setState(() {
                          print(
                              TimeOfDay(hour: time.hour, minute: time.minute));
                          Provider.of<SignUpProvider>(context, listen: false)
                                  .open =
                              TimeOfDay(hour: time.hour, minute: time.minute)
                                  .toString()
                                  .substring(10, 15);
                          hour1 = TimeOfDay(
                                      hour: time.hour, minute: time.minute)
                                  .format(context)
                                  .contains("AM")
                              ? TimeOfDay(hour: time.hour, minute: time.minute)
                                  .format(context)
                                  .replaceAll("AM", "ص")
                              : TimeOfDay(hour: time.hour, minute: time.minute)
                                  .format(context)
                                  .replaceAll("PM", "م");
                        });
                      },
                      icon: FontAwesomeIcons.clock,
                      date: hour1,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomButtonClock(
                      lable: "الي",
                      onConfirm: (time) {
                        setState(() {
                          setState(() {
                            Provider.of<SignUpProvider>(context, listen: false)
                                    .close =
                                TimeOfDay(hour: time.hour, minute: time.minute)
                                    .toString()
                                    .substring(10, 15);
                            hour2 =
                                TimeOfDay(hour: time.hour, minute: time.minute)
                                        .format(context)
                                        .contains("AM")
                                    ? TimeOfDay(
                                            hour: time.hour,
                                            minute: time.minute)
                                        .format(context)
                                        .replaceAll("AM", "ص")
                                    : TimeOfDay(
                                            hour: time.hour,
                                            minute: time.minute)
                                        .format(context)
                                        .replaceAll("PM", "م");
                          });
                        });
                      },
                      icon: FontAwesomeIcons.clock,
                      date: hour2,
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    RegisterSecureTextField(
                      onChange: (v) {
                        Provider.of<SignUpProvider>(context, listen: false)
                            .password = v;
                      },
                      label: "كلمة المرور",
                      icon: Icons.lock,
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    RegisterSecureTextField(
                      onChange: (v) {
                        Provider.of<SignUpProvider>(context, listen: false)
                            .passwordConfirmation = v;
                      },
                      label: 'تأكيد كلمة المرور',
                      icon: Icons.lock,
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    CheckboxListTile(
                      value: _accept,
                      onChanged: (value) {
                        setState(() {
                          _accept = !_accept;
                        });
                      },
                      activeColor: Theme.of(context).primaryColor,
                      checkColor: Colors.white,
                      dense: true,
                      title: InkWell(
                        onTap: () => TermsDialog().show(context: context),
                        child: Text(
                          'اوافق على الشروط والأحكام',
                          textAlign: TextAlign.right,
                          style:
                              TextStyle(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                    SizedBox(height: mediaQuery.height * 0.02),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SignInButton(
                        txtColor: Colors.white,
                        onPressSignIn: () async {
                          setState(() {
                            autoError = true;
                          });
                          final isValid = _form.currentState.validate();
                          if (!isValid) {
                            return;
                          }
                          _form.currentState.save();
                          if (_accept == false) {
                            CustomAlert().toast(
                                context: context,
                                title: 'يجب الموافقة على الشروط والأحكام');
                          } else {
                            print('$_radioValue1');
                            // Provider.of<LoginProvider>(context, listen: false)
                            //     .type = 3;

                            Provider.of<SignUpProvider>(context, listen: false)
                                .type = 3;

                            await Provider.of<SignUpProvider>(context,
                                    listen: false)
                                .signUp(_deviceToken, context)
                                .then((v) {
                              if (v != null) {
                                Provider.of<SharedPref>(context, listen: false)
                                    .getSharedHelper(v);
                              }
                            });
                          }
                        },
                        btnWidth: MediaQuery.of(context).size.width,
                        btnHeight: MediaQuery.of(context).size.height * .07,
                        btnColor: Theme.of(context).primaryColor,
                        buttonText: 'تسجيل',
                      ),
                    ),
                    SizedBox(height: 50),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
