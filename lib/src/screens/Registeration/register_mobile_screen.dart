import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:todaysguide/src/provider/auth/registerMobileProvider.dart';
import 'package:todaysguide/src/provider/countriesProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';

import 'widget/appIcon.dart';

class RegisterMobileScreen extends StatefulWidget {
  @override
  _RegisterMobileScreenState createState() => _RegisterMobileScreenState();
}

class _RegisterMobileScreenState extends State<RegisterMobileScreen> {
  final _form = GlobalKey<FormState>();
  bool autoError = false;
  bool city = false;
  @override
  void initState() {
    super.initState();
    Provider.of<CountriesProvider>(context, listen: false)
        .getCountries(context);
  }

  @override
  Widget build(BuildContext context) {
    var register = Provider.of<RegisterMobileProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Form(
        key: _form,
        autovalidateMode: autoError ? AutovalidateMode.always : AutovalidateMode.disabled,
        child: Stack(
          children: <Widget>[
            ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: <Widget>[
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height * .9,
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        AppIcon(),
                        Center(
                            child: Text(
                          "تسجيل جديد",
                          style: TextStyle(color: Colors.black87, fontSize: 20),
                        )),
                        SizedBox(
                          height: 5,
                        ),
                        Center(
                            child: Text(
                          "فضلا قم بادخال رقم جوالك ليصلك كود التفعيل",
                          style: TextStyle(color: Colors.black87, fontSize: 13),
                          textAlign: TextAlign.center,
                        )),
                        SizedBox(
                          height: 40,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 10, left: 10),
                          child: Directionality(
                            textDirection: TextDirection.rtl,
                            child: TextFormField(
                              textAlign: TextAlign.right,
                              keyboardType: TextInputType.number,
                              onChanged: (v) {
                                register.phone = v;
                              },
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "رقم الجوال مطلوب ";
                                }
                                if (!value.startsWith('0')) {
                                  return "يجب ان يبدا رقم هاتفك ب 05";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                prefixIcon: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircleAvatar(
                                    radius: 1,
                                    child: Icon(
                                      Icons.phone,
                                      size: 15,
                                      color: Colors.white,
                                    ),
                                    backgroundColor:
                                        Theme.of(context).primaryColor,
                                  ),
                                ),
                                // errorText: widget.errorText ?? null,
                                hintText: "رقم الجوال",
                                contentPadding:
                                    EdgeInsets.only(top: 20, right: 10),
                                border: new OutlineInputBorder(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(20)),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SignInButton(
                            txtColor: Colors.white,
                            onPressSignIn: () async {
                              setState(() {
                                autoError = true;
                              });
                              final isValid = _form.currentState.validate();
                              if (!isValid) {
                                return;
                              }
                              _form.currentState.save();

                              await register.registerMobile(context);
                            },
                            btnWidth: MediaQuery.of(context).size.width,
                            btnHeight: 45,
                            btnColor: Theme.of(context).primaryColor,
                            buttonText: 'التالي',
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              top: 40,
              left: 20,
              child: IconButton(
                onPressed: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black87,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
