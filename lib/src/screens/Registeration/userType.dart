import 'package:flutter/material.dart';

import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/Registeration/sign_up_driver_screen.dart';
import 'package:todaysguide/src/screens/Registeration/sign_up_store_screen.dart';

import 'Sign_Up_Users_screen.dart';


class UserType extends StatefulWidget {
  @override
  _UserTypeState createState() => _UserTypeState();
}

class _UserTypeState extends State<UserType> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
       //   ImageBG(),
          ListView(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            children: <Widget>[
              
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height * .9,
                  alignment: Alignment.center,
                  child: Form(
                    autovalidateMode:  AutovalidateMode.always ,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Center(
                            child: Image.asset(
                          'assets/images/icon-001.png',
                          height: MediaQuery.of(context).size.height*0.2,
                          fit: BoxFit.cover,
                        )),
                        SizedBox(
                          height:MediaQuery.of(context).size.height*0.1,
                        ),
                      
                               Padding(
                          padding: const EdgeInsets.only(right: 8),
                          child: Text(
                            "تسجيل جديد",
                            style: TextStyle(
                                color: Color.fromRGBO(77, 107, 197, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SignInButton(
                            txtColor: Colors.white,
                            onPressSignIn: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => SignUpUsers()));
                            },
                            btnWidth: MediaQuery.of(context).size.width,
                            btnHeight: MediaQuery.of(context).size.height * .07,
                            btnColor: Theme.of(context).primaryColor,
                            buttonText: 'التسجيل كعميل',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SignInButton(
                            txtColor: Colors.white,
                            onPressSignIn: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => SignUpStoreScreen( )));
                            },
                            btnWidth: MediaQuery.of(context).size.width,
                            btnHeight: MediaQuery.of(context).size.height * .07,
                            btnColor: Theme.of(context).primaryColor,
                            buttonText: 'التسجيل كمتجر',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SignInButton(
                            txtColor: Colors.white,
                            onPressSignIn: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => SignUpDriverScreen( )));
                            },
                            btnWidth: MediaQuery.of(context).size.width,
                            btnHeight: MediaQuery.of(context).size.height * .07,
                            btnColor: Theme.of(context).primaryColor,
                            buttonText: 'التسجيل كمندوب',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            top: 40,
            left: 20,
            child: IconButton(
              onPressed: () {
                FocusScope.of(context).requestFocus(FocusNode());
                Navigator.of(context).pop();
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.black87,
              ),
            ),
          )
        ],
      ),
    );
  }
}
