import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/provider/auth/forgetPasswordProvider.dart';
import 'package:todaysguide/src/provider/countriesProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';
import 'widget/appIcon.dart';
class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  @override
  void initState() {
    super.initState();
        Provider.of<CountriesProvider>(context, listen: false).getCountries(context);
  }

  bool city = false;
  final _form = GlobalKey<FormState>();
  bool autoError = false;
  @override
  Widget build(BuildContext context) {
    var forgetPassword =
        Provider.of<ForgetPasswordProvider>(context, listen: false);

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Form(
        key: _form,
        autovalidateMode: autoError ? AutovalidateMode.always : AutovalidateMode.disabled,
        child: Stack(
          children: <Widget>[
            //   ImageBG(),
            ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: <Widget>[
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 50),
                        AppIcon(),
                        Center(
                            child: Text(
                          "فضلا ادخل رقم الجوال",
                          style: TextStyle(color: Colors.black87, fontSize: 20),
                        )),
                        SizedBox(
                          height: 5,
                        ),
                        Center(
                            child: Text(
                          "لاستعادة كلمة المرور",
                          style: TextStyle(color: Colors.black87, fontSize: 15),
                        )),
                        SizedBox(
                          height: 50,
                        ),
                        RegisterTextField(
                          hint: 'رقم الجوال',
                          icon: Icons.phone,
                          onChange: (v) {
                            forgetPassword.phone = v;
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
           
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SignInButton(
                            txtColor: Colors.white,
                            onPressSignIn: () async {
                              setState(() {
                                autoError = true;
                              });
                              final isValid = _form.currentState.validate();
                              if (!isValid) {
                                return;
                              }
                              _form.currentState.save();
                              await forgetPassword.forgetPassword(context);
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (_) => ResetPasswordScreen()));
                            },
                            btnWidth: MediaQuery.of(context).size.width,
                            btnHeight: 45,
                            btnColor: Theme.of(context).primaryColor,
                            buttonText: 'ارسال كود التفعيل',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              top: 40,
              left: 20,
              child: IconButton(
                onPressed: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.of(context).pop();
                },
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black87,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
