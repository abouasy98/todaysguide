import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_secure_text_field.dart';
import 'package:todaysguide/src/screens/Registeration/widget/appIcon.dart';
import '../../provider/auth/loginProvider.dart';
import 'forgetPassword.dart';
import 'register_mobile_screen.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });
      print('The device Token is :' + _deviceToken);
    });
  }

  @override
  void dispose() {
    // _loginButtonController.dispose();
    super.dispose();
  }

  // Future<Null> _playAnimation() async {
  //   try {
  //     await _loginButtonController.forward();
  //     await _loginButtonController.reverse();
  //   } on TickerCanceled {}
  // }

  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;
  final _form = GlobalKey<FormState>();
  bool autoError = false;
  @override
  @override
  Widget build(BuildContext context) {
    var loginProvider = Provider.of<LoginProvider>(context, listen: false);

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          resizeToAvoidBottomInset: true,
          body: Form(
            key: _form,
            autovalidateMode: autoError ? AutovalidateMode.always : AutovalidateMode.disabled,
            child: Stack(
              children: <Widget>[
                //ImageBG(),
                ListView(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  children: <Widget>[
                    Center(
                      child: Container(
                        height: MediaQuery.of(context).size.height * .95,
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AppIcon(),

                            Padding(
                              padding: const EdgeInsets.only(right: 18),
                              child: Text(
                                "تسجيل الدخول",
                                style: TextStyle(
                                    color: Colors.indigo,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20),
                              ),
                            ),

                            SizedBox(
                              height: 20,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(right: 10, left: 10),
                              child: Directionality(
                                textDirection: TextDirection.rtl,
                                child: TextFormField(
                                  textAlign: TextAlign.right,
                                  keyboardType: TextInputType.number,
                                  onChanged: (v) {
                                    loginProvider.phone = v;
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "رقم الجوال مطلوب ";
                                    }
                                    if (!value.startsWith('0')) {
                                      return "يجب ان يبدا رقم هاتفك ب 05";
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: CircleAvatar(
                                        radius: 1,
                                        child: Icon(
                                          Icons.phone,
                                          size: 15,
                                          color: Colors.white,
                                        ),
                                        backgroundColor:
                                            Theme.of(context).primaryColor,
                                      ),
                                    ),
                                    // errorText: widget.errorText ?? null,
                                    hintText: "رقم الجوال",
                                    contentPadding:
                                        EdgeInsets.only(top: 20, right: 10),
                                    border: new OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(20)),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            RegisterSecureTextField(
                              icon: Icons.lock,
                              label: "كلمة المرور",
                              onChange: (v) {
                                loginProvider.password = v;
                              },
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              alignment: Alignment.centerLeft,
                              child: FlatButton(
                                child: Text('نسيت كلمة المرور ',
                                    style: TextStyle(color: Colors.black)),
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (_) => ForgetPassword()));
                                },
                              ),
                            ),
                            // animationStatus == 0
                            //     ?
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SignInButton(
                                txtColor: Colors.white,
                                onPressSignIn: () async {
                                  // setState(() {
                                  //   animationStatus = 1;
                                  // });
                                  // _playAnimation();
                                  setState(() {
                                    autoError = true;
                                  });
                                  final isValid = _form.currentState.validate();
                                  if (!isValid) {
                                    return;
                                  }
                                  _form.currentState.save();
                                  await loginProvider
                                      .login(_deviceToken, context)
                                      .then((v) {
                                    if (v != null) {
                                      Provider.of<SharedPref>(context,
                                              listen: false)
                                          .getSharedHelper(v);
                                    }
                                  });
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (_) => OrderTypeScreen()));
                                },
                                btnWidth: MediaQuery.of(context).size.width,
                                btnHeight: 45,
                                btnColor: Theme.of(context).primaryColor,
                                buttonText: 'دخول',
                              ),
                            ),
                            // : StaggerAnimation(buttonController: _loginButtonController.view),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('ليس عندك حساب ؟'),
                                GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (_) =>
                                                  RegisterMobileScreen()));
                                    },
                                    child: Text(
                                      'تسجيل جديد',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          )),
    );
  }
}
