import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/auth/signUpProvider.dart';
import 'package:todaysguide/src/provider/get/RegionsProvider.dart';
import 'package:todaysguide/src/provider/termsProvider.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_secure_text_field.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';
import 'package:todaysguide/src/screens/MainWidgets/terms_dialog.dart';
import 'package:todaysguide/src/screens/Registeration/widget/appIcon.dart';
import 'documentsdriver.dart';

class SignUpDriverScreen extends StatefulWidget {
  @override
  _SignUpDriverScreenState createState() => _SignUpDriverScreenState();
}

class _SignUpDriverScreenState extends State<SignUpDriverScreen> {
  bool _accept = false;

  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;
  @override
  void initState() {
   // Provider.of<MapHelper>(context, listen: false).getLocation();
    //  Provider.of<CarTypeProvider>(context, listen: false).getCarsType();
    // Provider.of<DepartMentProvider>(context, listen: false).getDepartements();
    // Provider.of<IdentituTypeProvider>(context, listen: false).getIdentities();
    // Provider.of<NationalitiesProvider>(context, listen: false).getNationalities();
    Provider.of<TermsProvider>(context, listen: false).getTerms(context);
    Provider.of<RegionsProvider>(context, listen: false).getRegions(context);
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });

      print('The device Token is :' + _deviceToken);
    });
    super.initState();
  }

  final _form = GlobalKey<FormState>();
  bool autoError = false;
  int _radioValue1 = -1;
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        key: _globalKey,
        resizeToAvoidBottomInset: true,
        body: Form(
          key: _form,
          autovalidateMode: autoError ? AutovalidateMode.always : AutovalidateMode.disabled,
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                primary: false,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: mediaQuery.height * 0.1,
                    ),
                    AppIcon(),
                    Container(
                      width: mediaQuery.width,
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.only(right: 18),
                      child: Text(
                        'تسجيل جديد كمندوب',
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    RegisterTextField(
                      icon: Icons.person,
                      onChange: (v) {
                        Provider.of<SignUpProvider>(context, listen: false)
                            .name = v;
                      },
                      label: 'الاسم',
                      type: TextInputType.text,
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    Padding(
                        padding: const EdgeInsets.only(right: 10, left: 10),
                        child: Directionality(
                          textDirection: TextDirection.rtl,
                          child: TextFormField(
                            //     initialValue: widget.init,
                            textAlign: TextAlign.right,
                            keyboardType: TextInputType.number,
                            onChanged: (v) {
                              Provider.of<SignUpProvider>(context,
                                      listen: false)
                                  .price = int.parse(v);
                            },
                            validator: (value) {
                              if (value.isEmpty) {
                                return "سعر التوصيل مطلوب";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              prefixIcon: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CircleAvatar(
                                  radius: 1,
                                  child: Icon(
                                    Icons.drive_eta,
                                    size: 15,
                                    color: Colors.white,
                                  ),
                                  backgroundColor:
                                      Theme.of(context).primaryColor,
                                ),
                              ),

                              labelText: 'سعر التوصيل',
                              // errorText: widget.errorText ?? null,
                              // hintText: widget.hint == null ? '' : widget.hint,
                              contentPadding:
                                  EdgeInsets.only(top: 20, right: 10),
                              border: new OutlineInputBorder(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(20)),
                              ),
                            ),
                          ),
                        )),
                    SizedBox(height: mediaQuery.height * 0.04),
                    Documents(),
                    Container(
                      height: mediaQuery.height * 0.1,
                      width: mediaQuery.width * 0.8,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Text(
                            'مبردة',
                            style: new TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          ),
                          new Radio(
                            activeColor: Theme.of(context).primaryColor,
                            value: 1,
                            groupValue: _radioValue1,
                            onChanged: (value) {
                              setState(() {
                                _radioValue1 = value;
                              });
                            },
                          ),
                          new Text(
                            'عادية',
                            style: new TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          ),
                          new Radio(
                            activeColor: Theme.of(context).primaryColor,
                            value: 0,
                            groupValue: _radioValue1,
                            onChanged: (value) {
                              setState(() {
                                _radioValue1 = value;
                              });
                            },
                          ),
                          Expanded(child: Text('')),
                          Text(
                            'نوع السيارة',
                            style: TextStyle(
                                fontSize: 16.0, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    RegisterSecureTextField(
                      onChange: (v) {
                        Provider.of<SignUpProvider>(context, listen: false)
                            .password = v;
                      },
                      label: "كلمة المرور",
                      icon: Icons.lock,
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    RegisterSecureTextField(
                      onChange: (v) {
                        Provider.of<SignUpProvider>(context, listen: false)
                            .passwordConfirmation = v;
                      },
                      label: 'تأكيد كلمة المرور',
                      icon: Icons.lock,
                    ),
                    SizedBox(height: mediaQuery.height * 0.04),
                    CheckboxListTile(
                      value: _accept,
                      onChanged: (value) {
                        setState(() {
                          _accept = !_accept;
                        });
                      },
                      activeColor: Theme.of(context).primaryColor,
                      checkColor: Colors.white,
                      dense: true,
                      title: InkWell(
                        onTap: () => TermsDialog().show(context: context),
                        child: Text(
                          'اوافق على الشروط والأحكام',
                          textAlign: TextAlign.right,
                          style:
                              TextStyle(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                    SizedBox(height: mediaQuery.height * 0.02),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SignInButton(
                        txtColor: Colors.white,
                        onPressSignIn: () async {
                          setState(() {
                            autoError = true;
                          });
                          final isValid = _form.currentState.validate();
                          if (!isValid) {
                            return;
                          }
                          _form.currentState.save();
                          if (_accept == false) {
                            CustomAlert().toast(
                                context: context,
                                title: 'يجب الموافقة على الشروط والأحكام');
                          } else {
                            print('$_radioValue1');
                            // Provider.of<LoginProvider>(context, listen: false)
                            //     .type = 2;
                            Provider.of<SignUpProvider>(context, listen: false)
                                .type = 2;
                            Provider.of<SignUpProvider>(context, listen: false)
                                .carType = _radioValue1;
                            await Provider.of<SignUpProvider>(context,
                                    listen: false)
                                .signUp(_deviceToken, context)
                                .then((v) {
                              if (v != null) {
                                Provider.of<SharedPref>(context, listen: false)
                                    .getSharedHelper(v);
                              }
                            });
                          }
                        },
                        btnWidth: MediaQuery.of(context).size.width,
                        btnHeight: MediaQuery.of(context).size.height * .07,
                        btnColor: Theme.of(context).primaryColor,
                        buttonText: 'تسجيل',
                      ),
                    ),
                    SizedBox(height: 50),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
