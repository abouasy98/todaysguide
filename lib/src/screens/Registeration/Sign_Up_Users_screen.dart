import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/auth/signUpProvider.dart';
import 'package:todaysguide/src/provider/termsProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_secure_text_field.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';
import 'package:todaysguide/src/screens/MainWidgets/terms_dialog.dart';
import 'widget/appIcon.dart';

class SignUpUsers extends StatefulWidget {
  @override
  _SignUpUsersState createState() => _SignUpUsersState();
}

class _SignUpUsersState extends State<SignUpUsers> {
  bool _accept = false;
  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
 //   Provider.of<MapHelper>(context, listen: false).getLocation();
    Provider.of<TermsProvider>(context, listen: false).getTerms(context);
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });
      print('The device Token is :' + _deviceToken);
    });
    super.initState();
  }

  bool city = false;
  final _form = GlobalKey<FormState>();
  bool autoError = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _globalKey,
      resizeToAvoidBottomInset: true,
      body: Form(
        key: _form,
        autovalidateMode: autoError ? AutovalidateMode.always : AutovalidateMode.disabled,
        child: Stack(
          children: <Widget>[
          //  ImageBG(),
            ListView(
              children: <Widget>[
           
                AppIcon(),
          
                SizedBox(height: 20),
                RegisterTextField(
                  icon: Icons.person,
                  onChange: (v) {
                   Provider.of<SignUpProvider>(context, listen: false)
                        .name = v;
                  },
                  label: 'الاسم',
                  type: TextInputType.text,
                ),
            
                SizedBox(height: 20),
                RegisterSecureTextField(
                  onChange: (v) {
                   Provider.of<SignUpProvider>(context, listen: false)
                       .password = v;
                  },
                  label: "كلمة المرور",
                  icon: Icons.lock,
                ),
                SizedBox(height: 20),
                RegisterSecureTextField(
                  onChange: (v) {
                   Provider.of<SignUpProvider>(context, listen: false)
                       .passwordConfirmation = v;
                  },
                  icon: Icons.lock,
                  label: 'تأكيد كلمة المرور',
                ),
                // LabeledBottomSheet(
                //   label: '-- إختر المنطقة --',
                //   onChange: (v) {
                //     Provider.of<CitiesProvider>(context, listen: false)
                //         .getCities(v.id.toString());
                //     setState(() {
                //       city = true;
                //     });
                //   },
                //   data: Provider.of<RegionsProvider>(context, listen: true)
                //       .bottomSheet,
                // ),
                // LabeledBottomSheet(
                //   label: '-- إختر المدينة --',
                //   onChange: (v) {
                //     Provider.of<SignUpUserProvider>(context, listen: false)
                //         .cityId = v.id.toString();
                //     setState(() {
                //       city = true;
                //     });
                //   },
                //   ontap: city,
                //   data: Provider.of<CitiesProvider>(context, listen: true)
                //       .cotiesSheet,
                // ),
                SizedBox(height: 20),
                CheckboxListTile(
                  value: _accept,
                  onChanged: (value) {
                    setState(() {
                      _accept = !_accept;
                    });
                  },
                  activeColor: Theme.of(context).primaryColor,
                  checkColor: Colors.white,
                  dense: true,
                  title: InkWell(
                    onTap: () => TermsDialog().show(context: context),
                    child: Text(
                      'اوافق على الشروط والأحكام',
                      textAlign: TextAlign.right,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SignInButton(
                    txtColor: Colors.white,
                    onPressSignIn: ()async {
                      setState(() {
                        autoError = true;
                      });
                      final isValid = _form.currentState.validate();
                      if (!isValid) {
                        return;
                      }
                      _form.currentState.save();

                      if (_accept == false) {
                        CustomAlert().toast(
                            context: context,
                            title: 'يجب الموافقة على الشروط والأحكام');
                      } else {
                             _form.currentState.save();
                        //  Provider.of<LoginProvider>(context, listen: false)
                        //      .type = 1;
                         Provider.of<SignUpProvider>(context, listen: false)
                             .type = 1;
                       await   Provider.of<SignUpProvider>(context, listen: false)
                             .signUp(_deviceToken, context)
                             .then((v) {
                           if (v != null) {
                         Provider.of<SharedPref>(context, listen: false)
                                .getSharedHelper(v);
                          }
                            //   Navigator.of(context).push(MaterialPageRoute(
                            //       builder: (_) => MainPage(
                            //           )));
                         });
                      }
                    },
                    btnWidth: MediaQuery.of(context).size.width,
                    btnHeight: 50,
                    btnColor: Theme.of(context).primaryColor,
                    buttonText: 'انشاء حساب',
                  ),
                ),
                SizedBox(height: 50),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
