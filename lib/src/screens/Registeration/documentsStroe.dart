import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/provider/auth/signUpProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_image_getter.dart';
import 'package:todaysguide/src/screens/Registeration/storeDetails.dart';

class DocumentsStore extends StatefulWidget {
  @override
  _DocumentsStoreState createState() => _DocumentsStoreState();
}

class _DocumentsStoreState extends State<DocumentsStore> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        GetImage().showMainImg(
          context: context,
          deleteImage: () {
            setState(() {
              _profileImg = null;
            });
          },
          cameraBtn: _cameraBtn(
              onTap: () => _getMainImg(ImageSource.gallery, 3),
              icon: Icons.camera_alt,
              label: "صورة المتجر"),
          mainImg: _profileImg,
        ),
        SizedBox(height: 20),
        StoreDaitels(),
      ],
    );
  }

  _getMainImg(ImageSource source, int currentImage) async {
    var image = await ImagePicker().getImage(source: source);
    setState(() {
      switch (currentImage) {
        case 0:
          return setState(() {
            _mainImg = File(image.path);
            Provider.of<SignUpProvider>(context, listen: false).identity =
                _mainImg;
          });
        case 1:
          return setState(() {
            _idImg = File(image.path);
            Provider.of<SignUpProvider>(context, listen: false).license =
                _idImg;
          });
        case 2:
          return setState(() {
            _carFormImg = File(image.path);
            Provider.of<SignUpProvider>(context, listen: false).carForm =
                _carFormImg;
          });
        case 3:
          return setState(() {
            _profileImg = File(image.path);
            Provider.of<SignUpProvider>(context, listen: false).photo =
                _profileImg;
          });

          break;
      }
    });
  }

  Widget _cameraBtn({Function onTap, String label, IconData icon}) {
    return Padding(
      padding: const EdgeInsets.only(right: 5, left: 5),
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
        ),
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white70, width: 1),
                  borderRadius: BorderRadius.circular(15),
                ),
                color: Colors.green,
                onPressed: onTap,
                child: Row(children: [
                  Text(
                    'ارفاق صورة',
                    style: TextStyle(color: Colors.white),
                  ),
                  Icon(Icons.photo, color: Colors.white)
                ]),
              ),
              Expanded(
                  child: Text(
                label,
                textAlign: TextAlign.end,
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ))
            ],
          ),
        ),
      ),
    );
  }

  File _mainImg;
  File _carFormImg;
  File _profileImg;
  File _idImg;
}
