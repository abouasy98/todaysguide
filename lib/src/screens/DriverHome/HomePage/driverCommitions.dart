import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Models/DriverModel/driverComisstionModle.dart';
import 'package:todaysguide/src/provider/DriverProvider/driverCommitionProvider.dart';
import 'package:todaysguide/src/screens/DriverHome/HomePage/widget/driverCommitionCard.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';

import 'widget/total_card.dart';

class DriverCommitions extends StatefulWidget {
  final String url;

  const DriverCommitions({Key key, this.url}) : super(key: key);
  @override
  _DriverCommitionsState createState() => _DriverCommitionsState();
}

class _DriverCommitionsState extends State<DriverCommitions> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text("عمولاتي"),
            centerTitle: true,
            elevation: 0,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(60),
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TabBar(
                      unselectedLabelColor: Colors.black,
                      indicatorSize: TabBarIndicatorSize.label,
                      indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Theme.of(context).primaryColor,
                      ),
                      tabs: [
                        Tab(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: Theme.of(context).primaryColor,
                                  width: 1),
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text("العمولات المدفوعه"),
                            ),
                          ),
                        ),
                        Tab(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: Theme.of(context).primaryColor,
                                  width: 1),
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text("العمولات المستحقة"),
                            ),
                          ),
                        ),
                      ]),
                ),
              ),
            ),
          ),
          body: TabBarView(children: [
            FutureBuilder(
                future: Provider.of<DriverCommitionsProvider>(context,
                        listen: false)
                    .getDriverCommitions(
                        Provider.of<SharedPref>(context, listen: false).token,
                        widget.url,
                        "0",
                        context),
                builder: (c, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return AppLoader();
                    default:
                      if (snapshot.hasError)
                        return Text('Error: ${snapshot.error}');
                      else {
                        List<Commission> filteromision = [];
                        if (snapshot.data.data != null) {
                          for (int i = 0;
                              i < snapshot.data.data[0].commissions.length;
                              i++) {
                            if (snapshot.data.data[0].commissions[i]
                                    .commissionStatus ==
                                "1") {
                              filteromision
                                  .add(snapshot.data.data[0].commissions[i]);
                            }
                          }
                        }
                        return snapshot.data.data == null
                            ? Center(
                                child: Text("لا يوجد عمولات"),
                              )
                            : ListView(
                                children: [
                                  Container(
                                    height: MediaQuery.of(context).size.height /
                                        1.5,
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: filteromision.length,
                                        itemBuilder: (c, index) =>
                                            DriverCommitionCard(
                                              driverCommitions:
                                                  filteromision[index],
                                            )),
                                  ),
                                  TotalCard(
                                    total: snapshot
                                        .data.data[0].totalPaidCommissions,
                                    title: 'إجمالي العمولات المدفوعة',
                                  )
                                ],
                              );
                      }
                  }
                }),
            FutureBuilder(
                future: Provider.of<DriverCommitionsProvider>(context,
                        listen: false)
                    .getDriverCommitions(
                        Provider.of<SharedPref>(context, listen: false).token,
                        widget.url,
                        "1",
                        context),
                builder: (c, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return AppLoader();
                    default:
                      if (snapshot.hasError)
                        return Text('Error: ${snapshot.error}');
                      else {
                        List<Commission> filteromision = [];
                        if (snapshot.data.data != null) {
                          for (int i = 0;
                              i < snapshot.data.data[0].commissions.length;
                              i++) {
                            if (snapshot.data.data[0].commissions[i]
                                    .commissionStatus ==
                                "0") {
                              filteromision
                                  .add(snapshot.data.data[0].commissions[i]);
                            }
                          }
                        }
                        return snapshot.data.data == null
                            ? Center(
                                child: Text("لا يوجد عمولات"),
                              )
                            : ListView(
                                children: [
                                  Container(
                                    height: MediaQuery.of(context).size.height /
                                        1.5,
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: filteromision.length,
                                        itemBuilder: (c, index) =>
                                            DriverCommitionCard(
                                              driverCommitions:
                                                  filteromision[index],
                                            )),
                                  ),
                                  TotalCard(
                                    total: snapshot.data.data[0]
                                            .totalUnpaidCommissions ??
                                        "0",
                                    title: 'إجمالي العمولات المستحقة',
                                  )
                                ],
                              );
                      }
                  }
                })
          ]),
        ));
  }
}
