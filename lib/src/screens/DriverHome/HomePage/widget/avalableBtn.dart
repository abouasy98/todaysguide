import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/DriverProvider/availabiltyProvider.dart';
import 'package:toggle_switch/toggle_switch.dart';

class AvalbleBtn extends StatefulWidget {
  @override
  _AvalbleBtnState createState() => _AvalbleBtnState();
}

class _AvalbleBtnState extends State<AvalbleBtn>
    with SingleTickerProviderStateMixin {
  int isActive;
  Timer timer;

  @override
  void initState() {
    isActive = Provider.of<SharedPref>(context, listen: false).avalable;
    timer = Timer.periodic(Duration(seconds: 60), (Timer t) {
      if (Provider.of<SharedPref>(context, listen: false).avalable == 1) {
        Provider.of<AvailabilityProvider>(context, listen: false)
            .changeAvailable(
                available: 1,
                context: context,
                token: Provider.of<SharedPref>(context, listen: false).token);
      }
    });
    // print(
    //     "Provider ${Provider.of<AuthController>(context, listen: false).userModel.data.available}");
    // print("Provider $isActive");
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 30,
        width: 103,
        child: ToggleSwitch(
          minWidth: 50.0,
          minHeight: 50.0,
          fontSize: 9.0,
          initialLabelIndex:
              Provider.of<SharedPref>(context, listen: false).avalable ?? 0,
          activeBgColor: Colors.green,
          activeFgColor: Colors.white,
          inactiveBgColor: Colors.grey[100],
          inactiveFgColor: Colors.grey[900],
          labels: ['غير متاح', 'متاح'],
          onToggle: (index) {
            print('switched to: $index');
            if (index == 0) {
              Provider.of<AvailabilityProvider>(context, listen: false)
                  .changeAvailable(
                      available: 0,
                      context: context,
                      token:
                          Provider.of<SharedPref>(context, listen: false).token)
                  .then((v) {
                setState(() {
                  Provider.of<SharedPref>(context, listen: false).avalable = 0;
                });
              });
            } else {
              Provider.of<AvailabilityProvider>(context, listen: false)
                  .changeAvailable(
                      available: 1,
                      context: context,
                      token:
                          Provider.of<SharedPref>(context, listen: false).token)
                  .then((v) {
                setState(() {
                  Provider.of<SharedPref>(context, listen: false).avalable =1 ;
                });
              });
            }
          },
        ),
      ),
    );
  }
}
