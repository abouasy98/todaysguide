import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/screens/DriverHome/DriverOrder/driverOrder.dart';
import 'package:todaysguide/src/screens/DriverHome/HomePage/widget/avalableBtn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_app_bar_back_ground.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_btn.dart';
import 'package:todaysguide/src/screens/MainWidgets/driver_data.dart';

import 'driverCommitions.dart';

class DriverHome extends StatefulWidget {
  @override
  _DriverHomeState createState() => _DriverHomeState();
}

class _DriverHomeState extends State<DriverHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(
            MediaQuery.of(context).size.height / 4,
          ),
          child: Stack(
            children: <Widget>[
              CustomAppBarBackGround(
                height: MediaQuery.of(context).size.height / 4,
                widgets: <Widget>[
                  Expanded(
                    child: Text(""),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DriverData(),
              ),
            ],
          )),
      body: ListView(
        children: [
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: [
          //     Text(Provider.of<SharedPref>(context,listen: false).car),
          //   ],
          // ),
          SizedBox(height: 20),
          AvalbleBtn(),
          SizedBox(height: 50),
          Consumer<MapHelper>(builder: (context, snap, _) {
            if (snap.position == null) {
              return Center(
                child: InkWell(
                  onTap: () async {
                    await Provider.of<MapHelper>(context, listen: false)
                        .getLocation();
                  },
                  child: Container(
                    color: Colors.red,
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Shimmer.fromColors(
                        baseColor: Colors.white,
                        highlightColor: Colors.yellow,
                        child: Text(
                          'انت لم تقم بتفعيل خاصية اللوكيشن ،لذا يرجي الضغط لتفعيلها',
                          style: TextStyle(
                              color: Colors.blue, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            } else {
              return SizedBox();
            }
          }),
          SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 20),
            child: CustomBtn(
              txtColor: Colors.black,
              text: "طلبات جديدة",
              onTap: () => Navigator.push(
                  context, MaterialPageRoute(builder: (c) => DriverOrder())),
              color: Colors.white,
              elevtion: 10,
            ),
          ),
          CustomBtn(
              txtColor: Colors.white70,
              text: "عمولاتي",
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (c) => DriverCommitions(
                            url: "driver-commission",
                          ))),
              color: Theme.of(context).primaryColor),
        ],
      ),
    );
  }
}
