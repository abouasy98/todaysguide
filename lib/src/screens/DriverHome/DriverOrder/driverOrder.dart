import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/DriverProvider/DriverOrderProvider.dart';
import 'package:todaysguide/src/screens/DriverHome/DriverOrder/driverOrderDetails.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';

import 'widget/orderCard.dart';

class DriverOrder extends StatefulWidget {
  @override
  _DriverOrderState createState() => _DriverOrderState();
}

class _DriverOrderState extends State<DriverOrder> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text("طلباتي"),
            centerTitle: true,
            elevation: 0,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(60),
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TabBar(
                      unselectedLabelColor: Colors.black,
                      indicatorSize: TabBarIndicatorSize.label,
                      indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Theme.of(context).primaryColor,
                      ),
                      tabs: [
                        Tab(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: Theme.of(context).primaryColor,
                                  width: 1),
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text("الطلبات المكتملة"),
                            ),
                          ),
                        ),
                        Tab(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                  color: Theme.of(context).primaryColor,
                                  width: 1),
                            ),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text("الطلبات النشطة"),
                            ),
                          ),
                        ),
                      ]),
                ),
              ),
            ),
          ),
          body: TabBarView(children: [
            FutureBuilder(
                future: Provider.of<DriverOrdersProvider>(context,
                        listen: false)
                    .getDriverOrders(
                        Provider.of<SharedPref>(context, listen: false).token,
                        "2",
                        context),
                builder: (c, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return AppLoader();
                    default:
                      if (snapshot.hasError)
                        return Text('Error: ${snapshot.error}');
                      else
                        return snapshot.data.data == null
                            ? Center(
                                child: Text("لا يوجد طلبات"),
                              )
                            : ListView.builder(
                                shrinkWrap: true,
                                itemCount: snapshot.data.data.length,
                                itemBuilder: (c, index) => OrderCard(
                                      type: 1,
                                      onTap: () {
                                        Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (c) =>
                                                        DriverOrderDetails(
                                                          order: snapshot
                                                              .data.data[index],
                                                        )))
                                            .then((value) => setState(() {}));
                                      },
                                      createdAt: snapshot
                                          .data.data[index].createdAt
                                          .toString()
                                          .substring(0, 10),
                                      image:
                                          snapshot.data.data[index].shopPhoto ??
                                              "",
                                      orderNum: snapshot.data.data[index].id
                                          .toString(),
                                      placeName: snapshot.data.data[index].shop,
                                      price: snapshot.data.data[index].price ??
                                          "0",
                                    ));
                  }
                }),
            FutureBuilder(
                future: Provider.of<DriverOrdersProvider>(context,
                        listen: false)
                    .getDriverOrders(
                        Provider.of<SharedPref>(context, listen: false).token,
                        "1",
                        context),
                builder: (c, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return AppLoader();
                    default:
                      if (snapshot.hasError)
                        return Text('Error: ${snapshot.error}');
                      else
                        return snapshot.data.data == null
                            ? Center(
                                child: Text("لا يوجد طلبات"),
                              )
                            : ListView.builder(
                                shrinkWrap: true,
                                itemCount: snapshot.data.data.length,
                                itemBuilder: (c, index) => OrderCard(
                                      type: 1,
                                      onTap: () {
                                        Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (c) =>
                                                        DriverOrderDetails(
                                                          orderType: 1,
                                                          order: snapshot
                                                              .data.data[index],
                                                        )))
                                            .then((value) => setState(() {}));
                                      },
                                      createdAt: snapshot
                                          .data.data[index].createdAt
                                          .toString()
                                          .substring(0, 10),
                                      image:
                                          snapshot.data.data[index].shopPhoto ??
                                              "",
                                      orderNum: snapshot.data.data[index].id
                                          .toString(),
                                      placeName: snapshot.data.data[index].shop,
                                      price: snapshot.data.data[index].price ??
                                          "0",
                                    ));
                  }
                })
          ]),
        ));
  }
}
