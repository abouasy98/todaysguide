import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class OrderCard extends StatelessWidget {
  final String placeName;
  final String image;
  final String price;
  final String orderNum;
  final String createdAt;
  final Function onTap;
  final int type;

  const OrderCard(
      {Key key,
      this.placeName,
      this.image,
      this.price,
      this.orderNum,
      this.createdAt,
      this.onTap, this.type})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(right: 8.0, left: 8, top: 8),
        child: InkWell(
          onTap: onTap,
          child: Card(
            elevation: 10,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              "Sr",
                              // style: TextStyle(fontSize: 10),
                            ),
                            Text(
                              price,
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        type == 1? 
                        Container(
                            width: 100,
                            height: 30,
                            decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.circular(20)),
                            child: Center(child: Text("التفاصيل"))):Container()
                      ],
                    ),
                    Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text("رقم الطلب:  $orderNum"),
                            Text(
                              placeName,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 5),
                            Text(createdAt, style: TextStyle(fontSize: 10)),
                          ],
                        ),
                        SizedBox(width: 10),
                        Container(
                          height: 70,
                          width: 70,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: CachedNetworkImageProvider(image))),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
