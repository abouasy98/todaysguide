import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/post/postServiceModel.dart';

import 'package:todaysguide/src/screens/DriverHome/OurService/servicebyId.dart';
import 'package:todaysguide/src/screens/DriverHome/OurService/widget/servicesCard.dart';

class SearchServiceIteams extends StatelessWidget {
  final String query;
  final List<Datum> services;
  final Key key;
  SearchServiceIteams({
    this.query,
    this.services,
    this.key,
  });

  @override
  Widget build(BuildContext context) {
    final List<Datum> suggteionList = query.isEmpty
        ? []
        : services != null
            ? services.where((Datum user) {
                String getUserName = user.name.toLowerCase();
                String _query = query.toLowerCase();
                //  String getTitle = user.title.toLowerCase();
                bool matchUserName = getUserName.contains(_query);
                //  bool matchtitle = getTitle.contains(_query);
                return (matchUserName);
              }).toList()
            : [];

    return ((suggteionList == null) || (suggteionList.length <= 0))
        ? Container(
            height: MediaQuery.of(context).size.height * 0.4,
            child: Center(
              child: Text(
                "لا يوجد نتائج",
                style: TextStyle(color: Colors.black),
              ),
            ),
          )
        : ListView.builder(
            primary: false,
            shrinkWrap: true,
            itemCount: suggteionList.length,
            itemBuilder: ((context, i) {
              Datum searchedUser = Datum(
                createdAt: services[i].createdAt,
                details: services[i].details,
                latitude: services[i].latitude,
                name: services[i].name,
                id: services[i].id,
                longitude: services[i].longitude,
                phoneNumber: services[i].phoneNumber,
                photo: services[i].photo,
                serviceCategory: services[i].serviceCategory,
                serviceCategoryId: services[i].serviceCategoryId,
                state: services[i].state,
                user: services[i].user,
                userId: services[i].userId,
              );

              return ServicesCard(
                ontap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (c) => ServiceById(
                        createdAt: searchedUser.createdAt,
                        details: searchedUser.details,
                        name: searchedUser.name,
                        id: searchedUser.id,
                        lat: searchedUser.latitude,
                        long: searchedUser.longitude,
                        phoneNumber: searchedUser.phoneNumber,
                        photo: searchedUser.photo,
                        serviceCategory: searchedUser.serviceCategory,
                        serviceCategoryId: searchedUser.serviceCategoryId,
                        state: searchedUser.state,
                        user: searchedUser.user,
                        userId: searchedUser.userId,
                      ),
                    ),
                  );
                },
                servicesItem: services[i],
              );
            }));
  }
}
