import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ServicesCard extends StatefulWidget {
  final servicesItem;
  final Function ontap;

  const ServicesCard({Key key, this.servicesItem, this.ontap})
      : super(key: key);

  @override
  _ServicesCardState createState() => _ServicesCardState();
}

class _ServicesCardState extends State<ServicesCard> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Directionality(
      textDirection: TextDirection.rtl,
      child: InkWell(
        onTap: widget.ontap,
        child: Card(
          elevation: 10,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  topLeft: Radius.circular(25),
                  bottomRight: Radius.circular(40),
                  topRight: Radius.circular(40))),
          child: Row(children: [
            Stack(children: [
              CircleAvatar(
                radius: 40,
                backgroundColor: Theme.of(context).primaryColor,
                child: widget.servicesItem.photo != null
                    ? CachedNetworkImage(
                        imageUrl: widget.servicesItem.photo,
                        fadeInDuration: Duration(seconds: 2),
                        placeholder: (context, url) => CircleAvatar(
                            radius: 37,
                            backgroundImage:
                                AssetImage('assets/images/newLogo.png')),
                        imageBuilder: (context, provider) {
                          return CircleAvatar(
                              radius: 37, backgroundImage: provider);
                        },
                      )
                    : CircleAvatar(
                        radius: 37,
                        backgroundImage:
                            AssetImage('assets/images/newLogo.png'),
                      ),
              ),
            ]),
            Container(
              height: mediaQuery.height * 0.1,
              alignment: Alignment.centerRight,
              width: mediaQuery.width - 90,
              child: ListTile(
                leading: Container(
                  width: mediaQuery.width * 0.37,
                  alignment: Alignment.centerRight,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.servicesItem.name,
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                      Expanded(child: Text('')),
                      Text(
                        '${widget.servicesItem.serviceCategory}',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 14),
                      ),
                    ],
                  ),
                ),
                trailing: Container(
                  width: mediaQuery.width * 0.24,
                  alignment: Alignment.topLeft,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.location_on,
                          size: 15,
                        ),
                        Text(
                          '${widget.servicesItem.distance.round()} كم',
                          style: TextStyle(fontSize: 15),
                        ),
                      ]),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
