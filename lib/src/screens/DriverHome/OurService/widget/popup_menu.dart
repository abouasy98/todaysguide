import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:provider/provider.dart';
// import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
// import 'package:todaysguide/src/provider/DriverProvider/MyServicesProvider.dart';

class PopupWidget extends StatefulWidget {
  final List<PopupMenuItem> popList;
  final Function onTap;

  const PopupWidget({Key key, this.popList, this.onTap}) : super(key: key);
  @override
  _PopupWidgetState createState() => _PopupWidgetState();
}

class _PopupWidgetState extends State<PopupWidget> {
  @override
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
        icon: Icon(
          FontAwesomeIcons.filter,
          color: Colors.white,
        ),
        onSelected: widget.onTap,
        itemBuilder: (_) => widget.popList
        // [
        //   PopupMenuItem(
        //     enabled: true,
        //     value: 0,
        //     child: Text(
        //       'إرسال شكوى',
        //       style: TextStyle(color: Colors.black, fontSize: 13),
        //     ),
        //   ),
        //   PopupMenuItem(
        //     enabled: true,
        //     value: 1,
        //     child: Text(
        //       'اتصال',
        //       style: TextStyle(color: Colors.black, fontSize: 13),
        //       textAlign: TextAlign.center,
        //     ),
        //   ),

        // ],
        );
  }

  // onselectpop(v) {
  //   print("$v");
  //   Provider.of<MyServicesProvider>(context, listen: false).getMyServices(
  //       Provider.of<SharedPref>(context, listen: false).token, v, context).then((value) => setState(() {}));
  // }
}
