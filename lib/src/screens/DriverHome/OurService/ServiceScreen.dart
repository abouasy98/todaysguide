import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Models/post/postServiceModel.dart';
import 'package:todaysguide/src/provider/postServiceProvider.dart';
import 'package:todaysguide/src/provider/servicesCatogoryProvider.dart';
import 'package:todaysguide/src/screens/DriverHome/OurService/servicebyId.dart';
import 'package:todaysguide/src/screens/DriverHome/OurService/widget/searchServiceItem.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';
import 'createService.dart';

import 'widget/popup_menu.dart';
import 'widget/servicesCard.dart';

class ServiceScreen extends StatefulWidget {
  @override
  _ServiceScreenState createState() => _ServiceScreenState();
}

class _ServiceScreenState extends State<ServiceScreen> {
  String query = "";
  TextEditingController _searchController = TextEditingController();
  List<PopupMenuItem> popList = [];
  int filterServce;
  List<Datum> services;
  @override
  void initState() {
    if (Provider.of<SharedPref>(context, listen: false).token != null) {
      Provider.of<ServicesCatogoryProvider>(context, listen: false)
          .getServicesCatogory(context)
          .then((value) {
        if (value != null) {
          for (int i = 0;
              i <
                  Provider.of<ServicesCatogoryProvider>(context, listen: false)
                      .services
                      .length;
              i++) {
            popList.add(
              PopupMenuItem(
                enabled: true,
                value: Provider.of<ServicesCatogoryProvider>(context,
                        listen: false)
                    .services[i]
                    .id,
                child: Text(
                  Provider.of<ServicesCatogoryProvider>(context, listen: false)
                      .services[i]
                      .name,
                  style: TextStyle(color: Colors.black, fontSize: 13),
                ),
              ),
            );
          }

          services = Provider.of<PostServicesProvider>(context, listen: false)
              .myServices;
          services.sort(
            (shop1, shop2) => shop2.distance.compareTo(
              shop1.distance,
            ),
          );
        }
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Directionality(
        textDirection: TextDirection.ltr,
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text("دليل الخدمات"),
            leading: PopupWidget(
              popList: popList,
              onTap: (v) {
                setState(() {
                  filterServce = v;
                });
              },
            ),
            actions: [
              Padding(
                padding:
                    const EdgeInsets.only(top: 10.0, bottom: 10, right: 10),
                child: InkWell(
                  onTap: () => Navigator.push(context,
                          MaterialPageRoute(builder: (c) => CreateService()))
                      .then((v) {
                    if (v == true) {
                      setState(() {
                        filterServce = null;
                      });
                    }
                  }),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          "تسجيل المزودين",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 10,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          body: Provider.of<SharedPref>(context, listen: false).token == null
              ? Center(
                  child: Row(
                  textDirection: TextDirection.rtl,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(
                      "لمشاهدة دليل الخدمات يرجي التسجيل اولا",
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (c) => SignInScreen()));
                      },
                      child: Text(
                        "تسجيل الدخول",
                        style: TextStyle(
                            color: Colors.blue, fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ))
              : Consumer<MapHelper>(builder: (context, snap, _) {
                  if (snap.position == null) {
                    return Center(
                      child: InkWell(
                        onTap: () async {
                          await Provider.of<MapHelper>(context, listen: false)
                              .getLocation();
                        },
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              'انت لم تقم بتفعيل خاصية اللوكيشن ،لذا يرجي الضغط لتفعيلها',
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
                    );
                  } else {
                    return ListView(shrinkWrap: true, children: [
                      Column(
                        children: [
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: Container(
                                height: mediaQuery.height * 0.05,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: Color.fromRGBO(201, 202, 207, 0.8),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    right: 8.0,
                                  ),
                                  child: Container(
                                    alignment: Alignment.center,
                                    width: mediaQuery.width,
                                    child: TextFormField(
                                      keyboardType: TextInputType.name,
                                      textCapitalization:
                                          TextCapitalization.sentences,
                                      textInputAction: TextInputAction.search,
                                      controller: _searchController,
                                      onChanged: (val) {
                                        setState(() {
                                          query = val.trim();
                                        });
                                      },
                                      decoration: InputDecoration(
                                        contentPadding:
                                            EdgeInsets.only(top: 8.0),
                                        isDense: true,
                                        hintText: ' البحث عن المزودين',
                                        suffixIcon: Container(
                                          decoration: BoxDecoration(
                                            color: Color.fromRGBO(
                                                234, 237, 244, 0.8),
                                          ),
                                          child: query != ''
                                              ? Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          bottom: 8.0),
                                                  child: IconButton(
                                                    iconSize: 20,
                                                    icon: Icon(
                                                      Icons.close,
                                                      color: Colors.green,
                                                    ),
                                                    onPressed: () {
                                                      WidgetsBinding.instance
                                                          .addPostFrameCallback(
                                                              (_) =>
                                                                  _searchController
                                                                      .clear());
                                                      setState(() {
                                                        query = '';
                                                      });
                                                    },
                                                  ),
                                                )
                                              : Icon(
                                                  Icons.search,
                                                  size: 20,
                                                  color: Colors.green,
                                                ),
                                        ),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                )),
                          ),
                          SizedBox(height: mediaQuery.height * 0.01),
                          query == ''
                              ? FutureBuilder(
                                  future: Provider.of<PostServicesProvider>(
                                          context,
                                          listen: false)
                                      .getMyServices(
                                          Provider.of<SharedPref>(context,
                                                  listen: false)
                                              .token,
                                          filterServce,
                                          context),
                                  builder: (c, snapshot) {
                                    switch (snapshot.connectionState) {
                                      case ConnectionState.waiting:
                                        return AppLoader();
                                      default:
                                        if (snapshot.hasError)
                                          return Center(
                                              child: Text(
                                                  'Error: ${snapshot.error}'));
                                        else if (snapshot.data.data == null) {
                                          return Center(
                                            child: Text("لا يوجد خدمات"),
                                          );
                                        } else {
                                          services =
                                              Provider.of<PostServicesProvider>(
                                                      context,
                                                      listen: false)
                                                  .myServices;
                                          services.sort(
                                            (shop1, shop2) =>
                                                shop1.distance.compareTo(
                                              shop2.distance,
                                            ),
                                          );
                                          return ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: services.length,
                                            itemBuilder: (c, index) =>
                                                ServicesCard(
                                              ontap: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (c) => ServiceById(
                                                      createdAt: services[index]
                                                          .createdAt,
                                                      details: services[index]
                                                          .details,
                                                      name:
                                                          services[index].name,
                                                      id: services[index].id,
                                                      lat: services[index]
                                                          .latitude,
                                                      long: services[index]
                                                          .longitude,
                                                      phoneNumber:
                                                          services[index]
                                                              .phoneNumber,
                                                      photo:
                                                          services[index].photo,
                                                      serviceCategory:
                                                          services[index]
                                                              .serviceCategory,
                                                      serviceCategoryId:
                                                          services[index]
                                                              .serviceCategoryId,
                                                      state:
                                                          services[index].state,
                                                      user:
                                                          services[index].user,
                                                      userId: services[index]
                                                          .userId,
                                                    ),
                                                  ),
                                                );
                                              },
                                              servicesItem: services[index],
                                            ),
                                          );
                                        }
                                    }
                                  })
                              : SearchServiceIteams(
                                  query: query,
                                  services: services,
                                  key: UniqueKey(),
                                ),
                        ],
                      ),
                    ]);
                  }
                }),
        ));
  }
}
