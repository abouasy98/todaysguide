import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/createServiceProvider.dart';
import 'package:todaysguide/src/provider/servicesCatogoryProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_app_bar.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_image_getter.dart';
import 'package:todaysguide/src/screens/MainWidgets/details_text_field.dart';
import 'package:todaysguide/src/screens/MainWidgets/labeled_bottom_sheet.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';
import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';

class CreateService extends StatefulWidget {
  @override
  _CreateServiceState createState() => _CreateServiceState();
}

class _CreateServiceState extends State<CreateService> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    Provider.of<ServicesCatogoryProvider>(context, listen: false)
        .getServicesCatogory(context);
    Future.delayed(Duration(microseconds: 150), () {
      Provider.of<CreateServiceProvider>(context, listen: false).setNull();
    });

    super.initState();
  }

  _getMainImg(ImageSource source, int currentImage) async {
    var image = await ImagePicker().getImage(source: source);
    setState(() {
      return setState(() {
        _mainImg = File(image.path);
        Provider.of<CreateServiceProvider>(context, listen: false).photo =
            _mainImg;
      });
    });
  }

  Widget _cameraBtn({Function onTap, String label, IconData icon}) {
    return Padding(
      padding: const EdgeInsets.only(right: 5, left: 5),
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
        ),
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              RaisedButton(
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white70, width: 1),
                  borderRadius: BorderRadius.circular(15),
                ),
                color: Colors.green,
                onPressed: onTap,
                child: Row(children: [
                  Text(
                    'ارفاق صورة',
                    style: TextStyle(color: Colors.white),
                  ),
                  Icon(Icons.photo, color: Colors.white)
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }

  File _mainImg;

  final _form = GlobalKey<FormState>();
  bool autoError = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          onTap: () => Navigator.pop(context),
          label: "تسجيل بدليل الخدمات",
          iconData: Icons.arrow_back_ios,
        ),
      ),
      body: Provider.of<SharedPref>(context, listen: false).token == null
          ? Center(
              child: Row(
              textDirection: TextDirection.rtl,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  "لمشاهدة الاشعارات يرجي التسجيل اولا",
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (c) => SignInScreen()));
                  },
                  child: Text(
                    "تسجيل الدخول",
                    style: TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ))
          : Form(
              key: _form,
              autovalidateMode: autoError
                  ? AutovalidateMode.always
                  : AutovalidateMode.disabled,
              child: ListView(
                children: <Widget>[
                  SizedBox(height: 20),
                  RegisterTextField(
                    label: ' اسم الجهة',
                    hint: "يكتب هنا اسم الجهة",
                    type: TextInputType.text,
                    onChange: (v) {
                      Provider.of<CreateServiceProvider>(context, listen: false)
                          .placeName = v;
                    },
                  ),
                  LabeledBottomSheet(
                      label: '-- القسم التابع له --',
                      onChange: (v) {
                        Provider.of<CreateServiceProvider>(context,
                                listen: false)
                            .serviceCategoryId = v.id.toString();
                      },
                      data: Provider.of<ServicesCatogoryProvider>(context,
                              listen: true)
                          .bottomSheet),
                  SizedBox(height: 20),
                  DetailsTextField(
                    createService: true,
                    hint: 'يكتب هنا التفاصيل العامة ',
                    onImg: (v) {
                      Provider.of<CreateServiceProvider>(context, listen: false)
                          .photo = v;
                    },
                    onChange: (v) {
                      Provider.of<CreateServiceProvider>(context, listen: false)
                          .details = v;
                    },
                    label: 'التفاصيل',
                  ),
                  SizedBox(height: 20),
                  GetImage().showMainImg(
                    context: context,
                    deleteImage: () {
                      setState(() {
                        _mainImg = null;
                      });
                    },
                    cameraBtn: _cameraBtn(
                        onTap: () => _getMainImg(ImageSource.gallery, 3),
                        icon: Icons.camera_alt,
                        label: "صورة المتجر"),
                    mainImg: _mainImg,
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SignInButton(
                      textSize: 15,
                      btnWidth: MediaQuery.of(context).size.width - 20,
                      btnHeight: 45,
                      onPressSignIn: () {
                        setState(() {
                          autoError = true;
                        });
                        final isValid = _form.currentState.validate();
                        if (!isValid) {
                          return;
                        }
                        _form.currentState.save();
                        Provider.of<CreateServiceProvider>(context,
                                listen: false)
                            .createService(
                                Provider.of<SharedPref>(context, listen: false)
                                    .token,
                                0,
                                context);
                      },
                      txtColor: Colors.white,
                      btnColor: Theme.of(context).primaryColor,
                      buttonText: "تسجيل",
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
