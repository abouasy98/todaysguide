import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_app_bar_back_ground%20copy.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class ServiceById extends StatelessWidget {
  final int id;
  final int userId;
  final String user;
  final String name;
  final String details;
  final String phoneNumber;
  final String photo;
  final String lat;
  final String long;
  final int state;
  final int serviceCategoryId;
  final String serviceCategory;
  final DateTime createdAt;
  ServiceById(
      {this.createdAt,
      this.details,
      this.id,
      this.lat,
      this.long,
      this.name,
      this.phoneNumber,
      this.photo,
      this.serviceCategory,
      this.serviceCategoryId,
      this.state,
      this.user,
      this.userId});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(
            MediaQuery.of(context).size.height / 4,
          ),
          child: Stack(
            children: <Widget>[
              CustomAppBarBackGround(
                height: MediaQuery.of(context).size.height / 4,
                widgets: <Widget>[
                  Expanded(
                    child: Text(""),
                  ),
                ],
              ),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 45,
                      ),
                      Text(
                        "الحساب الشخصي",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color:Colors.white,
                                      width: 5)),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    photo != null
                                        ? CachedNetworkImage(
                                            imageUrl: photo,
                                            fadeInDuration:
                                                Duration(seconds: 2),
                                            placeholder: (context, url) =>
                                                CircleAvatar(
                                                    radius: 45,
                                                    backgroundImage: AssetImage(
                                                        'assets/images/avatar.jpeg')),
                                            imageBuilder: (context, provider) {
                                              return CircleAvatar(
                                                  radius: 45,
                                                  backgroundImage: provider);
                                            },
                                          )
                                        : CircleAvatar(
                                            radius: 45,
                                            backgroundImage: AssetImage(
                                                "assets/images/avatar.jpeg")),
                                  ]))
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            user,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          )
                        ],
                      ),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, top: 40),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ))),
              )
            ],
          )),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                        alignment: Alignment.centerRight,
                        width: MediaQuery.of(context).size.width * 0.55,
                        child: Text(
                          phoneNumber,
                          style: TextStyle(fontSize: 15, color: Colors.grey),
                        )),
                    Expanded(child: Text('')),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.35,
                      alignment: Alignment.centerRight,
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Text(
                        ' : رقم الجوال',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Divider(
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                        alignment: Alignment.centerRight,
                        width: MediaQuery.of(context).size.width * 0.55,
                        child: Text(
                          name,
                          style: TextStyle(fontSize: 15, color: Colors.grey),
                        )),
                    Expanded(child: Text('')),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.35,
                      alignment: Alignment.centerRight,
                      child: Text(
                        ' : الجهة التابع لها',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Divider(
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                        alignment: Alignment.centerRight,
                        width: MediaQuery.of(context).size.width * 0.55,
                        child: Text(
                          serviceCategory,
                          style: TextStyle(fontSize: 15, color: Colors.grey),
                        )),
                    Expanded(child: Text('')),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.35,
                      alignment: Alignment.centerRight,
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Text(
                        ' : القسم',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Divider(
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.35,
                      alignment: Alignment.centerRight,
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Text(
                        ' : التفاصيل',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                        padding: const EdgeInsets.only(left:16.0),
                      width: MediaQuery.of(context).size.width*0.9,
                      alignment: Alignment.centerRight,
                      child: Text(

                        details,
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                ),
                SizedBox(
                  height: 20,
                ),
                SignInButton(
                  txtColor: Colors.white,
                  onPressSignIn: () {
                    UrlLauncher.launch('tel:+$phoneNumber');
                  },
                  btnWidth: MediaQuery.of(context).size.width * 0.9,
                  btnHeight: 45,
                  btnColor: Theme.of(context).primaryColor,
                  buttonText: 'اتصل الان',
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
          ],
        ),
      ),
    );
  }
}
