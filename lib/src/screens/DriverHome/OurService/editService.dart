import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/Widgets/ImagePicker/image_picker_handler.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Models/get/MyServicesModle.dart';
import 'package:todaysguide/src/provider/editServiceProvider.dart';
import 'package:todaysguide/src/provider/servicesCatogoryProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_app_bar_back_ground%20copy.dart';
import 'package:todaysguide/src/screens/MainWidgets/details_text_field.dart';
import 'package:todaysguide/src/screens/MainWidgets/labeled_bottom_sheet.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';

class EditService extends StatefulWidget {
  final MyServicesItem servicesItem;

  const EditService({Key key, this.servicesItem}) : super(key: key);
  @override
  _EditServiceState createState() => _EditServiceState();
}

class _EditServiceState extends State<EditService>
    with TickerProviderStateMixin, ImagePickerListener {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  AnimationController _controller;
  ImagePickerHandler imagePicker;

  @override
  void initState() {
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    imagePicker = new ImagePickerHandler(
        this, _controller, Color.fromRGBO(12, 169, 149, 1));
    imagePicker.init();
    super.initState();
  }

  File _image;
  final _form = GlobalKey<FormState>();
  bool autoError = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _globalKey,
        body: Form(
          key: _form,
          autovalidateMode:
              autoError ? AutovalidateMode.always : AutovalidateMode.disabled,
          child: Stack(children: [
            Center(
              child: Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * .25),
                child: ListView(
                  children: <Widget>[
                    SizedBox(height: 20),
                    RegisterTextField(
                      label: ' اسم الجهة',
                      hint: "يكتب هنا اسم الجهة",
                      type: TextInputType.text,
                      init: widget.servicesItem.name,
                      onChange: (v) {
                        Provider.of<EditServiceProvider>(context, listen: false)
                            .placeName = v;
                      },
                    ),
                    LabeledBottomSheet(
                        label: '-- ${widget.servicesItem.serviceCategory} --',
                        onChange: (v) {
                          Provider.of<EditServiceProvider>(context,
                                  listen: false)
                              .serviceCategoryId = v.id.toString();
                        },
                        data: Provider.of<ServicesCatogoryProvider>(context,
                                listen: true)
                            .bottomSheet),
                    SizedBox(height: 20),
                    DetailsTextField(
                      createService: true,
                      hint: 'يكتب هنا التفاصيل العامة ',
                      init: widget.servicesItem.details,
                      onChange: (v) {
                        Provider.of<EditServiceProvider>(context, listen: false)
                            .details = v;
                      },
                      label: 'التفاصيل',
                    ),
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SignInButton(
                        textSize: 15,
                        btnWidth: MediaQuery.of(context).size.width - 20,
                        btnHeight: 45,
                        onPressSignIn: () {
                          setState(() {
                            autoError = true;
                          });
                          final isValid = _form.currentState.validate();
                          if (!isValid) {
                            return;
                          }
                          _form.currentState.save();
                          Provider.of<EditServiceProvider>(context,
                                  listen: false)
                              .createService(
                                  Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token,
                                  widget.servicesItem.id,
                                  context);
                        },
                        txtColor: Colors.white,
                        btnColor: Theme.of(context).primaryColor,
                        buttonText: "تعديل",
                      ),
                    ),
                  ],
                ),
              ),
            ),
            CustomAppBarBackGround(
              widgets: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                Expanded(
                  child: Container(
                    height: 60,
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 16.0),
                      child: Text(
                        "تعديل بدليل الخدمات",
                        // "تعديل حسابي",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * .05),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.white, width: 5)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        InkWell(
                          onTap: () => imagePicker.showDialog(context),
                          child: _image == null
                              ? CachedNetworkImage(
                                  imageUrl: widget.servicesItem.photo,
                                  fadeInDuration: Duration(seconds: 2),
                                  placeholder: (context, url) => CircleAvatar(
                                      radius: 60,
                                      backgroundImage: AssetImage(
                                          'assets/images/avatar.jpeg')),
                                  imageBuilder: (context, provider) {
                                    return CircleAvatar(
                                        radius: 60, backgroundImage: provider);
                                  },
                                )
                              : Container(
                                  child: CircleAvatar(
                                    radius: 60,
                                    backgroundImage: Image.file(_image).image,
                                  ),
                                ),
                        ),
                        Positioned(
                          bottom: 5,
                          right: 10,
                          child: InkWell(
                            onTap: () => imagePicker.showDialog(context),
                            child: CircleAvatar(
                              child: Icon(
                                Icons.add,
                                color: Colors.black,
                                size: 15,
                              ),
                              radius: 10,
                              backgroundColor: Colors.white,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  @override
  userImage(File _image) {
    setState(() {
      this._image = _image;
      Provider.of<EditServiceProvider>(context, listen: false).photo = _image;
    });
  }
}
