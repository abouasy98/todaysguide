import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/DriverProvider/MyServicesProvider.dart';
import 'package:todaysguide/src/provider/deleteServiceProvider.dart';
import 'package:todaysguide/src/provider/servicesCatogoryProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';
import 'createService.dart';
import 'editService.dart';
import 'widget/servicesCard.dart';

class OurService extends StatefulWidget {
  @override
  _OurServiceState createState() => _OurServiceState();
}

class _OurServiceState extends State<OurService> {
  List<PopupMenuItem> popList = [];
  int filterServce;

  @override
  void initState() {
    if (Provider.of<SharedPref>(context, listen: false).token != null) {
      Provider.of<ServicesCatogoryProvider>(context, listen: false)
          .getServicesCatogory(context)
          .then((value) {
        if (value != null) {
          for (int i = 0;
              i <
                  Provider.of<ServicesCatogoryProvider>(context, listen: false)
                      .services
                      .length;
              i++) {
            popList.add(
              PopupMenuItem(
                enabled: true,
                value: Provider.of<ServicesCatogoryProvider>(context,
                        listen: false)
                    .services[i]
                    .id,
                child: Text(
                  Provider.of<ServicesCatogoryProvider>(context, listen: false)
                      .services[i]
                      .name,
                  style: TextStyle(color: Colors.black, fontSize: 13),
                ),
              ),
            );
          }
        }
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("دليل الخدمات"),
          // leading: PopupWidget(
          //   popList: popList,
          //   onTap: (v) {
          //     setState(() {
          //       filterServce = v;
          //     });
          //   },
          // ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 10, right: 10),
              child: InkWell(
                onTap: () => Navigator.push(context,
                        MaterialPageRoute(builder: (c) => CreateService()))
                    .then((v) {
                  if (v == true) {
                    setState(() {
                      filterServce = null;
                    });
                  }
                }),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(
                        "تسجيل المزودين",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 10,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
        body: Provider.of<SharedPref>(context, listen: false).token == null
            ? Center(
                child: Row(
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    "لمشاهدة دليل الخدمات يرجي التسجيل اولا",
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (c) => SignInScreen()));
                    },
                    child: Text(
                      "تسجيل الدخول",
                      style: TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ))
            : FutureBuilder(
                future: Provider.of<MyServicesProvider>(context, listen: false)
                    .getMyServices(
                        Provider.of<SharedPref>(context, listen: false).token,
                        filterServce,
                        context),
                builder: (c, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return AppLoader();
                    default:
                      if (snapshot.hasError)
                        return Center(child: Text('Error: ${snapshot.error}'));
                      else
                        return snapshot.data.data == null
                            ? Center(
                                child: Text("لا يوجد خدمات"),
                              )
                            : ListView.builder(
                                shrinkWrap: true,
                                itemCount: Provider.of<MyServicesProvider>(
                                        context,
                                        listen: false)
                                    .myServices
                                    .length,
                                itemBuilder: (c, index) => Slidable(
                                      actionPane: SlidableDrawerActionPane(),
                                      actionExtentRatio: 0.25,
                                      child: ServicesCard(
                                        ontap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (c) => EditService(
                                                        servicesItem: Provider.of<
                                                                    MyServicesProvider>(
                                                                context,
                                                                listen: false)
                                                            .myServices[index],
                                                      ))).then((value) {
                                            setState(() {});
                                          });
                                        },
                                        servicesItem:
                                            Provider.of<MyServicesProvider>(
                                                    context,
                                                    listen: false)
                                                .myServices[index],
                                      ),
                                      actions: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            Provider.of<DeleteServiceProvider>(
                                                    context,
                                                    listen: false)
                                                .deletNot(
                                                    Provider.of<SharedPref>(
                                                            context,
                                                            listen: false)
                                                        .token,
                                                    Provider.of<MyServicesProvider>(
                                                            context,
                                                            listen: false)
                                                        .myServices[index]
                                                        .id,
                                                    context)
                                                .then((v) {
                                              setState(() {});
                                              // _getShared();
                                            });
                                          },
                                          child: Material(
                                            shape: CircleBorder(),
                                            color: Colors.redAccent,
                                            child: Padding(
                                              padding: const EdgeInsets.all(15),
                                              child: Icon(Icons.delete,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ));
                  }
                }),
      ),
    );
  }
}
