import 'package:flutter/material.dart';
import 'package:todaysguide/src/screens/HomePages/Notification/notification.dart';
import 'package:todaysguide/src/screens/HomePages/more/moreScreen.dart';
import 'HomePage/driverHome.dart';
import 'OurService/ServiceScreen.dart';

class MainPageDriver extends StatefulWidget {
  @override
  _MainPageDriverState createState() => _MainPageDriverState();
}

class _MainPageDriverState extends State<MainPageDriver> {
  bool isInit = true;
  int _selectedpage = 1;
  List<Map<String, Object>> pages;
  @override
  void initState() {
    super.initState();
    pages = [
      {
        'page': MoreScreen(),
      },
      {
        'page': DriverHome(),
      },
      {
        'page': ServiceScreen(),
      },
      {
        'page': AppNotification(),
      },
    ];
  }

  void selectedpage(int index) {
    setState(() {
      _selectedpage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: pages[_selectedpage]['page'],
        bottomNavigationBar: Card(
          elevation: 10,
          margin: EdgeInsets.only(bottom: 0.0),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40), topRight: Radius.circular(40))),
          child: BottomNavigationBar(
            elevation: 10,
            backgroundColor: Colors.white,
            onTap: selectedpage,
            currentIndex: _selectedpage,
            type: BottomNavigationBarType.fixed,
            selectedItemColor: Theme.of(context).primaryColor,
             selectedLabelStyle:
                TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
            unselectedLabelStyle:
                TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
            items: [
              BottomNavigationBarItem(
                label: "حسابي",
                activeIcon: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: ImageIcon(
                      AssetImage("assets/icons/user.png"),
                      color: Theme.of(context).primaryColor,
                      size: 20,
                    ),
                  ),
                ),
                icon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/user.png"),
                    size: 20,
                  ),
                ),
              ),
              BottomNavigationBarItem(
                label: "الرئيسية",
                activeIcon: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: ImageIcon(
                      AssetImage("assets/icons/home.png"),
                      color: Theme.of(context).primaryColor,
                      size: 20,
                    ),
                  ),
                ),
                icon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/home.png"),
                    size: 20,
                  ),
                ),
              ),
              BottomNavigationBarItem(
                label: "دليل الخدمات",
                activeIcon: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: ImageIcon(
                      AssetImage("assets/icons/00000-00.png"),
                      color: Theme.of(context).primaryColor,
                      size: 20,
                    ),
                  ),
                ),
                icon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/00000-00.png"),
                    size: 20,
                  ),
                ),
              ),
              BottomNavigationBarItem(
                label: "الاشعارات",
                activeIcon: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: ImageIcon(
                      AssetImage("assets/icons/alarm.png"),
                      color: Theme.of(context).primaryColor,
                      size: 20,
                    ),
                  ),
                ),
                icon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/alarm.png"),
                    size: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}
