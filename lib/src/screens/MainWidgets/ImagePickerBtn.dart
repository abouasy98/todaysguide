import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/DriverProvider/editCarDataProvider.dart';
import 'package:todaysguide/src/screens/Intro/waitingScreen.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'customImageNetworkGetter.dart';

class ImagePickerBtn extends StatefulWidget {
  @override
  _ImagePickerBtnState createState() => _ImagePickerBtnState();
}

class _ImagePickerBtnState extends State<ImagePickerBtn> {
  File _carFormImg;
  File _idImg;
  File _carImg;
  List list;

  _showDialog(String text, BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            elevation: 3,
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(15),
            children: <Widget>[
              Container(
                height: 50,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Text(
                      text,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        Provider.of<EditCarDataProvider>(context, listen: false)
                            .changeCarData(
                                Provider.of<SharedPref>(context, listen: false)
                                    .token,
                                context);
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (context) => WaitingAccepting(),
                          ),
                          (Route<dynamic> route) => false,
                        );
                      },
                      child: Container(
                          width: 50,
                          height: 30,
                          child: Center(child: Text("متاكد")))),
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                          width: 50,
                          height: 30,
                          child: Center(child: Text("الغاء"))))
                ],
              )
            ],
          );
        });
  }

  bottomCameraSheet(int index) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (_) {
        return Material(
          type: MaterialType.transparency,
          child: Opacity(
            opacity: 1.0,
            child: Container(
              padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 20.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      _getMainImg(ImageSource.camera, index);
                    },
                    child: _roundedButton(
                      label: "Camera",
                      bgColor: Theme.of(context).primaryColor,
                      txtColor: Colors.white,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      _getMainImg(ImageSource.gallery, index);
                    },
                    child: _roundedButton(
                      label: "Gallery",
                      bgColor: Theme.of(context).primaryColor,
                      txtColor: Colors.white,
                    ),
                  ),
                  SizedBox(height: 15.0),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: new Padding(
                      padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
                      child: _roundedButton(
                        label: "Cancel",
                        bgColor: Colors.black,
                        txtColor: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _roundedButton({String label, Color bgColor, Color txtColor}) {
    return Container(
      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      padding: EdgeInsets.all(15.0),
      alignment: FractionalOffset.center,
      decoration: new BoxDecoration(
        color: bgColor,
        borderRadius: new BorderRadius.all(const Radius.circular(100.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: const Color(0xFF696969),
            offset: Offset(1.0, 6.0),
            blurRadius: 0.001,
          ),
        ],
      ),
      child: Text(
        label,
        style: new TextStyle(
          color: txtColor,
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  _getMainImg(ImageSource source, int currentImage) async {
    var image = await ImagePicker().getImage(source: source);
    setState(() {
      switch (currentImage) {
        case 0:
          return setState(() {
            _carImg = File(image.path);

            Provider.of<EditCarDataProvider>(context, listen: false).identity =
                _carImg;
          });
        case 1:
          return setState(() {
            _idImg = File(image.path);
            Provider.of<EditCarDataProvider>(context, listen: false).license =
                _idImg;
          });
        case 2:
          return setState(() {
            _carFormImg = File(image.path);
            Provider.of<EditCarDataProvider>(context, listen: false).carForm =
                _carFormImg;
          });

          break;
      }
    });
  }

  Widget _oneImgFile(File img, String image, int index) {
    print("FileImage=$img");
    print("StringImage=$image");
    return Padding(
      padding: const EdgeInsets.only(left: 10),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.3,
            child: GetImageNetwork().showMainImg(
              context: context,
              editDriver: true,
              mainImg: img,
              mainImgNetwork: image,
            ),
          ),
          Positioned(
            bottom: 10,
            right: 0,
            child: InkWell(
              onTap: () {
                setState(() {
                  list[index] = null;
                  image = null;
                  img = null;
                });
                bottomCameraSheet(index);
              },
              child: Material(
                color: Colors.green,
                elevation: 2,
                shape: CircleBorder(),
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Icon(
                    Icons.add,
                    size: 18,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    list = [
      Provider.of<SharedPref>(context, listen: false).identity,
      Provider.of<SharedPref>(context, listen: false).license,
      Provider.of<SharedPref>(context, listen: false).carForm,
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 10, left: 10),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          SizedBox(height: 10),
          Directionality(
            textDirection: TextDirection.rtl,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.3,
              child: Column(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height * 0.18,
                    child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.15,
                                child: _oneImgFile(
                                    _carImg != null ? _carImg : null,
                                    list[0] != null ? list[0] : null,
                                    0),
                              ),
                              Text(
                                'صورة الهوية',
                                style: TextStyle(fontSize: 15),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.15,
                                child: _oneImgFile(
                                  _idImg != null ? _idImg : null,
                                  list[1] != null ? list[1] : null,
                                  1,
                                ),
                              ),
                              Text(
                                'صورة الاستمارة',
                                style: TextStyle(fontSize: 15),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.15,
                                child: _oneImgFile(
                                    _carFormImg != null ? _carFormImg : null,
                                    list[2] != null ? list[2] : null,
                                    2),
                              ),
                              Text(
                                'استمارة السيارة',
                                style: TextStyle(fontSize: 15),
                              ),
                            ],
                          ),
                        ]),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SignInButton(
                    txtColor: Colors.white,
                    onPressSignIn: () {
                      if (_idImg == null &&
                          _carFormImg == null &&
                          _carImg == null) {
                        CustomAlert().toast(
                          context: context,
                          title: "لم يتم اضافة اي تعديل",
                        );

                        return;
                      }
                      _showDialog(
                          "عند تعديل بيانات السيارة سيتم ايقاف الحساب لحين الموافقة من الادارة",
                          context);
                    },
                    btnWidth: MediaQuery.of(context).size.width - 40,
                    btnHeight: MediaQuery.of(context).size.height * .07,
                    btnColor: Theme.of(context).primaryColor,
                    buttonText: " تعديل بيانات السيارة",
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
