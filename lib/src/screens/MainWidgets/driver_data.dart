import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';

class DriverData extends StatefulWidget {
  @override
  _DriverDataState createState() => _DriverDataState();
}

class _DriverDataState extends State<DriverData> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 40,
        ),
        Text(
          "مرحبا بك",
          style: TextStyle(color: Colors.white),
        ),
        Text(Provider.of<SharedPref>(context, listen: false).name,
            style: TextStyle(color: Colors.white)),
        SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                        color: Theme.of(context).primaryColor, width: 5)),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Provider.of<SharedPref>(context, listen: false)
                                  .photo !=
                              null
                          ? CachedNetworkImage(
                              imageUrl: Provider.of<SharedPref>(context,
                                      listen: false)
                                  .photo,
                              fadeInDuration: Duration(seconds: 2),
                              placeholder: (context, url) => CircleAvatar(
                                  radius: 45,
                                  backgroundImage: AssetImage(
                                      'assets/images/avatar.jpeg')),
                              imageBuilder: (context, provider) {
                                return CircleAvatar(
                                    radius: 45, backgroundImage: provider);
                              },
                            )
                          : CircleAvatar(
                              radius: 45,
                              backgroundImage:
                                  AssetImage("assets/images/avatar.jpeg")),
                    ]))
          ],
        )
      ],
    );
  }
}
