import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';

class UserData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 70,
            width: 70,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: CachedNetworkImage(
                imageUrl:
                    Provider.of<SharedPref>(context, listen: false).photo ?? "",
                errorWidget: (context, url, error) => ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.asset('assets/images/avatar.jpeg',
                        fit: BoxFit.cover)),
                fadeInDuration: Duration(seconds: 2),
                placeholder: (context, url) => ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.asset('assets/images/avatar.jpeg',
                        fit: BoxFit.cover)),
                imageBuilder: (context, provider) {
                  return ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image(
                        image: provider,
                        fit: BoxFit.cover,
                      ));
                },
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                  Provider.of<SharedPref>(context, listen: false).name ??
                      "محمد عبدالرحمن",
                  style: TextStyle(color: Colors.black)),
              RatingBar.builder(
                itemBuilder: (_, index) {
                  return Icon(Icons.star);
                },
                initialRating: 3,
                itemSize: 15.0,
                allowHalfRating: true,
                itemCount: 5,
                // itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                onRatingUpdate: null,
              ),
            ],
          ),
    
        ],
      ),
    );
  }
}
