import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:todaysguide/src/provider/get/categoriesProvider.dart';

// ignore: must_be_immutable
class CategoryCard extends StatefulWidget {
  final List<Categories> list;
  String category;
 CategoryCard({Key key, this.list, this.category}) : super(key: key);

  @override
  _CategoryCardState createState() => _CategoryCardState();
}

class _CategoryCardState extends State<CategoryCard> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(left: 5, right: 5),
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        physics: ScrollPhysics(),
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          return AnimationConfiguration.staggeredList(
            position: index,
            delay: Duration(milliseconds: 500),
            child: SlideAnimation(
              horizontalOffset: 500,
              child: FadeInAnimation(
                child: Padding(
                  padding: const EdgeInsets.only(right: 5, left: 5),
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        for (int i = 0; i < widget.list.length; i++) {
                          setState(() {
                            widget.list[i].selected = false;
                          });
                        }
                        setState(() {
                          widget.list[index].selected =
                              !widget.list[index].selected;
                          widget.category = widget.list[index].name;
                        });

                        print("category name : ${    widget.category}");
                        //    widget.list[index].selectedwidget=widget.list[index].id;
                      });
                    },
                    child: Container(
                      height: mediaQuery.height * 0.07,
                      decoration: BoxDecoration(
                        color: widget.list[index].selected
                            ? Theme.of(context).primaryColor
                            : Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      padding: EdgeInsets.only(left: 8, right: 8),
                      child: Center(
                          child: Text(
                        widget.list[index].name,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: widget.list[index].selected
                                ? Colors.white
                                : Colors.black,
                            fontSize: 12),
                      )),
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
