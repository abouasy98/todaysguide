import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomOptionCard {
  Widget optionCard({String label, final icon, Function onTap}) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: Padding(
        padding: EdgeInsets.only(top: 5,right: 8,left: 8),
        child: InkWell(
          onTap: onTap,
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 255, 255, 1),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8.0, left: 8),
                    child: Text(
                      label,
                      textAlign: TextAlign.end,
                      style: TextStyle(color: Colors.black87, fontSize: 17),
                    ),
                  ),
                ),
                Visibility(
                  visible: icon != null,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(63, 99, 237, 1.0),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      child: Image.asset(icon),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
