import 'package:flutter/material.dart';

import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';

import 'customBtn.dart';
import 'custom_bottom_sheet.dart';

class ReportBtn extends StatefulWidget {
  final BuildContext context;
  final Function func;
  const ReportBtn({Key key, this.context,this.func}) : super(key: key);

  @override
  _ReportBtnState createState() => _ReportBtnState();
}

class _ReportBtnState extends State<ReportBtn> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30),
      child: CustomBtn(
        color: Colors.red,
        onTap: () => CustomBottomSheet().show(
            context: widget.context,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  RegisterTextField(
                    onChange: widget.func,
                    label: 'سبب الابلاغ',
                    icon: Icons.label,
                    type: TextInputType.text,
                  ),
                  SizedBox(height: 20),
                  CustomBtn(
                    text: 'تاكيد الابلاغ',
                    color: Colors.red,
                    onTap: () {
                      Navigator.pop(widget.context);
                    },
                    txtColor: Colors.white,
                  )
                ],
              ),
            )),
        text: 'إلغاء الطلب',
        txtColor: Colors.white,
      ),
    );
  }
}
