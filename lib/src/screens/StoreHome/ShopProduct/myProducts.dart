import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/Shop/MyProductProvider.dart';
import 'package:todaysguide/src/provider/Shop/deleteProductProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/StoreHome/widget/productCard.dart';
import 'package:todaysguide/src/screens/MainWidgets/customScrollPhysics.dart';
import 'createProduct.dart';
import 'editProduct.dart';

class MyProducts extends StatefulWidget {
  @override
  _MyProductsState createState() => _MyProductsState();
}

class _MyProductsState extends State<MyProducts> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("منتجاتي"),
        centerTitle: true,
        actions: [
          InkWell(
            onTap: () => Navigator.push(
                    context, MaterialPageRoute(builder: (c) => CreateProduct()))
                .then((v) {
              setState(() {});
            }),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(Icons.add),
            ),
          )
        ],
      ),
      body: FutureBuilder(
          future: Provider.of<ShopProuctProvider>(context, listen: false)
              .getDriverOrders(
                  Provider.of<SharedPref>(context, listen: false).token,
                  "",
                  context),
          builder: (c, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return AppLoader();
              default:
                if (snapshot.hasError)
                  return Center(child: Text('Error: ${snapshot.error}'));
                else
                  return snapshot.data.data == null
                      ? Center(
                          child: Text("لا يوجد منتجات"),
                        )
                      : ListView.builder(
                          shrinkWrap: true,
                          physics: CustomScrollPhysics(),
                          itemCount: Provider.of<ShopProuctProvider>(context,
                                  listen: false)
                              .shopProduct
                              .length,
                          itemBuilder: (c, index) => Slidable(
                                actionPane: SlidableDrawerActionPane(),
                                actionExtentRatio: 0.25,
                                child: Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Productcard(
                                    onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (c) => EditProduct(
                                                  shopProduct: Provider.of<
                                                              ShopProuctProvider>(
                                                          context,
                                                          listen: false)
                                                      .shopProduct[index],
                                                ))).then((v) {
                                      setState(() {});
                                    }),
                                    shopProduct:
                                        Provider.of<ShopProuctProvider>(context,
                                                listen: false)
                                            .shopProduct[index],
                                  ),
                                ),
                                actions: <Widget>[
                                  InkWell(
                                    onTap: () {
                                      Provider.of<DeleteProductProvider>(
                                              context,
                                              listen: false)
                                          .deletNot(
                                              Provider.of<SharedPref>(context,
                                                      listen: false)
                                                  .token,
                                              Provider.of<ShopProuctProvider>(
                                                      context,
                                                      listen: false)
                                                  .shopProduct[index]
                                                  .id,
                                              context)
                                          .then((v) {
                                        setState(() {});
                                        // _getShared();
                                      });
                                    },
                                    child: Material(
                                      shape: CircleBorder(),
                                      color: Colors.redAccent,
                                      child: Padding(
                                        padding: const EdgeInsets.all(15),
                                        child: Icon(Icons.delete,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ],
                              ));
            }
          }),
    );
  }
}
