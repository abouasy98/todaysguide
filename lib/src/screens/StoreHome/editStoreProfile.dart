import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Helpers/Widgets/ImagePicker/image_picker_handler.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/changeData/changePhoneProvider.dart';
import 'package:todaysguide/src/provider/changeData/editAcountProvider.dart';
import 'package:todaysguide/src/provider/get/RegionsProvider.dart';
import 'package:todaysguide/src/provider/get/citiesProvider.dart';
import 'package:todaysguide/src/provider/get/departmentsProvider.dart';
import 'package:todaysguide/src/provider/termsProvider.dart';
import 'package:todaysguide/src/screens/HomePages/more/internal/editData/editPassword.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_app_bar_back_ground.dart';
import 'package:todaysguide/src/screens/MainWidgets/labeled_bottom_sheet.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';

class EditStoreProfile extends StatefulWidget {
  @override
  _EditStoreProfileState createState() => _EditStoreProfileState();
}

class _EditStoreProfileState extends State<EditStoreProfile>
    with TickerProviderStateMixin, ImagePickerListener {
  SharedPreferences _preferences;

  ImageProvider imageProvider = AssetImage("assets/avatar.jpg");
  EditUserDataProvider editUserDataProvider;
  File _image;

  AnimationController _controller;
  ImagePickerHandler imagePicker;
  bool city = false;

  @override
  void initState() {
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    imagePicker = new ImagePickerHandler(
        this, _controller, Color.fromRGBO(12, 169, 149, 1));
    imagePicker.init();
    super.initState();
  }

  bool isInit = true;
  @override
  void didChangeDependencies() async {
    if (isInit) {
      _preferences = await SharedPreferences.getInstance();
      _preferences.getString("photo");

      Provider.of<TermsProvider>(context, listen: false).getTerms(context);
      Provider.of<RegionsProvider>(context, listen: false).getRegions(context);
      Provider.of<DepartMentProvider>(context, listen: false)
          .getDepartements(context);
      editUserDataProvider =
          Provider.of<EditUserDataProvider>(context, listen: false);

      isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: Stack(children: [
          Center(
            child: Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * .25),
              child: ListView(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                children: <Widget>[
                  SizedBox(height: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      RegisterTextField(
                        icon: Icons.person,
                        onChange: (v) {
                          Provider.of<EditUserDataProvider>(context,
                                  listen: false)
                              .name = v;
                        },
                        init: Provider.of<SharedPref>(context, listen: false)
                            .name,
                        label: "اسم المستخدم",
                        type: TextInputType.text,
                      ),
                      SizedBox(height: 10),
                      LabeledBottomSheet(
                        label: Provider.of<SharedPref>(context, listen: false)
                            .region,
                        onChange: (v) {
                          // Provider.of<SignUpProvider>(context, listen: false).regionId =
                          //     v.id.toString();
                          Provider.of<CitiesProvider>(context, listen: false)
                              .getCities(v.id.toString(), context);
                          setState(() {
                            city = true;
                          });
                        },
                        data:
                            Provider.of<RegionsProvider>(context, listen: true)
                                .bottomSheet,
                      ),
                      SizedBox(height: 10),
                      LabeledBottomSheet(
                        label: Provider.of<SharedPref>(context, listen: false)
                            .city,
                        onChange: (v) {
                          Provider.of<EditUserDataProvider>(context,
                                  listen: false)
                              .cityId = v.id.toString();
                        },
                        ontap: city,
                        data: Provider.of<CitiesProvider>(context, listen: true)
                            .cotiesSheet,
                      ),
                      SizedBox(height: 10),
                      LabeledBottomSheet(
                        label: Provider.of<SharedPref>(context, listen: false)
                            .department,
                        onChange: (v) {
                          Provider.of<EditUserDataProvider>(context,
                                  listen: false)
                              .departMentId = v.id.toString();
                        },
                        data: Provider.of<DepartMentProvider>(context,
                                listen: true)
                            .cotiesSheet,
                      ),
                      SizedBox(height: 10),
                      SignInButton(
                        txtColor: Colors.white,
                        onPressSignIn: () {
                          Provider.of<EditUserDataProvider>(context,
                                  listen: false)
                              .changeUserData(
                                  Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token,
                                  context)
                              .then((v) {
                            if (v == true) {
                              Provider.of<SharedPref>(context, listen: false)
                                  .getSharedHelper(_preferences);
                            }
                          });
                        },
                        btnWidth: MediaQuery.of(context).size.width - 40,
                        btnHeight: 45,
                        btnColor: Theme.of(context).primaryColor,
                        buttonText: "تعديل بيانات المستخدم",
                      ),
                      SizedBox(height: 10),
                      RegisterTextField(
                        hint: 'رقم الجوال',
                        icon: Icons.phone,
                        onChange: (v) {
                          Provider.of<ChangeMobileProvider>(context,
                                  listen: false)
                              .phone = v;
                        },
                        init: Provider.of<SharedPref>(context, listen: false)
                            .phone,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SignInButton(
                        txtColor: Colors.white,
                        onPressSignIn: () {
                          Provider.of<ChangeMobileProvider>(context,
                                  listen: false)
                              .changeMobile(
                            Provider.of<SharedPref>(context, listen: false)
                                .token,
                            context,
                          );
                        },
                        btnWidth: MediaQuery.of(context).size.width - 40,
                        btnHeight: 45,
                        btnColor: Theme.of(context).primaryColor,
                        buttonText: 'تعديل رقم الجوال',
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  EditPassword()
                ],
              ),
            ),
          ),
          CustomAppBarBackGround(
            widgets: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              Expanded(
                child: Container(
                  height: 60,
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                    child: Text(
                      "تعديل حسابي",
                      // "تعديل حسابي",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * .05),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white, width: 5)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      InkWell(
                        onTap: () => imagePicker.showDialog(context),
                        child: _image == null
                            ? Provider.of<SharedPref>(context, listen: false)
                                        .photo !=
                                    null
                                ? CachedNetworkImage(
                                    imageUrl: Provider.of<SharedPref>(context,
                                            listen: false)
                                        .photo,
                                    fadeInDuration: Duration(seconds: 2),
                                    placeholder: (context, url) => CircleAvatar(
                                        radius: 60,
                                        backgroundImage: AssetImage(
                                            'assets/images/avatar.jpeg')),
                                    imageBuilder: (context, provider) {
                                      return CircleAvatar(
                                          radius: 60,
                                          backgroundImage: provider);
                                    },
                                  )
                                : CircleAvatar(
                                    radius: 60,
                                    backgroundImage:
                                        AssetImage('assets/images/avatar.jpeg'))
                            : Container(
                                child: CircleAvatar(
                                  radius: 60,
                                  backgroundImage: Image.file(_image).image,
                                ),
                              ),
                      ),
                      Positioned(
                        bottom: 5,
                        right: 10,
                        child: InkWell(
                          onTap: () => imagePicker.showDialog(context),
                          child: CircleAvatar(
                            child: Icon(
                              Icons.add,
                              color: Colors.black,
                              size: 15,
                            ),
                            radius: 10,
                            backgroundColor: Colors.white,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }

  @override
  userImage(File _image) {
    setState(() {
      this._image = _image;
      Provider.of<EditUserDataProvider>(context, listen: false).image = _image;
    });
  }
}
