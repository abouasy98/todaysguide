import 'dart:io';

import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Models/get/getUserDataModel.dart';
import 'package:todaysguide/src/provider/changeData/editAcountProvider.dart';
import 'package:todaysguide/src/provider/deleteShopPhoto.Provider.dart';
import 'package:todaysguide/src/provider/get/getUserDataProvider.dart';
import 'package:todaysguide/src/screens/Intro/splash_screen.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/multI_image_btn_sheet.dart';

class EditSlider extends StatefulWidget {
  @override
  _EditSliderState createState() => _EditSliderState();
}

class _EditSliderState extends State<EditSlider> with TickerProviderStateMixin {
  List<File> _img = [];
  List<Asset> files = [];
  List<Asset> filesAsset = [];
  UserData getUserDataProvider;
  EditUserDataProvider editUserDataProvider;
  SharedPreferences _preferences;
  bool isInit = true;
  @override
  void didChangeDependencies() async {
    if (isInit) {
      _preferences = await SharedPreferences.getInstance();

      await Provider.of<GetUserDataProvider>(context, listen: false)
          .getUserData(
              Provider.of<SharedPref>(context, listen: false).token, context);
      editUserDataProvider =
          Provider.of<EditUserDataProvider>(context, listen: false);
      getUserDataProvider =
          Provider.of<GetUserDataProvider>(context, listen: false)
              .userDataModel
              .data;
      setState(() {
        isInit = false;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('تعديل السلايدر'),
      ),
      body: Column(
        children: [
          isInit
              ? AppLoader()
              : (_img.length > 0 ||
                          (getUserDataProvider.photos != null &&
                              getUserDataProvider.photos.length > 0)) ==
                      true
                  ? Directionality(
                      textDirection: TextDirection.rtl,
                      child: Container(
                        height: 120,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: getUserDataProvider.photos.length,
                          itemBuilder: (context, index) {
                            return _oneImgNetwork(
                                getUserDataProvider.photos[index].photo,
                                index,
                                getUserDataProvider.photos[index].id);
                          },
                        ),
                      ),
                    )
                  : Container(),
          _img.length != 0 || filesAsset.length != 0
              ? Container(
                  height: 150,
                  child: ListView(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    children: [
                      (filesAsset.length < 1) == true
                          ? Container()
                          : Directionality(
                              textDirection: TextDirection.rtl,
                              child: Container(
                                height: 120,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: filesAsset.length,
                                  itemBuilder: (context, index) {
                                    return _oneImgFileAsset(
                                        filesAsset[index], index);
                                  },
                                ),
                              ),
                            ),
                      _img.length < 1 == true
                          ? Container()
                          : Directionality(
                              textDirection: TextDirection.rtl,
                              child: Container(
                                height: 120,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: _img.length,
                                  itemBuilder: (context, index) {
                                    return _oneImgFile(_img[index], index);
                                  },
                                ),
                              ),
                            )
                    ],
                  ),
                )
              : Container(),
          SizedBox(height: 20),
          InkWell(
            onTap: () => ImagePickerDialogBtSheet().show(
                context: context,
                onGet: (f, v) {
                  if (v == 0)
                    setState(() {
                      filesAsset.addAll(f);
                    });
                  else {
                    setState(() {
                      _img.add(f);
                    });
                  }
                  return;
                }),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Color.fromRGBO(220, 220, 220, 1))),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Icon(Icons.photo),
                    Text(
                      "المعرض",
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
          SignInButton(
            txtColor: Colors.white,
            onPressSignIn: () {
              editUserDataProvider.photos = filesAsset;
              editUserDataProvider.imgs = _img;
              Provider.of<EditUserDataProvider>(context, listen: false)
                  .changeUserData(
                      Provider.of<SharedPref>(context, listen: false).token,
                      context)
                  .then((v) {
                if (v == true) {
                  Provider.of<SharedPref>(context, listen: false)
                      .getSharedHelper(_preferences);
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Splash(),
                    ),
                    (Route<dynamic> route) => false,
                  );
                }
              });
            },
            btnWidth: MediaQuery.of(context).size.width - 40,
            btnHeight: 45,
            btnColor: Theme.of(context).primaryColor,
            buttonText: "تعديل السلايدر",
          ),
        ],
      ),
    );
  }

  Widget _oneImgFile(File img, int index) {
    return Padding(
      padding: const EdgeInsets.only(right: 10, left: 10),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10, right: 10),
            child: Align(
              alignment: Alignment.centerRight,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.file(
                    img,
                    // width: 300,
                    // height: 300,
                  )),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: InkWell(
              onTap: () {
                setState(() {
                  _img.removeAt(index);
                });
              },
              child: Material(
                color: Colors.white,
                elevation: 2,
                shape: CircleBorder(),
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Icon(Icons.close, size: 18),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _oneImgFileAsset(Asset img, int index) {
    return Padding(
      padding: const EdgeInsets.only(right: 10, left: 10),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10, right: 10),
            child: Align(
              alignment: Alignment.centerRight,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: AssetThumb(
                    asset: img,
                    width: 300,
                    height: 300,
                  )),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: InkWell(
              onTap: () {
                setState(() {
                  filesAsset.removeAt(index);
                });
              },
              child: Material(
                color: Colors.white,
                elevation: 2,
                shape: CircleBorder(),
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Icon(Icons.close, size: 18),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _oneImgNetwork(String img, int index, int id) {
    return Padding(
      padding: const EdgeInsets.only(right: 10, left: 10),
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10, right: 10),
            child: Align(
              alignment: Alignment.centerRight,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    img,
                  )),
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: InkWell(
              onTap: () {
                setState(() {
                  if (getUserDataProvider.photos.length == 1) {
                    CustomAlert()
                        .toast(context: context, title: "لا يمكن الحذف");
                  } else {
                  
                    getUserDataProvider.photos.removeAt(index);
                    Provider.of<DeleteShopPhotoProvider>(context, listen: false)
                        .deletNot(
                            Provider.of<SharedPref>(context, listen: false)
                                .token,
                            id,
                            context);
                  }
                });
              },
              child: Material(
                color: Colors.white,
                elevation: 2,
                shape: CircleBorder(),
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Icon(Icons.close, size: 18),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
