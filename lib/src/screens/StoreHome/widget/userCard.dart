import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/shopOrdersModle.dart';

class UserCard extends StatefulWidget {
  final ShopOrders order;
  final int type;
  final Function ontap;

  const UserCard({Key key, this.order, this.ontap, this.type})
      : super(key: key);

  @override
  _UserCardState createState() => _UserCardState();
}

class _UserCardState extends State<UserCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10, top: 10),
      child: InkWell(
        onTap: widget.ontap,
        child: Card(
          color: Colors.white,
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Padding(
            padding: EdgeInsets.all(10),
            child: ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: 60,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CachedNetworkImage(
                              imageUrl: widget.type == 1
                                  ? widget.order.userPhoto ?? ""
                                  : widget.order.driverPhoto ?? "",
                              errorWidget: (context, url, error) => ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: Image.asset('assets/images/avatar.jpeg',
                                      fit: BoxFit.cover)),
                              fadeInDuration: Duration(seconds: 2),
                              placeholder: (context, url) => ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: Image.asset('assets/images/avatar.jpeg',
                                      fit: BoxFit.cover)),
                              imageBuilder: (context, provider) {
                                return ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: Image(
                                      image: provider,
                                      fit: BoxFit.cover,
                                    ));
                              },
                            ),
                          ),
                        ),
                        SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.type == 1
                                  ? widget.order.user
                                  : widget.order.driver,
                              style: TextStyle(fontSize: 16),
                            ),
                            Text(widget.type == 1
                                ? widget.order.userPhone
                                : widget.order.driverPhone)
                          ],
                        ),
                      ],
                    ),
                    Text(
                      widget.type == 1 ? "العميل" : "السائق",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
