import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/Shop/myProductModle.dart';

class Productcard extends StatefulWidget {
  final ShopProduct shopProduct;
  final Function onTap;

  const Productcard({Key key, this.shopProduct, this.onTap}) : super(key: key);
  @override
  _ProductcardState createState() => _ProductcardState();
}

class _ProductcardState extends State<Productcard> {
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return InkWell(
      onTap: widget.onTap,
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25),
                topLeft: Radius.circular(25),
                bottomRight: Radius.circular(45),
                topRight: Radius.circular(45))),
        child: Container(
          height: mediaQuery.height * 0.1,
          width: mediaQuery.width * 0.9,
          child: Row(children: [
            Stack(children: [
              CircleAvatar(
                radius: 40,
                backgroundColor: Colors.blue,
                child: widget.shopProduct.photos != null
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(37.0),
                        child: FadeInImage.assetNetwork(
                          image: widget.shopProduct.photos[0].photo,
                          imageCacheHeight: 500,
                          imageCacheWidth: 500,
                          placeholder: "assets/images/newLogo.png",
                        ),
                      )
                    : CircleAvatar(
                        radius: 37,
                        backgroundImage:
                            AssetImage('assets/images/newLogo.png'),
                      ),
              ),
            ]),
            Container(
              height: mediaQuery.height * 0.1,
              alignment: Alignment.centerRight,
              width: mediaQuery.width * 0.67,
              child: ListTile(
                  leading: Container(
                    width: mediaQuery.width * 0.37,
                    alignment: Alignment.centerRight,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.shopProduct.name,
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        Expanded(child: Text('')),
                        // if (widget.type != 2)
                        Text(
                          'عدد المنتجات : ${widget.shopProduct.available} منتج',
                          style: TextStyle(fontSize: 14),
                        ),
                      ],
                    ),
                  ),
                  trailing: Container(
                    width: mediaQuery.width * 0.24,
                    alignment: Alignment.centerLeft,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                widget.shopProduct.price,
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context).primaryColor),
                              ),
                              Text(
                                'SR',
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                            ]),
                        Expanded(child: Text('')),
                      ],
                    ),
                  )
                  // : Container(
                  //     width: mediaQuery.width * 0.24,
                  //     alignment: Alignment.centerLeft,
                  //     child: Column(
                  //       mainAxisAlignment: MainAxisAlignment.start,
                  //       crossAxisAlignment: CrossAxisAlignment.start,
                  //       children: [
                  //         Row(
                  //             mainAxisAlignment:
                  //                 MainAxisAlignment.spaceEvenly,
                  //             children: [
                  //               Icon(
                  //                 Icons.location_on,
                  //                 size: 15,
                  //               ),
                  //               Text(
                  //                 '${widget.distance} ك',
                  //                 style: TextStyle(fontSize: 15),
                  //               ),
                  //             ]),
                  //         Expanded(child: Text('')),
                  //         Container(
                  //           decoration: BoxDecoration(
                  //               color: Colors.grey[300],
                  //               borderRadius: BorderRadius.circular(20)),
                  //           child: Row(
                  //               mainAxisAlignment:
                  //                   MainAxisAlignment.spaceEvenly,
                  //               children: [
                  //                 Icon(
                  //                   Icons.remove_red_eye,
                  //                   size: 15,
                  //                 ),
                  //                 Text('${widget.viewCount} مشاهده',
                  //                     style: TextStyle(fontSize: 14)),
                  //               ]),
                  //         )
                  //       ],
                  //     ),
                  //   ),
                  ),
            ),
          ]),
        ),
      ),
    );
  }
}
