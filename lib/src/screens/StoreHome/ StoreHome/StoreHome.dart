import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/screens/DriverHome/HomePage/driverCommitions.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_app_bar_back_ground.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_btn.dart';
import 'package:todaysguide/src/screens/MainWidgets/driver_data.dart';
import 'package:todaysguide/src/screens/StoreHome/%20StoreHome/shopOrders.dart';
import 'package:todaysguide/src/screens/StoreHome/ShopProduct/myProducts.dart';

class StoreHome extends StatefulWidget {
  @override
  _StoreHomeState createState() => _StoreHomeState();
}

class _StoreHomeState extends State<StoreHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(
            MediaQuery.of(context).size.height / 4,
          ),
          child: Stack(
            children: <Widget>[
              CustomAppBarBackGround(
                height: MediaQuery.of(context).size.height / 4,
                widgets: <Widget>[
                  Expanded(
                    child: Text(""),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: DriverData(),
              ),
            ],
          )),
      body: ListView(
        children: [
          // AvalbleBtn(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(Provider.of<SharedPref>(context, listen: false).city,
                  style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold)),
                  Text(Provider.of<SharedPref>(context, listen: false).address,
                  style: TextStyle(color: Colors.black)),
            ],
          ),
          SizedBox(height: 100),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 20),
            child: CustomBtn(
              txtColor: Colors.black,
              text: "منتجاتي",
              onTap: () => Navigator.push(
                  context, MaterialPageRoute(builder: (c) => MyProducts())),
              color: Colors.white,
              elevtion: 10,
            ),
          ),
          CustomBtn(
            elevtion: 10,
              txtColor: Colors.white,
              text: "عمولاتي",
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (c) => DriverCommitions(
                            url: "shop-commission",
                          ))),
              color: Theme.of(context).primaryColor),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 20),
            child: CustomBtn(
              txtColor: Colors.black,
              text: "طلباتي",
              onTap: () => Navigator.push(
                  context, MaterialPageRoute(builder: (c) => ShopOrders())),
              color: Colors.white,
              elevtion: 10,
            ),
          ),
        ],
      ),
    );
  }
}
