import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/shopOrdersModle.dart';
import 'package:todaysguide/src/screens/DriverHome/DriverOrder/widget/orderCard.dart';
import 'package:todaysguide/src/screens/DriverHome/HomePage/widget/total_card.dart';
import 'package:todaysguide/src/screens/MainWidgets/details_card.dart';
import 'package:todaysguide/src/screens/MainWidgets/oneOrderMapCard.dart';
import 'package:todaysguide/src/screens/StoreHome/%20StoreHome/ShopProductDetails.dart';
import 'package:todaysguide/src/screens/StoreHome/%20StoreHome/shopClientCard.dart';

import 'package:url_launcher/url_launcher.dart';

class OrderDetails extends StatelessWidget {
  final ShopOrders order;
  final int orderType;
  const OrderDetails({Key key, this.order, this.orderType}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(order.shop),
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(height: 20),
          DetailsCard(
            label: 'تفاصيل الطلب',
            content: order.orderDetails ?? "",
          ),
          Visibility(
            visible: order.shop != null,
            child: DetailsCard(
              label: 'تفاصيل العنوان',
              content: order.addressDetails ?? "",
            ),
          ),
          // order.shopPhoto != null
          //     ? OrderImageCard(
          //         link: order.shopPhoto,
          //         lable: "صورة المكان",
          //       )
          //     : Container(),
          orderType == 1
              ? DriverShopCard(
                  order: order,
                  ontap: () {
                    launch("tel://${order.userPhone}");
                  },
                  type: 1,
                )
              : SizedBox(),
          orderType == 1
              ? DriverShopCard(
                  order: order,
                  ontap: () {
                    launch("tel://${order.driverPhone}");
                  },
                  // type: 1,
                )
              : SizedBox(),
          ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: order.productsCart.length,
              itemBuilder: (c, index) {
                return OrderCard(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (c) => ShopProductDetails(
                                  products: order.productsCart[index],
                                )));
                  },
                  createdAt: order.productsCart[index].createdAt
                      .toString()
                      .substring(0, 10),
                  image: order.productsCart[index].photos[0].photo ?? "",
                  orderNum: order.productsCart[index].orderId.toString(),
                  placeName: order.productsCart[index].productName,
                  price: order.productsCart[index].price ?? "0",
                );
              }),
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "موقع الطلب",
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
          Visibility(
            visible: order.orderLatitude != null && order.orderLatitude != null,
            child: OneOrderMapCard(
              orderLat: double.parse(order.orderLatitude ?? "0.0"),
              orderLong: double.parse(order.orderLongitude ?? "1.0"),
              lat: double.parse(order.latitude ?? "1.0"),
              long: double.parse(order.longitude ?? "1.0"),
            ),
          ),
          TotalCard(
            total: order.price ?? "0",
            title: 'إجمالي التوصيل',
          ),
          TotalCard(
            total: order.totalPrice ?? "0",
            title: 'إجمالي الطلب',
          ),

          SizedBox(height: 20),
        ],
      ),
    );
  }
}
