import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/changeData/editAcountProvider.dart';
import 'package:todaysguide/src/screens/HomePages/more/internal/myPlaces/pickPlace.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';

class EditLocationStore extends StatefulWidget {
  @override
  _EditLocationStoreState createState() => _EditLocationStoreState();
}

class _EditLocationStoreState extends State<EditLocationStore> {
  SharedPreferences _preferences;
  bool isInit = true;

  
  @override
  void didChangeDependencies() async {
    if (isInit) {
      Provider.of<MapHelper>(context, listen: false).getLocation();
      _preferences = await SharedPreferences.getInstance();
      isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('تعديل موقع المتجر'),
          centerTitle: true,
        ),
        body: Consumer<MapHelper>(builder: (context, snap, _) {
          if (snap.position == null) {
            return Center(
              child: InkWell(
                onTap: () async {
                  await Provider.of<MapHelper>(context, listen: false)
                      .getLocation();
                },
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(
                      ' انت لم تقم بتفعيل خاصية اللوكيشن ،لذا يرجي الضغط لتفعيلها لتتمكن من تعديل موقعك',
                      style: TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            );
          } else {
            return Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 20),
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => PickPlace()));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.grey)),
                      padding: EdgeInsets.all(10),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 8.0, left: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Icon(Icons.arrow_drop_down),
                            Text(Provider.of<EditUserDataProvider>(context, listen: false)
                                        .address !=
                                    null
                                ? Provider.of<EditUserDataProvider>(context, listen: false)
                                            .address
                                            .length >
                                        50
                                    ? Provider.of<EditUserDataProvider>(context,
                                            listen: false)
                                        .address
                                        .substring(0, 40)
                                    : Provider.of<EditUserDataProvider>(context,
                                            listen: false)
                                        .address
                                : Provider.of<SharedPref>(context, listen: false)
                                            .address
                                            .length >
                                        50
                                    ? Provider.of<SharedPref>(context, listen: false)
                                        .address
                                        .substring(0, 40)
                                    : Provider.of<SharedPref>(context, listen: false).address)
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                SignInButton(
                  txtColor: Colors.white,
                  onPressSignIn: () {
                    Provider.of<EditUserDataProvider>(context, listen: false)
                        .changeUserData(
                            Provider.of<SharedPref>(context, listen: false)
                                .token,
                            context)
                        .then((v) {
                      if (v == true) {
                        Provider.of<SharedPref>(context, listen: false)
                            .getSharedHelper(_preferences);
                      }
                    });
                  },
                  btnWidth: MediaQuery.of(context).size.width - 40,
                  btnHeight: 45,
                  btnColor: Theme.of(context).primaryColor,
                  buttonText: "تعديل موقع المتجر",
                ),
              ],
            );
          }
        }));
  }
}
