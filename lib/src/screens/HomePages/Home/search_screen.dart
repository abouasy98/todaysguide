import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Models/post/search_model.dart';
import 'package:todaysguide/src/provider/post/search_provider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/shopDetailsById.dart';
import 'package:todaysguide/src/screens/HomePages/Home/homeItem.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';

class SearchScreen extends StatefulWidget {
  final int filterType;
  SearchScreen({
    this.filterType,
  });
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  String item;
  SearchModel model;
  String errorMessage;
  SearchProvider searchProvider;
  bool isInit = true;
  bool isLoading = false;
  var controller = TextEditingController();
  List<Datum> results;
  Future<void> searchByKeyword(String keyword) async {
    if (keyword.isNotEmpty) {
      setState(() {
        isLoading = true;
      });
      model = await Provider.of<SearchProvider>(context, listen: false)
          .search(keyword, context);
      searchProvider = Provider.of<SearchProvider>(context, listen: false);
      results =
          searchProvider.shops.where((element) => element.open == 1).toList();
      if (model.data == null) {
        errorMessage = model.error[0].value;
      }

      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    controller.addListener(() {
      if (controller.text.trim().isEmpty) {
        if (errorMessage != null) {
          setState(() {
            errorMessage = null;
          });
        }
        if (isLoading) {
          setState(() {
            isLoading = false;
          });
          if (model != null) {
            setState(() {
              model = null;
            });
          }
        }
        if (model != null) {
          setState(() {
            model = null;
          });
        }
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          appBar: AppBar(
            // automaticallyImplyLeading: false,
            leading: Container(),
            title: Container(
              height: mediaQuery.height * 0.06,
              width: double.infinity,
              child: TextField(
                controller: controller,
                onChanged: (value) {
                  if (value.trim().isEmpty) {
                    print('aaag');
                  }
           
                  searchByKeyword(value.trim());
                },
                textAlign: TextAlign.start,
                // textDirection: TextDirection.rtl,
                textAlignVertical: TextAlignVertical.bottom,
                decoration: InputDecoration(
                  hintText: 'البحث',
                  hintStyle: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 20,
                  ),
                  suffixIcon: Icon(
                    FontAwesomeIcons.search,
                  ),
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide.none),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: BorderSide.none),
                ),
              ),
            ),
            actions: [
              IconButton(
                icon: Icon(Icons.arrow_forward_ios),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
          body: isLoading
              ? SingleChildScrollView(child: AppLoader())
              : errorMessage != null
                  ? Center(
                      child: Text(
                        errorMessage,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    )
                  : (model != null && results != null && results.length != 0)
                      ? Center(
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.9,
                            child: ListView.builder(
                              padding: EdgeInsets.only(
                                top: 2,
                                bottom: 3,
                              ),
                              itemCount: results.length,
                              itemBuilder: (ctx, i) => InkWell(
                                splashColor: Color(0xffFCE8E6),
                                onTap: () {
                                  print('ss');

                                  Navigator.of(context).push(
                                    PageRouteBuilder(
                                      pageBuilder: (_, __, ___) => ShopDetailsById(
                                        id: results[i].id,
                                      ),
                                    ),
                                  );
                                },
                                child: HomeItem(
                                  name: results[i].name,
                                  photo: results[i].photo,
                                  productNumber: results[i].productsNumber,
                                  search: true,
                                  distance: results[i].distance,
                                  id: results[i].id,
                                  viewCount: results[i].viewCount,
                                ),
                              ),
                            ),
                          ),
                        )
                      : Center(
                          child: Text(
                            'يرجى إدخال الاسم المراد البحث عنه',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        )),
    );
  }
}
