import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ActiveOrderDetailsIteams extends StatelessWidget {
  final String name;
  final int id;
  final int quantity;
  final String shopName;
  final String photo;
  final String price;
  ActiveOrderDetailsIteams(
      {this.id,
      this.name,
      this.price,
      this.quantity,
      this.shopName,
      this.photo});
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      elevation: 10,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                name,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Card(
                elevation: 0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    alignment: Alignment.centerRight,
                    height: mediaQuery.height * 0.15,
                    width: mediaQuery.width * 0.4,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.end,
                        //   children: [
                        //     Text(
                        //       '$id',
                        //       style: TextStyle(fontSize: 13),
                        //     ),
                        //     Text(
                        //       ' : رقم',
                        //       style: TextStyle(
                        //           fontWeight: FontWeight.bold, fontSize: 13),
                        //     ),
                        //   ],
                        // ),
                        // SizedBox(
                        //   height: 5,
                        // ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              '$quantity',
                              style: TextStyle(fontSize: 13),
                            ),
                            Text(
                              ' : عدد',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 13),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              width: mediaQuery.width * 0.27,
                              child: Text(
                                shopName,
                                textAlign: TextAlign.right,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontSize: 13),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              ' : المتجر',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 13),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              'SR',
                              style: TextStyle(fontSize: 13),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              price,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 13),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              // Divider(height: 6,),
              Container(
                width: 1,
                height: 100,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: mediaQuery.height * 0.15,
                  width: mediaQuery.width * 0.3,
                  child: CachedNetworkImage(
                      imageUrl: photo,
                      fadeInDuration: Duration(seconds: 2),
                      placeholder: (context, url) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/images/icon-001.png'),
                                  fit: BoxFit.fill),
                            ),
                          ),
                      imageBuilder: (context, provider) {
                        return Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                image: provider, fit: BoxFit.fill),
                          ),
                        );
                      }),
                ),
              ),
            ],
          ),
          // Center(child: Container(

          //                               width: mediaQuery.width * 0.8,child: Divider(color: Colors.black54,))),
        ],
      ),
    );
  }
}
