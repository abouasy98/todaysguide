import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/post/acceptOfferProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';

import '../../../Models/post/shopsModel.dart';
import 'ShopDetails/shopDetails.dart';
import 'ShopDetails/shopDetailsById.dart';

class HomeItem extends StatefulWidget {
  final String photo;
  final int id;
  final String name;
  final int productNumber;
  final int distance;
  final String price;
  final int type;
  final int viewCount;
  final String carType;
  final int driverId;
  final Shops shops;
  final bool search;
  HomeItem(
      {this.name,
      this.photo,
      this.id,
      this.type,
      this.productNumber,
      @required this.search,
      this.driverId,
      this.price,
      this.shops,
      this.carType,
      this.distance,
      this.viewCount});

  @override
  _HomeItemState createState() => _HomeItemState();
}

class _HomeItemState extends State<HomeItem> {
  CustomDialog _dialog = CustomDialog();

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return InkWell(
      onTap: () async {
        if (widget.type == 2) {
          await _dialog.showOptionDialog(
              context: context,
              msg: 'هل تود قبول عرض هذا السائق',
              okFun: () {
                Provider.of<AcceptOfferProvider>(
                  context,
                  listen: false,
                ).acceptOffer(
                    widget.id,
                    Provider.of<SharedPref>(context, listen: false).token,
                    context);
              },
              okMsg: 'نعم',
              cancelMsg: 'لا',
              cancelFun: () {
                return;
              });
        } else {
          if (widget.search) {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ShopDetailsById(id: widget.id)));
          } else {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ShopDetails(shops: widget.shops)));
          }
        }
      },
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25),
                topLeft: Radius.circular(25),
                bottomRight: Radius.circular(45),
                topRight: Radius.circular(45))),
        child: Container(
          // height: 50,
          // width: mediaQuery.width * 0.9,
          child: Row(children: [
            Stack(children: [
              CircleAvatar(
                radius: 40,
                backgroundColor: Theme.of(context).primaryColor,
                child: widget.photo != null
                    ? CachedNetworkImage(
                        imageUrl: widget.photo,
                        fadeInDuration: Duration(seconds: 2),
                        placeholder: (context, url) => CircleAvatar(
                            radius: 37,
                            backgroundImage:
                                AssetImage('assets/images/newLogo.png')),
                        imageBuilder: (context, provider) {
                          return CircleAvatar(
                            radius: 37,
                            backgroundImage: provider,
                          );
                        })
                    : CircleAvatar(
                        radius: 37,
                        backgroundImage:
                            AssetImage('assets/images/newLogo.png')),
              ),
              if (widget.type == 2)
                Positioned(
                  bottom: 15,
                  child: CircleAvatar(
                    radius: 6,
                    backgroundColor: Colors.white,
                    child: CircleAvatar(
                      radius: 5,
                      backgroundColor: Colors.green,
                    ),
                  ),
                )
            ]),
            Container(
              height: mediaQuery.height * 0.1,
              alignment: Alignment.centerRight,
              width: mediaQuery.width * 0.65,
              // color: Colors.black,
              child: ListTile(
                leading: Container(
                  width: mediaQuery.width * 0.37,
                  alignment: Alignment.centerRight,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.name,
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 5,
                      ),

                      // Expanded(child: Text('')),
                      if (widget.type != 2)
                        Row(
                          children: [
                            Text(
                              'عدد المنتجات : ',
                              style: TextStyle(fontSize: 10),
                            ),
                            Text(
                              "${widget.productNumber} منتج",
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      if (widget.type == 2)
                        Text(
                          widget.carType == '0' ? 'سياره عادية' : 'سيارة مبردة',
                          style: TextStyle(fontSize: 14, color: Colors.grey),
                        ),
                    ],
                  ),
                ),
                trailing: widget.type == 2
                    ? Container(
                        width: mediaQuery.width * 0.24,
                        alignment: Alignment.centerLeft,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                widget.price,
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context).primaryColor),
                              ),
                              Text(
                                'SR',
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                            ]),
                      )
                    : Container(
                        width: mediaQuery.width * 0.24,
                        alignment: Alignment.centerLeft,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.location_on,
                                    size: 12,
                                    color: Colors.black,
                                  ),
                                  Text(
                                    '${widget.distance} ك',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ]),
                            SizedBox(
                              height: 5,
                            ),
                            // Expanded(child: Text('')),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[300],
                                  borderRadius: BorderRadius.circular(20)),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Icon(
                                      Icons.remove_red_eye,
                                      size: 15,
                                    ),
                                    Text('${widget.viewCount} مشاهده',
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold)),
                                  ]),
                            )
                          ],
                        ),
                      ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
