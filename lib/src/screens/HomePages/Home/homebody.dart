import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/provider/checkProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/homeItem.dart';
import '../../MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/MainWidgets/customScrollPhysics.dart';
class HomeBody extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Consumer<CheckProvider>(builder: (context, snap, _) {
      if ((snap.shopsFilter == null)||(snap.shopsFilter!=null&&snap.shopsFilter.length<0)) {
        return AppLoader();
      } else if (snap.shopsFilter.length < 1) {
        return Center(child: Text('لا يوجد نتائج'));
      } else {
        return ListView.builder(
          primary: false,
          shrinkWrap: true,
          physics: CustomScrollPhysics(),
          //  reverse: true,
          itemCount: snap.shopsFilter.length,
          itemBuilder: (context, i) {
            return HomeItem(
              name: snap.shopsFilter[i].name,
              distance: snap.shopsFilter[i].distance,
              photo: snap.shopsFilter[i].photo,
                   search: false,
              productNumber: snap.shopsFilter[i].productsNumber,
              shops: snap.shopsFilter[i],
              id: snap.shopsFilter[i].id,
              viewCount: snap.shopsFilter[i].viewCount,
            );
          },
        );
      }
    });
  }
}
