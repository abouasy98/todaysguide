import 'package:flutter/material.dart';
import 'package:todaysguide/src/provider/post/productsFilterProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/shopItem.dart';

class SearchShopsIteams extends StatelessWidget {
  final String query;
  final List<Products> suggteionList;

  SearchShopsIteams({
    this.query,
    this.suggteionList,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: suggteionList.length,
        itemBuilder: ((context, i) {
          Products searchedUser = Products(
            available: suggteionList[i].available,
            category: suggteionList[i].category,
            categoryId: suggteionList[i].categoryId,
            details: suggteionList[i].details,
            price: suggteionList[i].price,
            shop: suggteionList[i].shop,
            shopId: suggteionList[i].shopId,
            photos: suggteionList[i].photos,
            id: suggteionList[i].id,
            name: suggteionList[i].name,
          );

          return ShopItem(
            name: searchedUser.name,
            photo: searchedUser.photos[0].photo,
            id: searchedUser.id,
           productsFilter: suggteionList[i],
            price: searchedUser.price,
            available: searchedUser.available,
            orderWaiting: false,
          );
        }));
  }
}
