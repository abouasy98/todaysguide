// import 'dart:io';
// import 'dart:typed_data';

// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:carousel_pro/carousel_pro.dart';
// import 'package:esys_flutter_share/esys_flutter_share.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:provider/provider.dart';
// import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
// import 'package:todaysguide/src/provider/get/getshopByIdProvider.dart';
// import 'package:todaysguide/src/provider/post/reportShopProvider.dart';
// import 'package:todaysguide/src/screens/MainWidgets/newsPhotoGallary.dart';
// import 'package:todaysguide/src/screens/MainWidgets/showGeneralDialog.dart';

// class SliderWidget extends StatefulWidget {
//   final ShopsByIdProvider shopById;

//   const SliderWidget({Key key, this.shopById}) : super(key: key);
//   @override
//   _SliderWidgetState createState() => _SliderWidgetState();
// }

// class _SliderWidgetState extends State<SliderWidget> {
//   @override
//   Widget build(BuildContext context) {
//     final mediaQuery = MediaQuery.of(context).size;
//      return Stack(
//       overflow: Overflow.visible,
//       children: [
//         Container(
//           height: mediaQuery.height * 0.3,
//           width: mediaQuery.width,
//           child: Carousel(
//             boxFit: BoxFit.fill,
//             images: List.generate(widget.shopById.restourant.data.photos.length,
//                 (int index) {
//               return InkWell(
//                 onTap: () => Navigator.push(
//                     context,
//                     MaterialPageRoute(
//                         builder: (context) => NewsPhotoGallary(
//                               images: photos,
//                             ))),
//                 child: widget.shopById.restourant.data.photos[index].photo != null
//                     ? CachedNetworkImage(
//                         imageUrl: widget.shopById.restourant.data.photos[index].photo,
//                         fadeInDuration: Duration(seconds: 2),
//                         placeholder: (context, url) => Container(
//                               decoration: BoxDecoration(
//                                 image: DecorationImage(
//                                     image: AssetImage(
//                                         'assets/images/icon-001.png'),
//                                     fit: BoxFit.fill),
//                               ),
//                             ),
//                         imageBuilder: (context, provider) {
//                           return Container(
//                             decoration: BoxDecoration(
//                               image: DecorationImage(
//                                   image: provider, fit: BoxFit.fill),
//                             ),
//                           );
//                         })
//                     : Container(
//                         decoration: BoxDecoration(
//                           image: DecorationImage(
//                               image: AssetImage('assets/images/icon-001.png'),
//                               fit: BoxFit.fill),
//                         ),
//                       ),
//               );
//             }),
//             autoplay: false,
//             dotIncreasedColor: Theme.of(context).primaryColor,
//             dotIncreaseSize: 2,
//             dotSize: 5,
//             dotBgColor: Color(0x00000000),
//             dotSpacing: widget.shopById.shopsById.length > 13
//                 ? 300 / widget.shopById.shopsById.length
//                 : 20,
//             autoplayDuration: Duration(seconds: 1),
//             animationCurve: Curves.decelerate,
//             animationDuration: Duration(milliseconds: 1000),
//             indicatorBgPadding: 10,
//             dotColor: Colors.white,
//           ),
//         ),
//         Positioned(
//           child: Container(
//             width: mediaQuery.width * 0.4,
//             child: Row(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: [
//                 CircleAvatar(
//                   backgroundColor: Colors.red[400],
//                   child: IconButton(
//                     iconSize: 20,
//                     onPressed: () async {
//                       GeneraDialog().show(
//                         context,
//                         Material(
//                           child: Container(
//                             height: mediaQuery.height * 0.5,
//                             width: mediaQuery.width * 0.9,
//                             decoration: BoxDecoration(
//                               color: Theme.of(context).primaryColor,
//                             ),
//                             child: Column(
//                               mainAxisSize: MainAxisSize.min,
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: [
//                                 Column(
//                                     mainAxisAlignment: MainAxisAlignment.start,
//                                     children: [
//                                       SizedBox(
//                                         height: mediaQuery.height * 0.05,
//                                       ),
//                                       Text(
//                                         'ارسل ملاحظاتك عن المتجر',
//                                         style: TextStyle(
//                                             color: Colors.white,
//                                             fontSize: 15,
//                                             fontWeight: FontWeight.bold),
//                                       ),
//                                       SizedBox(
//                                         height: mediaQuery.height * 0.05,
//                                       ),
//                                       Container(
//                                         height: mediaQuery.height * 0.29,
//                                         width: mediaQuery.width * 0.6,
//                                         child: Form(
//                                           key: _form,
//                                           child: TextFormField(
//                                             controller: _controller,
//                                             maxLines: 6,
//                                             minLines: 1,
//                                             style:
//                                                 TextStyle(color: Colors.white),
//                                             keyboardType:
//                                                 TextInputType.multiline,
//                                             textDirection: TextDirection.rtl,
//                                             autocorrect: false,
//                                             textAlign: TextAlign.right,
//                                             validator: (value) {
//                                               if (value.isEmpty) {
//                                                 return 'لا يجب ترك الحقل فارغ';
//                                               }

//                                               return null;
//                                             },
//                                             onChanged: (value) {},
//                                             decoration: InputDecoration(
//                                               labelStyle: TextStyle(
//                                                 color: Colors.black,
//                                               ),
//                                               border: new OutlineInputBorder(
//                                                   borderRadius:
//                                                       BorderRadius.all(
//                                                           Radius.circular(
//                                                               15.0)),
//                                                   borderSide: new BorderSide(
//                                                       color: Colors.black)),
//                                               enabledBorder: OutlineInputBorder(
//                                                 gapPadding: 2,
//                                                 borderSide: BorderSide(
//                                                     color: Colors.grey[300],
//                                                     width: 1.0),
//                                                 borderRadius: BorderRadius.all(
//                                                   Radius.circular(15.0),
//                                                 ),
//                                               ),
//                                               focusedBorder: OutlineInputBorder(
//                                                 gapPadding: 1,
//                                                 borderSide: BorderSide(
//                                                     color: Colors.grey[300],
//                                                     width: 2.0),
//                                                 borderRadius: BorderRadius.all(
//                                                   Radius.circular(15.0),
//                                                 ),
//                                               ),
//                                               focusColor: Colors.white,
//                                               contentPadding:
//                                                   EdgeInsets.all(10),
//                                             ),
//                                           ),
//                                         ),
//                                       ),
//                                     ]),
//                                 Padding(
//                                   padding: const EdgeInsets.only(
//                                     bottom: 12,
//                                   ),
//                                   child: Container(
//                                     height: mediaQuery.height * 0.06,
//                                     width: mediaQuery.width * 0.4,
//                                     alignment: Alignment.center,
//                                     child: _loadedSpinner
//                                         ? Center(
//                                             child: CircularProgressIndicator(),
//                                           )
//                                         : RaisedButton(
//                                             elevation: 8,
//                                             color: Colors.red,
//                                             onPressed: () async {
//                                               final _isValid =
//                                                   _form.currentState.validate();
//                                               if (!_isValid) {
//                                                 return;
//                                               }
//                                               _form.currentState.save();

//                                               setState(() {
//                                                 _loadedSpinner = true;
//                                               });

//                                               await Provider.of<
//                                                           ReportShopProvider>(
//                                                       context,
//                                                       listen: false)
//                                                   .reportShop(
//                                                       widget.id,
//                                                       _controller.text,
//                                                       Provider.of<SharedPref>(
//                                                               context,
//                                                               listen: false)
//                                                           .token,
//                                                       context);
//                                               _controller.clear();

//                                               setState(() {
//                                                 _loadedSpinner = false;
//                                               });
//                                               Fluttertoast.showToast(
//                                                   msg:
//                                                       'تم ارسال ملاحظاتك بنجاح');
//                                             },
//                                             child: Text(
//                                               'ارسال ملاحظاتك',
//                                               style: TextStyle(fontSize: 15),
//                                             ),
//                                             textColor: Colors.white,
//                                             shape: RoundedRectangleBorder(
//                                               borderRadius:
//                                                   BorderRadius.circular(15),
//                                             ),
//                                           ),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                       );
//                     },
//                     icon: Icon(
//                       Icons.priority_high,
//                       color: Colors.white,
//                     ),
//                   ),
//                 ),
//                 CircleAvatar(
//                   backgroundColor: Colors.white60,
//                   child: IconButton(
//                     iconSize: 20,
//                     onPressed: () async {
//                       await _saveForm();
//                     },
//                     icon: Icon(
//                       isFavourite ? Icons.favorite : Icons.favorite_border,
//                       color: isFavourite
//                           ? Colors.red
//                           : Color.fromRGBO(72, 83, 103, 1),
//                     ),
//                   ),
//                 ),
//                 CircleAvatar(
//                   backgroundColor: Colors.white60,
//                   child: IconButton(
//                     iconSize: 20,
//                     onPressed: () async {
//                       String _msg;
//                       StringBuffer _sb = new StringBuffer();
//                       setState(() {
//                         _sb.write(
//                             "عنوان الاعلان : ${widget.shopById.restourant.data.name} \n");

//                         _sb.write(shareLink);

//                         _msg = _sb.toString();
//                       });
//                       if (widget.shopById.restourant.data.photos != null) {
//                         if (widget.shopById.restourant.data.photos.length != 0) {
//                           var request = await HttpClient().getUrl(Uri.parse(
//                               widget.shopById.restourant.data.photos[0].photo));
//                           var response = await request.close();
//                           Uint8List bytes =
//                               await consolidateHttpClientResponseBytes(
//                                   response);
//                           await Share.file(
//                               'ESYS AMLOG', 'amlog.jpg', bytes, 'image/jpg',
//                               text: _msg);
//                         } else {
//                           Share.text("title", _msg, 'text/plain');
//                         }
//                       } else {
//                         Share.text("title", _msg, 'text/plain');
//                       }
//                     },
//                     icon: Icon(
//                       Icons.share,
//                       color: Color.fromRGBO(72, 83, 103, 1),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           left: 10,
//           top: 10,
//         )
//       ],
//     );
  
//   }
// }