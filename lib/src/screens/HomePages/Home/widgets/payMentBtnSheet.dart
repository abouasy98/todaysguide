import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/post/ClientFinishOrderProvider.dart';

class PaymentBtnSheet {
  show({BuildContext context, int id}) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        elevation: 2,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        builder: (_) {
          return Container(
            height: 350,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(10.0),
                  topRight: const Radius.circular(10.0),
                ),
                color: Colors.white),
            child: ListView(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: FlatButton(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Icon(FontAwesomeIcons.ccVisa),
                            Directionality(
                                textDirection: TextDirection.rtl,
                                child: Text("فيزا - ماستر")),
                          ],
                        ),
                        onPressed: () async {
                          await Provider.of<ClientFinishOrderProvider>(context,
                                  listen: false)
                              .clientFinishOder(
                                  Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token,
                                  id,
                                  2,
                                  context);
                        },
                      ),
                    ),
                    Divider(
                      indent: 10.0,
                      endIndent: 10.0,
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: FlatButton(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Icon(FontAwesomeIcons.creditCard),
                            Directionality(
                                textDirection: TextDirection.rtl,
                                child: Text("بطاقة مدى")),
                          ],
                        ),
                        onPressed: () async {
                          await Provider.of<ClientFinishOrderProvider>(context,
                                  listen: false)
                              .clientFinishOder(
                                  Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token,
                                  id,
                                  1,
                                  context);
                        },
                      ),
                    ),
                    Divider(
                      indent: 10.0,
                      endIndent: 10.0,
                    ),
                  ],
                ),
                Platform.isAndroid
                    ? Container()
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: FlatButton(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Icon(FontAwesomeIcons.applePay),
                                  Directionality(
                                      textDirection: TextDirection.rtl,
                                      child: Text("apple pay")),
                                ],
                              ),
                              onPressed: () async {
                                await Provider.of<ClientFinishOrderProvider>(
                                        context,
                                        listen: false)
                                    .clientFinishOder(
                                        Provider.of<SharedPref>(context,
                                                listen: false)
                                            .token,
                                        id,
                                        3,
                                        context);
                              },
                            ),
                          ),
                          Divider(
                            indent: 10.0,
                            endIndent: 10.0,
                          ),
                        ],
                      ),
              ],
            ),
          );
        });
  }
}
