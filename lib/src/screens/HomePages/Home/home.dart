import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/Models/get/splashesModel.dart';
import 'package:todaysguide/src/Models/post/shopsModel.dart';
import 'package:todaysguide/src/provider/checkProvider.dart';
import 'package:todaysguide/src/provider/get/splashesProvider.dart';
import 'package:todaysguide/src/provider/post/shopsProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/shopDetailsById.dart';
import 'package:todaysguide/src/screens/HomePages/Home/homebody.dart';
import 'package:todaysguide/src/screens/HomePages/Home/search_screen.dart';
import 'package:todaysguide/src/screens/HomePages/main_page.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../MainWidgets/app_loader.dart';
import '../drawer.dart';
import 'departmentFilter.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  FilterType filterType = FilterType.Nearest;
  String category = 'الكل';
  int categoryId = 0;
  SplashesModel splashModel;
  // DepartMentProvider categoriesProvider;
  ShopsProvider shopProvider;
  List<String> photos = [];
  bool isInit = true;
  GlobalKey<ScaffoldState> _keyDrawer = GlobalKey<ScaffoldState>();
  CheckProvider checkProvider;
  List<Shops> shops;

  _getShared() async {
    await Provider.of<ShopsProvider>(context, listen: false).getShops(
   
        0,
        null,
        context);

    shopProvider = Provider.of<ShopsProvider>(context, listen: false);

    checkProvider = Provider.of<CheckProvider>(context, listen: false);
    checkProvider.assignValueShops(
        shops: shopProvider.shops.where((e) => e.open == 1).toList());
  }

  _launchURL(String link) async {
    String url = link;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget _imageSlider() {
    final mediaQuery = MediaQuery.of(context).size;
    return FutureBuilder(
        future: Provider.of<SplashesProvider>(context, listen: false)
            .getSplashes(context),
        builder: (c, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return SpinKitThreeBounce(
                color: Theme.of(context).primaryColor,
                size: 20,
              );
            default:
              if (snapshot.hasError)
                return Text('Error: ${snapshot.error}');
              else
                return snapshot.data.data == null
                    ? Center(
                        child: Text("لا يوجد طلبات"),
                      )
                    : Container(
                        height: mediaQuery.height * 0.3,
                        child: Carousel(
                          boxFit: BoxFit.fill,
                          images: List.generate(snapshot.data.data.length,
                              (int index) {
                            return InkWell(
                              onTap: () {
                                if (snapshot.data.data[index].providerId !=
                                    null) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => ShopDetailsById(
                                            id: snapshot
                                                .data.data[index].providerId,
                                          )));
                                } else {
                                  _launchURL(snapshot.data.data[index].url);
                                }
                              },
                              child: snapshot.data.data[index].photo != null
                                  ? CachedNetworkImage(
                                      imageUrl: snapshot.data.data[index].photo,
                                      fadeInDuration: Duration(seconds: 2),
                                      placeholder: (context, url) => Container(
                                            decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/icon-001.png'),
                                                  fit: BoxFit.fill),
                                            ),
                                          ),
                                      imageBuilder: (context, provider) {
                                        return Container(
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: provider,
                                                fit: BoxFit.fill),
                                          ),
                                        );
                                      })
                                  : Container(
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'assets/images/icon-001.png'),
                                            fit: BoxFit.fill),
                                      ),
                                    ),
                            );
                          }),
                          autoplay: false,
                          dotIncreasedColor: Theme.of(context).primaryColor,
                          dotIncreaseSize: 2,
                          dotSize: 5,
                          dotSpacing: snapshot.data.data.length > 13
                              ? 300 / snapshot.data.data.length
                              : 20,
                          autoplayDuration: Duration(seconds: 1),
                          dotBgColor: Color(0x00000000),
                          animationCurve: Curves.decelerate,
                          animationDuration: Duration(milliseconds: 1000),
                          indicatorBgPadding: 10,
                          dotColor: Colors.white,
                        ),
                      );
          }
        });
  }

  @override
  void didChangeDependencies() async {
    if (isInit) {
      await _getShared();
      setState(() {
        isInit = false;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        drawer: MyDrawer(),
        key: _keyDrawer,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(mediaQuery.height * 0.3),
          child: AppBar(
            leading: Icon(null),
            backgroundColor: Colors.white,
            flexibleSpace: Stack(
                alignment: Alignment.center,
                clipBehavior: Clip.none,
                children: [
                  _imageSlider(),
                  Positioned(
                    child: Container(
                      height: mediaQuery.height * 0.04,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: ClipOval(
                                child: Material(
                                  color: Theme.of(context).primaryColor,
                                  child: InkWell(
                                    splashColor: Theme.of(context).accentColor,
                                    child: SizedBox(
                                        width: 30,
                                        height: 30,
                                        child: Icon(
                                          Icons.menu,
                                          size: 15,
                                          color: Colors.white,
                                        )),
                                    onTap: () {
                                      _keyDrawer.currentState.openDrawer();
                                    },
                                  ),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SearchScreen()));
                              },
                              child: Container(
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(201, 202, 207, 0.8),
                                      borderRadius: BorderRadius.circular(25)),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 8.0,
                                    ),
                                    child: Container(
                                      alignment: Alignment.center,
                                      width: mediaQuery.width * 0.7,
                                      child: TextFormField(
                                        keyboardType: TextInputType.name,
                                        enabled: false,
                                        textCapitalization:
                                            TextCapitalization.sentences,
                                        textInputAction: TextInputAction.search,
                                        decoration: InputDecoration(
                                          contentPadding:
                                              EdgeInsets.only(top: 6.0),
                                          isDense: true,
                                          hintText: 'البحث',
                                          suffixIcon: Container(
                                            decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    234, 237, 244, 0.8),
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(25),
                                                    topLeft:
                                                        Radius.circular(25))),
                                            child: Icon(
                                              Icons.search,
                                              size: 20,
                                              color: Colors.green,
                                            ),
                                          ),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  )),
                            ),
                            PopupMenuButton(
                              padding: EdgeInsets.zero,
                              onSelected: (filter) {
                                checkProvider.filterType = filter;
                                checkProvider.sortShops();
                              },
                              // icon: Icon(
                              //   Icons.more_vert,
                              //   color: Colors.white,
                              // ),
                              icon: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: ClipOval(
                                  child: Material(
                                    color: Theme.of(context).primaryColor,
                                    child: InkWell(
                                      splashColor:
                                          Theme.of(context).accentColor,
                                      child: SizedBox(
                                          width: 30,
                                          height: 30,
                                          child: Icon(
                                            Icons.more_vert,
                                            size: 15,
                                            color: Colors.white,
                                          )),
                                      // onTap: () {
                                      //   _keyDrawer.currentState
                                      //       .openDrawer();
                                      // },
                                    ),
                                  ),
                                ),
                              ),
                              itemBuilder: (ctx) => [
                                CheckedPopupMenuItem(
                                  checked: (checkProvider.filterType ==
                                      FilterType.NumberOfProducts),
                                  child: Text('حسب عدد المنتجات'),
                                  value: FilterType.NumberOfProducts,
                                ),
                                CheckedPopupMenuItem(
                                  checked: checkProvider.filterType ==
                                      FilterType.Nearest,
                                  child: Text('حسب الأقرب لك'),
                                  value: FilterType.Nearest,
                                ),
                                CheckedPopupMenuItem(
                                  checked: checkProvider.filterType ==
                                      FilterType.MostViews,
                                  child: Text('حسب الأكثر مشاهدة'),
                                  value: FilterType.MostViews,
                                ),
                              ],
                            ),
                          ]),
                    ),
                    top: mediaQuery.height * 0.01,
                    width: mediaQuery.width,
                  ),
                  if (Provider.of<MapHelper>(context, listen: false).position ==
                      null)
                    Positioned(
                      bottom: 1,
                      width: mediaQuery.width,
                      child: InkWell(
                        onTap: () async {
                          await Provider.of<MapHelper>(context, listen: false)
                              .getLocation();
                          if (Provider.of<MapHelper>(context, listen: false)
                                  .position !=
                              null) {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MainPage()),
                                (Route<dynamic> route) => false);
                          }
                        },
                        child: Container(
                          color: Colors.red,
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Shimmer.fromColors(
                              baseColor: Colors.white,
                              highlightColor: Colors.yellow,
                              child: Text(
                                'انت لم تقم بتفعيل خاصية اللوكيشن ،لذا يرجي الضغط لتفعيلها',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                ]),
          ),
        ),
        body: Column(children: [
          SizedBox(
            height: mediaQuery.height * 0.03,
          ),
          DepartmentFilter(),
          SizedBox(
            height: mediaQuery.height * 0.03,
          ),
          Expanded(
            child: Container(
              width: mediaQuery.width * 0.9,
              child: isInit ? AppLoader() : HomeBody(),
            ),
          ),
        ]),
      ),
    );
  }
}
