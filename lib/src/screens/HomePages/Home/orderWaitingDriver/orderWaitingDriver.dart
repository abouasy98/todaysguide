import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/get/offersProvider.dart';
import 'package:todaysguide/src/provider/get/setting.dart';
import 'package:todaysguide/src/provider/post/CancelOrderOffer.dart';
import 'package:todaysguide/src/provider/post/ClientOrderProvider.dart';
import 'package:todaysguide/src/provider/post/acceptOfferProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/shopItem.dart';
import 'package:todaysguide/src/screens/HomePages/Home/homeItem.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/weekcountdawn.dart';

class OrderWaitingDriver extends StatefulWidget {
  @override
  _OrderWaitingDriverState createState() => _OrderWaitingDriverState();
}

class _OrderWaitingDriverState extends State<OrderWaitingDriver> {
  ClientOrdersProvider clientOrdersProvider;
  AcceptOfferProvider acceptOfferProvider;
  OffersProvider offersProvider;

  SettingProvider settingProvider;
  bool loading = true;
  CustomDialog _dialog = CustomDialog();
  @override
  void didChangeDependencies() async {
    if (loading) {
      settingProvider = Provider.of<SettingProvider>(context, listen: false);
      await Provider.of<ClientOrdersProvider>(context, listen: false)
          .getClientOrders(
              Provider.of<SharedPref>(context, listen: false).token,
              0,
              context);
      clientOrdersProvider =
          Provider.of<ClientOrdersProvider>(context, listen: false);
      acceptOfferProvider =
          Provider.of<AcceptOfferProvider>(context, listen: false);
      await Provider.of<OffersProvider>(context, listen: false).getOffers(
          context: context, id: clientOrdersProvider.clientOrders[0].id);
      print('adddddddddd=${clientOrdersProvider.clientOrders[0].id}');
      offersProvider = Provider.of<OffersProvider>(context, listen: false);

      setState(() {
        loading = false;
      });
    }

    super.didChangeDependencies();
  }

  Future<bool> _onBackPressed() async {
    var alert = await _dialog.showOptionDialog(
        context: context,
        msg: 'هل انت متاكد سوف يتم الغاء الطلب ؟',
        okFun: () async {
          Provider.of<CancelOrderProvider>(context, listen: false)
              .clientCancelOder(
                  Provider.of<SharedPref>(context, listen: false).token,
                  clientOrdersProvider.clientOrders[0].id,
                  'العميل قام بإلغاء الطلب بنفسه',
                  context);
        },
        okMsg: 'نعم',
        cancelMsg: 'لا',
        cancelFun: () {
          return;
        });
    return alert;
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          appBar: AppBar(
            title: Text('طلب بانتظار السائقين'),
            centerTitle: true,
            leading: Container(),
          ),
          body: loading
              ? AppLoader()
              : ((clientOrdersProvider == null) ||
                      (clientOrdersProvider.clientOrders.length <= 0))
                  ? Container(
                      height: MediaQuery.of(context).size.height * 0.2,
                      child: Center(
                        child: Text(
                          "لا يوجد منتجات",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                  : ListView(primary: false, shrinkWrap: true, children: [
                      SizedBox(
                        height: mediaQuery.height * 0.05,
                      ),
                      Center(
                        child: Container(
                          height: mediaQuery.height * 0.2,
                          width: mediaQuery.width * 0.9,
                          alignment: Alignment.topCenter,
                          child: Dismissible(
                            direction: DismissDirection.startToEnd,
                            key: UniqueKey(),
                            onDismissed: (direction) {
                              if (direction == DismissDirection.startToEnd) {}
                            },
                            confirmDismiss: (direction) async {
                              return await _dialog.showOptionDialog(
                                  context: context,
                                  msg: 'هل ترغب بحذف الطلب؟',
                                  okFun: () {
                                    Provider.of<CancelOrderProvider>(context,
                                            listen: false)
                                        .clientCancelOder(
                                            Provider.of<SharedPref>(context,
                                                    listen: false)
                                                .token,
                                            clientOrdersProvider
                                                .clientOrders[0].id,
                                            'العميل قام بإلغاء الطلب بنفسه',
                                            context);
                                  },
                                  okMsg: 'نعم',
                                  cancelMsg: 'لا',
                                  cancelFun: () {
                                    return;
                                  });
                            },
                            background: Container(
                              color: Theme.of(context).errorColor,
                              child: Icon(
                                Icons.delete,
                                color: Colors.white,
                                size: 40,
                              ),
                              alignment: Alignment.centerRight,
                              padding: EdgeInsets.only(right: 20),
                              margin: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 4),
                            ),
                            child: ShopItem(
                              orderWaiting: true,
                              productsFilter: null,
                              price: clientOrdersProvider
                                  .clientOrders[0].totalPrice,
                              photo: clientOrdersProvider
                                  .clientOrders[0].shopPhoto,
                              name: clientOrdersProvider.clientOrders[0].shop,
                              id: clientOrdersProvider.clientOrders[0].shopId,
                              type: 2,
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Container(
                          height: mediaQuery.height * 0.8,
                          width: mediaQuery.width * 0.9,
                          child: ListView(
                            children: [
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                      'سينتهي الطلب في خلال',
                                      style: TextStyle(
                                        fontSize: 10,
                                      ),
                                    ),
                                    WeekCountdown(
                                      period: settingProvider
                                          .userDataModel.data.cancelDuration,
                                      createdAt: clientOrdersProvider
                                          .clientOrders[0].orderTime,
                                    ),
                                    Text(
                                      'في حالة عدم اختيار عرض سعر',
                                      style: TextStyle(
                                        fontSize: 10,
                                      ),
                                    ),
                                  ]),
                              SizedBox(
                                height: mediaQuery.height * 0.05,
                              ),
                              if (offersProvider.offers == null ||
                                  offersProvider.offers.isEmpty)
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.3,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Center(
                                          child: Text(
                                        'لايوجد سائقين متاحين الآن',
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),
                                      )),
                                      SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.05),
                                      Container(
                                        height: mediaQuery.height * 0.05,
                                        width: mediaQuery.width * 0.35,
                                        child: RaisedButton(
                                          elevation: 8,
                                          color: Theme.of(context).primaryColor,
                                          child: Text(
                                            'إلغاء الطلب',
                                            style: TextStyle(fontSize: 15),
                                          ),
                                          onPressed: () async {
                                            await _dialog.showOptionDialog(
                                                context: context,
                                                msg:
                                                    'سيتم إلغاء طلبك , هل أنت متأكد؟',
                                                okFun: () async {
                                                  Provider.of<CancelOrderProvider>(
                                                          context,
                                                          listen: false)
                                                      .clientCancelOder(
                                                          Provider.of<SharedPref>(
                                                                  context,
                                                                  listen: false)
                                                              .token,
                                                          clientOrdersProvider
                                                              .clientOrders[0]
                                                              .id,
                                                          'العميل قام بإلغاء الطلب بنفسه',
                                                          context);
                                                },
                                                okMsg: 'نعم',
                                                cancelMsg: 'لا',
                                                cancelFun: () {
                                                  return;
                                                });
                                          },
                                          textColor: Colors.white,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              if (offersProvider.offers != null &&
                                  offersProvider.offers.isNotEmpty)
                                ListView.builder(
                                  primary: false,
                                  shrinkWrap: true,
                                  itemCount: offersProvider.offers.length,
                                  itemBuilder: (context, i) => HomeItem(
                                    photo: offersProvider.offers[i].driverPhoto,
                                    name: offersProvider.offers[i].driver,
                                    price: offersProvider.offers[i].driverPrice,
                                    search: false,
                                    driverId: offersProvider.offers[i].driverId,
                                    id: offersProvider.offers[i].id,
                                    type: 2,
                                    carType: offersProvider.offers[i].carType,
                                  ),
                                ),
                            ],
                          ),
                        ),
                      )
                    ]),
        ),
      ),
    );
  }
}
