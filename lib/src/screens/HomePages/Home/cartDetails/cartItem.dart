import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/get/get_cart_provider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/productDetails/productDetailsbyId.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';

// ignore: must_be_immutable
class CartItem extends StatelessWidget {
  final String photo;
  final String name;
  final String price;
  final int quantity;
  final Key key;

  final int productId;
  final int id;
  CartItem(
      {this.name,
      this.photo,
      this.price,
      this.id,
      this.key,
      this.productId,
      this.quantity,
     });
  CustomDialog _dialog = CustomDialog();
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ProductDetailsById(
                  id: productId,
                )));
      },
      child: Dismissible(
        direction: DismissDirection.startToEnd,
        key: key,
        onDismissed: (direction) {
          if (direction == DismissDirection.startToEnd) {}
        },
        confirmDismiss: (direction) async {
          return await _dialog.showOptionDialog(
              context: context,
              msg: 'هل ترغب بحذف الطلب؟',
              okFun: () {
                Provider.of<GetCartProvider>(
                  context,
                  listen: false,
                ).deleteCart(context, id,Provider.of<SharedPref>(context, listen: false).token);
              },
              okMsg: 'نعم',
              cancelMsg: 'لا',
              cancelFun: () {
                return;
              });
        },
        background: Container(
          color: Theme.of(context).errorColor,
          child: Icon(
            Icons.delete,
            color: Colors.white,
            size: 40,
          ),
          alignment: Alignment.centerRight,
          padding: EdgeInsets.only(right: 20),
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
        ),
        child: Container(
          //  height: mediaQuery.height * 0.1,
          width: mediaQuery.width * 0.9,
          alignment: Alignment.center,
          child: Card(
            elevation: 10,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: ListTile(
              isThreeLine: true,
              leading: Container(
                alignment: Alignment.center,
                height: mediaQuery.height * 0.12,
                width: mediaQuery.width * 0.15,
                child:photo!=null? CachedNetworkImage(
                  imageUrl: photo,
                  fadeInDuration: Duration(seconds: 2),
                  placeholder: (context, url) => Container(
                      decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                        image: AssetImage('assets/images/newLogo.png'),
                        fit: BoxFit.fill),
                  )),
                  imageBuilder: (context, provider) {
                    return Container(
                        decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(image: provider, fit: BoxFit.fill),
                    ));
                  },
                ):Container(
                      decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                        image: AssetImage('assets/images/newLogo.png'),
                        fit: BoxFit.fill),
                  )),
              ),
              title: Container(
               // height: mediaQuery.height * 0.05,
                child: Text(
                  name,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
              ),
              trailing: Container(
                width: mediaQuery.width * 0.2,
         //       alignment: Alignment.topLeft,
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        '${NumberFormat.currency(decimalDigits: 0).parse(price)}',
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 15),
                      ),
                      Text(
                        'SR',
                        style: TextStyle(color: Colors.grey, fontSize: 15),
                      ),
                    ]),
              ),
              subtitle: Container(
                 height: mediaQuery.height * 0.05,
               
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('الكمية : ',
                        style: TextStyle(color: Colors.grey, fontSize: 14)),
                    Text(
                      '$quantity',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor, fontSize: 15),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
