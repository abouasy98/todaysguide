// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:todaysguide/src/provider/post/post_current_order_provider.dart';
// import 'package:todaysguide/src/screens/MainWidgets/customBtn.dart';
// import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
// import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
// import 'package:todaysguide/src/screens/MainWidgets/details_text_field_no_img%20copy.dart';

// class FinishOrder extends StatefulWidget {
//   const FinishOrder({
//     Key key,
//   }) : super(key: key);
//   @override
//   _FinishOrderState createState() => _FinishOrderState();
// }

// class _FinishOrderState extends State<FinishOrder> {
//   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
//   @override
//   void initState() {
//     super.initState();
//   }
//   CustomDialog _dialog = CustomDialog();
//   // bool location = false;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       key: _scaffoldKey,
//       appBar: AppBar(
//         title: Text('تاكيد الطلب'),
//         centerTitle: true,
//       ),
//       body: ListView(
//         children: <Widget>[
//           SizedBox(
//             height: 20,
//           ),
//           Material(
//             color: Colors.white,
//             child: Padding(
//               padding: const EdgeInsets.only(right: 10, left: 10),
//               child: Container(
//                 decoration: BoxDecoration(
//                   //color: location ? Colors.green : Colors.grey[200],
//                   color: Colors.grey[200],
//                   borderRadius: BorderRadius.all(Radius.circular(20)),
//                 ),
//                 child: Padding(
//                   padding: const EdgeInsets.all(10.0),
//                   child: TextField(
//                     onChanged: (v) {
//                       Provider.of<PostCurrentOrderProvider>(context,
//                               listen: false)
//                           .addressDetails = v;
//                     },
//                     textAlign: TextAlign.right,
//                     keyboardType: TextInputType.text,
//                     decoration: InputDecoration(
//                         floatingLabelBehavior: FloatingLabelBehavior.always,
//                         contentPadding: EdgeInsets.only(top: 20, right: 10),
//                         border: new OutlineInputBorder(
//                           borderRadius:
//                               const BorderRadius.all(Radius.circular(10)),
//                         ),
//                         hintStyle: TextStyle(fontSize: 15, height: .8),
//                         hintText: 'رقم المنزل/العمارة/الشقة (اختياري)'),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           SizedBox(
//             height: 20,
//           ),
//           SizedBox(
//             height: 20,
//           ),
//           Material(
//             color: Colors.white,
//             child: Padding(
//               padding: const EdgeInsets.only(right: 10, left: 10),
//               child: Container(
//                 decoration: BoxDecoration(
//                   //color: location ? Colors.green : Colors.grey[200],
//                   color: Colors.grey[200],
//                   borderRadius: BorderRadius.all(Radius.circular(20)),
//                 ),
//                 child: Padding(
//                   padding: EdgeInsets.all(10),
//                   child: DetailsTextFieldNoImg(
//                     label: 'تفاصيل الطلب',
//                     onChange: (v) {
//                       Provider.of<PostCurrentOrderProvider>(context,
//                               listen: false)
//                           .orderDetails = v;
//                     },
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           SizedBox(
//             height: 20,
//           ),
//           Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: CustomBtn(
//               color: Theme.of(context).primaryColor,
//               onTap: ()async {
//                 if (Provider.of<PostCurrentOrderProvider>(context,
//                                 listen: false)
//                             .orderDetails ==
//                         null ||
//                     Provider.of<PostCurrentOrderProvider>(context,
//                                 listen: false)
//                             .addressDetails ==
//                         null) {
//                   CustomAlert().toast(
//                     context: context,
//                     title: "يجب كتابة كافة تفاصيل للطلب",
//                   );
//                   return;
//                 }

//                         await _dialog.showOptionDialog(
//                         context: context,
//                         msg: 'هل ترغب بتأكيد الطلب؟',
//                         okFun: () {
//                       Provider.of<PostCurrentOrderProvider>(context, listen: false)
//                     .postCart(context);
//                         },
//                         okMsg: 'نعم',
//                         cancelMsg: 'لا',
//                         cancelFun: () {
//                           return;
//                         });
//                 print(Provider.of<PostCurrentOrderProvider>(context,
//                         listen: false)
//                     .addressDetails);
//                 print(Provider.of<PostCurrentOrderProvider>(context,
//                         listen: false)
//                     .orderDetails);

          
//               },
//               text: 'تأكيد الطلب',
//               txtColor: Colors.white,
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
