import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/provider/get/get_cart_provider.dart';
import 'package:todaysguide/src/provider/post/post_current_order_provider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/shopDetailsById.dart';
import 'package:todaysguide/src/screens/HomePages/Home/cartDetails/cartItem.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/MainWidgets/customBtn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_bottom_sheet.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/details_text_field_no_img%20copy.dart';

class CartDetails extends StatefulWidget {
  @override
  _CartDetailsState createState() => _CartDetailsState();
}

class _CartDetailsState extends State<CartDetails> {
  //int list = 3;
  //bool isInit = true;
  // bool isLoading = false;
  //GetCartProvider getCartData;
  //GetCartProvider emptyCartProvider;

  int totalPrice;
  String errorMessage;
  String errorAddressMessage;
  // List<getCartModl.Datum> myCartItem = [];

  PostCurrentOrderProvider currentOrderProvider;
  // CustomDialog _dialog = CustomDialog();
  CustomDialog _dialog = CustomDialog();
  Widget button({
    String title,
    Function func,
  }) {
    return RaisedButton(
      elevation: 10,
      color: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      onPressed: func,
      child: Text(
        title,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }

  getShared() async {
    currentOrderProvider = Provider.of<PostCurrentOrderProvider>(
      context,
      listen: false,
    );
  }

  @override
  void initState() {
    getShared();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'السلة',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: Center(
          child: Container(
            alignment: Alignment.topCenter,
            width: mediaQuery.width * 0.9,
            child: FutureBuilder(
                future: Provider.of<GetCartProvider>(context, listen: false)
                    .getCart(context),
                builder: (context, snapShot) {
                  if (snapShot.connectionState == ConnectionState.none) {
                    return Text('يرجي اعادة الاتصال بالانترنت');
                  }
                  if (snapShot.connectionState == ConnectionState.waiting) {
                    return AppLoader();
                  }
                  if (!snapShot.hasData) {
                    return Center(
                      child: Text(
                        'السلة فارغه',
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                  return Consumer<GetCartProvider>(builder: (context, snap, _) {
                    if (snap.carts.length <= 0) {
                      return Center(
                        child: Text(
                          'السلة فارغه',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                      );
                    }
                    return ListView(
                        primary: false,
                        shrinkWrap: true,
                        children: [
                          SizedBox(
                            height: mediaQuery.height * 0.03,
                          ),
                          Text(" المتجر : ${snap.carts[0].shopName}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20)),
                          SizedBox(
                            height: mediaQuery.height * 0.03,
                          ),
                          ListView.builder(
                            primary: false,
                            shrinkWrap: true,
                            itemCount: snap.carts.length,
                            itemBuilder: (context, i) => CartItem(
                              productId: snap.carts[i].productId,
                              name: snap.carts[i].productName,
                              quantity: snap.carts[i].quantity,
                              key: UniqueKey(),
                              photo: snap.carts[i].photos[0].photo,
                              id: snap.carts[i].id,
                              price: snap.carts[i].price,
                            ),
                          ),
                        ]);
                  });
                }),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        alignment: Alignment.center,
        height: mediaQuery.height * 0.2,
        width: mediaQuery.width * 0.9,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            button(
                title: 'اكمال التسويق',
                func: () {
                  if (Provider.of<GetCartProvider>(
                        context,
                        listen: false,
                      ).carts.isEmpty ||
                      Provider.of<GetCartProvider>(
                            context,
                            listen: false,
                          ).carts ==
                          null) {
                    Navigator.of(context).pop();
                  } else {
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => ShopDetailsById(
                              id: Provider.of<GetCartProvider>(
                                context,
                                listen: false,
                              ).carts[0].shopId,
                            )));
                  }
                }),
            button(
                title: 'افراغ السلة',
                func: () async {
                  if (Provider.of<GetCartProvider>(
                        context,
                        listen: false,
                      ).carts.length >
                      0) {
                    await _dialog.showOptionDialog(
                        context: context,
                        msg: 'سيتم حذف جميع المنتجات من السلة متأكد؟',
                        okFun: () async {
                          await Provider.of<GetCartProvider>(
                            context,
                            listen: false,
                          )
                              .emptyCart(context)
                              .whenComplete(() => setState(() {}));
                        },
                        okMsg: 'نعم',
                        cancelMsg: 'لا',
                        cancelFun: () {
                          return;
                        });
                  }
                }),
            button(
                title: 'اعتماد الطلب',
                func: () async {
                  if (Provider.of<GetCartProvider>(
                        context,
                        listen: false,
                      ).carts.length >
                      0) {
                    CustomBottomSheet().show(
                        context: context,
                        child: Padding(
                          padding: const EdgeInsets.all(20),
                          child: ListView(
                            children: [
                              SizedBox(
                                height: 20,
                              ),
                              Material(
                                color: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 10, left: 10),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      //color: location ? Colors.green : Colors.grey[200],
                                      color: Colors.grey[200],
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: TextField(
                                        onChanged: (v) {
                                          Provider.of<PostCurrentOrderProvider>(
                                                  context,
                                                  listen: false)
                                              .addressDetails = v;
                                        },
                                        textAlign: TextAlign.right,
                                        maxLines: null,
                                        keyboardType: TextInputType.text,
                                        decoration: InputDecoration(
                                            floatingLabelBehavior:
                                                FloatingLabelBehavior.always,
                                            contentPadding: EdgeInsets.only(
                                                top: 20, right: 10, left: 10),
                                            border: new OutlineInputBorder(
                                              borderRadius:
                                                  const BorderRadius.all(
                                                      Radius.circular(10)),
                                            ),
                                            hintStyle: TextStyle(
                                                fontSize: 15, height: .8),
                                            hintText:
                                                'رقم المنزل/العمارة/الشقة (اختياري)'),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Material(
                                color: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 10, left: 10),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      //color: location ? Colors.green : Colors.grey[200],
                                      color: Colors.grey[200],
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.all(10),
                                      child: DetailsTextFieldNoImg(
                                        label: 'تفاصيل الطلب',
                                        onChange: (v) {
                                          Provider.of<PostCurrentOrderProvider>(
                                                  context,
                                                  listen: false)
                                              .orderDetails = v;
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CustomBtn(
                                  color: Theme.of(context).primaryColor,
                                  onTap: () async {
                                    if (Provider.of<PostCurrentOrderProvider>(
                                                    context,
                                                    listen: false)
                                                .orderDetails ==
                                            null ||
                                        Provider.of<PostCurrentOrderProvider>(
                                                    context,
                                                    listen: false)
                                                .addressDetails ==
                                            null) {
                                      CustomAlert().toast(
                                        context: context,
                                        title: "يجب كتابة كافة تفاصيل للطلب",
                                      );
                                      return;
                                    }

                                    await _dialog.showOptionDialog(
                                        context: context,
                                        msg: 'هل ترغب بتأكيد الطلب؟',
                                        okFun: () {
                                          Provider.of<PostCurrentOrderProvider>(
                                                  context,
                                                  listen: false)
                                              .postCart(context);
                                        },
                                        okMsg: 'نعم',
                                        cancelMsg: 'لا',
                                        cancelFun: () {
                                          return;
                                        });
                                    print(Provider.of<PostCurrentOrderProvider>(
                                            context,
                                            listen: false)
                                        .addressDetails);
                                    print(Provider.of<PostCurrentOrderProvider>(
                                            context,
                                            listen: false)
                                        .orderDetails);
                                  },
                                  text: 'تأكيد الطلب',
                                  txtColor: Colors.white,
                                ),
                              )
                            ],
                          ),
                        ));
                  }
                }),
          ],
        ),
      ),
    );
  }
}
