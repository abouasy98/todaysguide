import 'dart:io';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Models/get/productByIdModel.dart';
import 'package:todaysguide/src/provider/get/get_cart_provider.dart';
import 'package:todaysguide/src/provider/get/productByIdProvider.dart';
import 'package:todaysguide/src/provider/get/productCartCountProvider.dart';
import 'package:todaysguide/src/provider/post/add_to_cart_provider.dart';
import 'package:todaysguide/src/provider/post/reportProductProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/cartDetails/cartDetails.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/MainWidgets/customBtn.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_bottom_sheet.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/newsPhotoGallary.dart';
import 'package:intl/intl.dart' as intl;
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';

import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';

class ProductDetailsById extends StatefulWidget {
  final int id;
  final Key key;
  ProductDetailsById({this.id, this.key});
  @override
  _ProductDetailsByIdState createState() => _ProductDetailsByIdState();
}

class _ProductDetailsByIdState extends State<ProductDetailsById> {
  Widget positionONImage() {
    final mediaQuery = MediaQuery.of(context).size;
    return Container(
      width: mediaQuery.width * 0.3,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
            backgroundColor: Colors.red[400],
            radius: 15,
            child: IconButton(
              iconSize: 15,
              onPressed: () async {
                if (Provider.of<SharedPref>(context, listen: false).token ==
                    null) {
                  return await _dialog.showOptionDialog(
                      context: context,
                      msg: 'للابلاغ يرجي التسجيل اولا',
                      okFun: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (c) => SignInScreen()));
                      },
                      okMsg: 'نعم',
                      cancelMsg: 'لا',
                      cancelFun: () {
                        return;
                      });
                } else {
                  CustomBottomSheet().show(
                      context: context,
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            RegisterTextField(
                              onChange: (v) {
                                Provider.of<ReportProductProvider>(context,
                                        listen: false)
                                    .details = v;
                              },
                              label: 'سبب الابلاغ',
                              icon: Icons.label,
                              type: TextInputType.text,
                            ),
                            SizedBox(height: 20),
                            CustomBtn(
                              text: 'تاكيد الابلاغ',
                              color: Colors.red,
                              onTap: () async {
                                Navigator.pop(context);
                                await Provider.of<ReportProductProvider>(
                                        context,
                                        listen: false)
                                    .reportShop(
                                        widget.id,
                                        Provider.of<SharedPref>(context,
                                                listen: false)
                                            .token,
                                        context);
                              },
                              txtColor: Colors.white,
                            )
                          ],
                        ),
                      ));
                }
              },
              icon: Icon(
                Icons.priority_high,
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(width: 5),
          CircleAvatar(
            backgroundColor: Colors.white60,
            radius: 15,
            child: IconButton(
              iconSize: 15,
              onPressed: () async {
                String _msg;
                StringBuffer _sb = new StringBuffer();
                setState(() {
                  _sb.write(
                      "اسم المنتج : ${productById.restourant.data.name} \n");

                  _sb.write(shareLink);

                  _msg = _sb.toString();
                });
                if (productById.restourant.data.photos != null) {
                  if (productById.restourant.data.photos.length != 0) {
                    var request = await HttpClient().getUrl(
                        Uri.parse(productById.restourant.data.photos[0].photo));
                    var response = await request.close();
                    Uint8List bytes =
                        await consolidateHttpClientResponseBytes(response);
                    await Share.file(
                        'ESYS AMLOG', 'amlog.jpg', bytes, 'image/jpg',
                        text: _msg);
                  } else {
                    Share.text("title", _msg, 'text/plain');
                  }
                } else {
                  Share.text("title", _msg, 'text/plain');
                }
              },
              icon: Icon(
                Icons.share,
                color: Color.fromRGBO(72, 83, 103, 1),
              ),
            ),
          ),
        ],
      ),
    );
  }

  String shareLink;
  CustomDialog _dialog = CustomDialog();
  bool isInit = true;
  bool loaded = false;
  ProductCartCountProvider productCartCountProvider;

  ProductsByIdProvider productById;
  int total = 1;
  ProductByIdModel _model;
  List<String> photos = [];
  Future<void> _createDynamicLink() async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://todaysguide.page.link',
      link: Uri.parse('https://todaysguide.page.link/1/${widget.id}'),
      androidParameters: AndroidParameters(
        packageName: 'com.tqnee.todaysguide',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.tqnee.todaysguide',
        minimumVersion: "4",
      ),
    );

    Uri url;
    final ShortDynamicLink shortLink = await parameters.buildShortLink();
    url = shortLink.shortUrl;
    setState(() {
      shareLink = url.toString();
    });
  }

  @override
  void didChangeDependencies() async {
    if (isInit) {


      await Provider.of<ProductsByIdProvider>(context, listen: false)
          .getShops(widget.id,
              Provider.of<SharedPref>(context, listen: false).token, context)
          .then((res) {
        setState(() {
          _model = res;
        });
        if (_model.data.photos != null) {
          for (int i = 0; i < _model.data.photos.length; i++) {
            photos.add(_model.data.photos[i].photo);
          }
        }
      });
    _createDynamicLink();
      productById = Provider.of<ProductsByIdProvider>(context, listen: false);
      if (Provider.of<SharedPref>(context, listen: false).token != null) {
        await Provider.of<ProductCartCountProvider>(context, listen: false)
            .productCartCount(
                Provider.of<SharedPref>(context, listen: false).token, context);
        productCartCountProvider =
            Provider.of<ProductCartCountProvider>(context, listen: false);
      }
      setState(() {
        isInit = false;
      });
    }
    super.didChangeDependencies();
  }

  Widget _imageSlider() {
    final mediaQuery = MediaQuery.of(context).size;
    return Stack(
      // overflow: Overflow.visible,
      children: [
        Container(
          height: mediaQuery.height * 0.3,
          width: mediaQuery.width,
          child: Carousel(
            boxFit: BoxFit.fill,
            images: List.generate(productById.restourant.data.photos.length,
                (int index) {
              return InkWell(
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => NewsPhotoGallary(
                              images: photos,
                            ))),
                child: productById.restourant.data.photos[index].photo != null
                    ? CachedNetworkImage(
                        imageUrl:
                            productById.restourant.data.photos[index].photo,
                        fadeInDuration: Duration(seconds: 2),
                        placeholder: (context, url) => Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/icon-001.png'),
                                    fit: BoxFit.fill),
                              ),
                            ),
                        imageBuilder: (context, provider) {
                          return Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: provider, fit: BoxFit.fill),
                            ),
                          );
                        })
                    : Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/images/icon-001.png'),
                              fit: BoxFit.fill),
                        ),
                      ),
              );
            }),
            autoplay: false,
            dotIncreasedColor: Theme.of(context).primaryColor,
            dotIncreaseSize: 2,
            dotSize: 5,
            dotBgColor: Color(0x00000000),
            dotSpacing: productById.productsById.length > 13
                ? 300 / productById.productsById.length
                : 20,
            autoplayDuration: Duration(seconds: 1),
            animationCurve: Curves.decelerate,
            animationDuration: Duration(milliseconds: 1000),
            indicatorBgPadding: 10,
            dotColor: Colors.white,
          ),
        ),
        Positioned(
          child: positionONImage(),
          left: 10,
          top: 10,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(
            isInit ? 0 : MediaQuery.of(context).size.height * 0.37),
        child: isInit
            ? SpinKitThreeBounce(
                color: Colors.white,
                size: 22,
              )
            : AppBar(
                title: Text(productById.restourant.data.name),
                centerTitle: true,
                leading: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(Icons.arrow_back_ios),
                ),
                actions: <Widget>[
                  if (Provider.of<SharedPref>(context, listen: false).token !=
                      null)
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Stack(alignment: Alignment.center, children: [
                          IconButton(
                            onPressed: () async {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => CartDetails()));
                            },
                            icon: Icon(Icons.shopping_cart),
                          ),
                          FutureBuilder(
                              future: Provider.of<ProductCartCountProvider>(
                                      context,
                                      listen: false)
                                  .productCartCount(
                                      Provider.of<SharedPref>(context,
                                              listen: false)
                                          .token,
                                      context),
                              builder: (context, snapShot) {
                                return Consumer<ProductCartCountProvider>(
                                  builder: (ctx, snap, ch) => Visibility(
                                    visible: snap.reportShopModel.data
                                            .productCartCount !=
                                        0,
                                    child: Positioned(
                                      top: -1,
                                      left: mediaQuery.width * 0.01,
                                      child: CircleAvatar(
                                        backgroundColor: Colors.red[400],
                                        radius: 10,
                                        child: Text(
                                          '${snap.reportShopModel.data.productCartCount}',
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ])),
                ],
                bottom: PreferredSize(
                  preferredSize: mediaQuery,
                  child: Stack(
                    alignment: Alignment.center,
                    clipBehavior: Clip.none,
                    children: [
                      if (productById.restourant.data.photos != null)
                        _imageSlider(),
                      if (productById.restourant.data.photos == null)
                        Stack(
                          // overflow: Overflow.visible,
                           children: [
                          Container(
                            height: mediaQuery.height * 0.3,
                            width: mediaQuery.width,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/newLogo.png'))),
                          ),
                          Positioned(
                            child: positionONImage(),
                            left: 10,
                            top: 10,
                          ),
                        ]),
                    ],
                  ),
                )),
      ),
      body: isInit
          ? AppLoader()
          : Center(
              child: Container(
                alignment: Alignment.topCenter,
                width: mediaQuery.width * 0.9,
                child: ListView(
                  primary: false,
                  shrinkWrap: true,
                  children: [
                    SizedBox(
                      height: mediaQuery.height * 0.03,
                    ),
                    Row(
                      children: [
                        Row(children: [
                          Text(
                            'SR',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontSize: 17,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            width: mediaQuery.width * 0.01,
                          ),
                          Text(
                            '${intl.NumberFormat.currency(decimalDigits: 0).parse(productById.restourant.data.price)}',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontSize: 17,
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ]),
                        Expanded(child: Text('')),
                        Row(children: [
                          Text(
                            '${productById.restourant.data.category}',
                            textAlign: TextAlign.right,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 13,
                              color: Colors.grey,
                            ),
                          ),
                          Text(
                            ' : قسم ',
                            textAlign: TextAlign.right,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              fontSize: 13,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ]),
                      ],
                    ),
                    SizedBox(
                      height: mediaQuery.height * 0.03,
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                      Text(
                        '  علبة ',
                        textAlign: TextAlign.right,
                        overflow: TextOverflow.ellipsis,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                      Text(
                        '${productById.restourant.data.available}',
                        textAlign: TextAlign.right,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(
                        ' :  مخزون المنتج',
                        textAlign: TextAlign.right,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 13,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ]),
                    SizedBox(
                      height: mediaQuery.height * 0.03,
                    ),
                    Text(
                      ': وصف المنتج ',
                      textAlign: TextAlign.right,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        productById.restourant.data.details,
                        textAlign: TextAlign.right,
                        style: TextStyle(fontSize: 15, color: Colors.grey),
                      ),
                    ),
                    SizedBox(
                      height: mediaQuery.height * 0.03,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.05,
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Visibility(
                              visible: Provider.of<SharedPref>(context,
                                          listen: false)
                                      .token ==
                                  null,
                              child: RaisedButton(
                                textColor: Colors.white,
                                color: Theme.of(context).primaryColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                ),
                                onPressed: () async {
                                  return await _dialog.showOptionDialog(
                                      context: context,
                                      msg: 'للاضافة للسلة  يرجي التسجيل اولا',
                                      okFun: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (c) =>
                                                    SignInScreen()));
                                      },
                                      okMsg: 'نعم',
                                      cancelMsg: 'لا',
                                      cancelFun: () {
                                        return;
                                      });
                                },
                                child: Text(
                                  'اضافة للسلة',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                ),
                              ),
                            ),
                            if (Provider.of<SharedPref>(context, listen: false)
                                    .token !=
                                null)
                              FutureBuilder(
                                  future: Provider.of<GetCartProvider>(context,
                                          listen: false)
                                      .getCart(context),
                                  builder: (context, snapShot) {
                                    return Consumer<GetCartProvider>(
                                        builder: (context, snap, _) {
                                      return RaisedButton(
                                        textColor: Colors.white,
                                        color: Theme.of(context).primaryColor,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(12.0),
                                        ),
                                        onPressed: () async {
                                          if (total > 0) {
                                            if (snap.carts.isEmpty ||
                                                snap.carts.length == 0) {
                                              return await _dialog
                                                  .showOptionDialog(
                                                      context: context,
                                                      msg:
                                                          'هل انت متاكد من اضافه هذا المنتج للسلة',
                                                      okFun: () {
                                                        Provider.of<AddToCartProvider>(
                                                                context,
                                                                listen: false)
                                                            .addToCart(
                                                                context,
                                                                widget.id,
                                                                total)
                                                            .whenComplete(() =>
                                                                setState(
                                                                    () {}));
                                                      },
                                                      okMsg: 'نعم',
                                                      cancelMsg: 'لا',
                                                      cancelFun: () {
                                                        return;
                                                      });
                                            } else if (snap.carts.isNotEmpty &&
                                                snap.carts[0].shopId ==
                                                    _model.data.shopId) {
                                              return await _dialog
                                                  .showOptionDialog(
                                                      context: context,
                                                      msg:
                                                          'هل انت متاكد من اضافه هذا المنتج للسلة',
                                                      okFun: () {
                                                        Provider.of<AddToCartProvider>(
                                                                context,
                                                                listen: false)
                                                            .addToCart(
                                                                context,
                                                                widget.id,
                                                                total)
                                                            .whenComplete(() =>
                                                                setState(
                                                                    () {}));
                                                      },
                                                      okMsg: 'نعم',
                                                      cancelMsg: 'لا',
                                                      cancelFun: () {
                                                        return;
                                                      });
                                            } else if (snap.carts[0].shopId !=
                                                _model.data.shopId) {
                                              return await _dialog
                                                  .showOptionDialog(
                                                      context: context,
                                                      msg:
                                                          'لا يمكنك طلب منتج من اكثر من متجر ، لذا هل ترغب بتفريغ السلة',
                                                      okFun: () {
                                                        Provider.of<
                                                                GetCartProvider>(
                                                          context,
                                                          listen: false,
                                                        )
                                                            .emptyCart(context)
                                                            .whenComplete(() =>
                                                                setState(
                                                                    () {}));
                                                      },
                                                      okMsg: 'نعم',
                                                      cancelMsg: 'لا',
                                                      cancelFun: () {
                                                        return;
                                                      });
                                            }
                                          }
                                        },
                                        child: Text(
                                          'اضافة للسلة',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                        ),
                                      );
                                    });
                                  }),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.02,
                            ),
                            Card(
                              elevation: 0.5,
                              child: Container(
                                height: 30,
                                width: 30,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10)),
                                child: Center(
                                  child: IconButton(
                                    iconSize: 12,
                                    icon: FaIcon(
                                      FontAwesomeIcons.minus,
                                      color: Colors.black,
                                    ),
                                    onPressed: () {
                                      if (total <= 1) {
                                        return;
                                      } else {
                                        setState(() {
                                          total -= 1;
                                        });
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              elevation: 0.5,
                              child: Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10)),
                                  width: 60,
                                  child: Center(
                                    child: Text(
                                      '$total',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Card(
                              elevation: 0.5,
                              child: Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10)),
                                child: IconButton(
                                  iconSize: 12,
                                  icon: FaIcon(
                                    FontAwesomeIcons.plus,
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      if (total >= 99) {
                                        return;
                                      } else {
                                        setState(() {
                                          total += 1;
                                        });
                                      }
                                    });
                                  },
                                ),
                              ),
                            ),
                          ]),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
