import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/get/getOrderByIdProvider.dart';
import 'package:todaysguide/src/provider/get/setting.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ActiveOrderDetailsIteams.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:intl/intl.dart' as intl;
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';

import 'widgets/payMentBtnSheet.dart';

class ActiveOrderDetails extends StatefulWidget {
  final int id;
  final int type;
  ActiveOrderDetails({this.id, this.type});
  @override
  _ActiveOrderDetailsState createState() => _ActiveOrderDetailsState();
}

class _ActiveOrderDetailsState extends State<ActiveOrderDetails> {
  GetOrderByIdProvider getOrderByIdProvider;
  bool loading = true;

  SettingProvider settingProvider;
  _sendWhatsApp() async {
    var url = "https://wa.me/${getOrderByIdProvider.getOrderByIdModel.data.driverPhone}";
    await canLaunch(url) ? launch(url) : print('No WhatsAPP');
  }

  @override
  void didChangeDependencies() async {
    if (loading) {
      settingProvider = Provider.of<SettingProvider>(context, listen: false);
      await Provider.of<GetOrderByIdProvider>(context, listen: false)
          .getOrderById(context, widget.id,
              Provider.of<SharedPref>(context, listen: false).token);
      getOrderByIdProvider =
          Provider.of<GetOrderByIdProvider>(context, listen: false);
      print(
          'getOrderByIdProvider.orderById.length=${getOrderByIdProvider.getOrderByIdModel.data.productsCart.length}');
      setState(() {
        loading = false;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: widget.type != 2 ? Text('الطلب النشط') : Text('الطلب المكتمل'),
        centerTitle: true,
      ),
      body: loading
          ? AppLoader()
          : ListView(
              primary: false,
              shrinkWrap: true,
              children: [
                SizedBox(
                  height: 5,
                ),
                Center(
                  child: Container(
                    width: mediaQuery.width * 0.9,
                    child:  ListView.builder(
                          primary: false,
                          shrinkWrap: true,
                          //  scrollDirection: Axis.horizontal,
                          itemCount: getOrderByIdProvider
                              .getOrderByIdModel.data.productsCart.length,
                          itemBuilder: (context, i) => ActiveOrderDetailsIteams(
                            id: getOrderByIdProvider
                                .getOrderByIdModel.data.productsCart[i].id,
                            name: getOrderByIdProvider.getOrderByIdModel.data
                                .productsCart[i].productName,
                            photo: getOrderByIdProvider.getOrderByIdModel.data
                                .productsCart[i].photos[0].photo,
                            price:
                                '${intl.NumberFormat.currency(decimalDigits: 0).parse(getOrderByIdProvider.getOrderByIdModel.data.productsCart[i].price)}',
                            quantity: getOrderByIdProvider.getOrderByIdModel
                                .data.productsCart[i].quantity,
                            shopName: getOrderByIdProvider
                                .getOrderByIdModel.data.shop,
                          ),
                        ),
                      
                    
                  ),
                ),
                Center(
                  child: Container(
                    width: mediaQuery.width * 0.9,
                             alignment: Alignment.topRight,
                    
                       
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(45)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            alignment: Alignment.topRight,
                            width: mediaQuery.width * 0.65,
                            // color: Colors.black,
                            child: ListTile(
                              leading: Container(
                                // color: Colors.red,
                                width: mediaQuery.width * 0.3,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(30),
                                      child: InkWell(
                                        onTap: _sendWhatsApp,
                                        child: Container(
                                          height: mediaQuery.height * 0.045,
                                          child: FaIcon(
                                            FontAwesomeIcons.whatsapp,
                                            color: Colors.green,
                                            size: 30,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: mediaQuery.width* 0.03,
                                    ),
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(30),
                                      child: InkWell(
                                        onTap: () {
                                          UrlLauncher.launch(
                                              'tel:+${getOrderByIdProvider.getOrderByIdModel.data.driverPhone}');
                                        },
                                        child: Container(
                                          height: mediaQuery.height * 0.038,
                                          child: Image.asset(
                                            'assets/images/12.png',
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              trailing: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    getOrderByIdProvider
                                        .getOrderByIdModel.data.driver,
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    getOrderByIdProvider
                                                .getOrderByIdModel.data.carType ==
                                            '1'
                                        ? 'سيارة مبردة'
                                        : "سيارة عادية",
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          CircleAvatar(
                            radius: 35,
                            backgroundColor: Theme.of(context).primaryColor,
                            child: getOrderByIdProvider
                                        .getOrderByIdModel.data.driverPhoto !=
                                    null
                                ? CachedNetworkImage(
                                    imageUrl: getOrderByIdProvider
                                        .getOrderByIdModel.data.driverPhoto,
                                    fadeInDuration: Duration(seconds: 2),
                                    placeholder: (context, url) => CircleAvatar(
                                        radius: 37,
                                        backgroundImage: AssetImage(
                                            'assets/images/newLogo.png')),
                                    imageBuilder: (context, provider) {
                                      return CircleAvatar(
                                          radius: 37, backgroundImage: provider);
                                    },
                                  )
                                : CircleAvatar(
                                    radius: 37,
                                    backgroundImage:
                                        AssetImage('assets/images/newLogo.png'),
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Container(
                       alignment: Alignment.topRight,
                  //  height: 70,
                                          width: mediaQuery.width * 0.9,
                    child: Card(
                      elevation: 0.5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(45)),
                      child:  ListTile(
                            leading: Container(
                              alignment: Alignment.centerRight,
                              width: mediaQuery.width * 0.3,
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(' : اجمالي'),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'SR',
                                          style: TextStyle(color: Colors.grey),
                                        ),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          '${getOrderByIdProvider.getOrderByIdModel.data.totalPrice}',
                                          style: TextStyle(
                                              color:
                                                  Theme.of(context).primaryColor,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                            trailing: Container(
                              height: mediaQuery.height * 0.1,
                              width: mediaQuery.width * 0.35,
                              // alignment: Alignment.centerLeft,
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          ' SR ${getOrderByIdProvider.getOrderByIdModel.data.orderPrice}',
                                          style: TextStyle(
                                            fontSize: 12,
                                          ),
                                        ),
                                        Text(
                                          ' : الطلب ',
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          ' SR ${getOrderByIdProvider.getOrderByIdModel.data.price}',
                                          style: TextStyle(
                                            fontSize: 12,
                                          ),
                                        ),
                                        Text(
                                          ' : التوصيل ',
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]),
                            )),
                      
                    ),
                  ),
                ),
                SizedBox(
                  height: mediaQuery.height * 0.02,
                ),
                if (widget.type != 2)
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SignInButton(
                      btnWidth: MediaQuery.of(context).size.width * 0.9,
                      btnHeight: 45,
                      btnColor: Theme.of(context).primaryColor,
                      txtColor: Colors.white,
                      onPressSignIn: () {
                        PaymentBtnSheet().show(context: context, id: widget.id);
                      },
                      buttonText: 'الدفع اونلاين لانهاء الطلب',
                    ),
                  ),
              ],
            ),
    );
  }
}
