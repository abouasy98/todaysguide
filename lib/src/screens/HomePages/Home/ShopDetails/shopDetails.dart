import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Models/post/fetchDataModel.dart';
import 'package:todaysguide/src/provider/get/getMyFavShopsProvider.dart';
import 'package:todaysguide/src/provider/get/productCartCountProvider.dart';
import 'package:todaysguide/src/provider/post/addAdToFavProvider.dart';
import 'package:todaysguide/src/provider/post/fetchbigDataProvider.dart';
import 'package:todaysguide/src/provider/post/productsFilterProvider.dart';
import 'package:todaysguide/src/provider/post/reportShopProvider.dart';
import 'package:todaysguide/src/provider/post/shopViewProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/shopItem.dart';
import 'package:todaysguide/src/screens/HomePages/Home/cartDetails/cartDetails.dart';
import 'package:todaysguide/src/screens/MainWidgets/customScrollPhysics.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/MainWidgets/customBtn.dart';
import 'package:todaysguide/src/screens/MainWidgets/customSliderImage.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_bottom_sheet.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';
import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';
import '../../../../Models/post/shopsModel.dart';
import 'categoriesFilter.dart';

class ShopDetails extends StatefulWidget {
  final Key key;
  final Shops shops;
  ShopDetails({this.key, this.shops});
  @override
  _ShopDetailsState createState() => _ShopDetailsState();
}

class _ShopDetailsState extends State<ShopDetails>
    with TickerProviderStateMixin {
  ScrollController _scrollController = new ScrollController();
  int _page = 1;
  bool isLoading = false;
  Timer _debounce;
  FetchDataProvider productList;
  final _searchQuery = TextEditingController();
  int page = 1;
  double _scrollPosition;
  _scrollListener() async {
    if (_scrollController.hasClients) {
      _scrollPosition = _scrollController.position.pixels;
    }
    if (_scrollController.hasClients &&
        _scrollPosition == _scrollController.position.maxScrollExtent) {
      productList.setLoadingState(LoadMoreStatus.LOADING);
      productList.pageNumber += 1;
      productList.fetchProducts(context: context, shopId: widget.shops.id);
    }
  }

  @override
  void initState() {
    productList = Provider.of<FetchDataProvider>(context, listen: false);
    productList.resetStreams();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      productList.setLoadingState(LoadMoreStatus.INITIAL);
    });
    productList.fetchProducts(
      context: context,
      shopId: widget.shops.id,
    );
    _scrollController.addListener(_scrollListener);

    super.initState();
  }

  _onSearchChange() {
    var productList = Provider.of<FetchDataProvider>(context, listen: false);

    if (_debounce?.isActive ?? false) _debounce.cancel();

    if (_searchQuery.text.isEmpty) {
      productList.resetStreams();
      productList.setLoadingState(LoadMoreStatus.INITIAL);
      productList.fetchProducts(context: context, shopId: widget.shops.id);
    } else {
      productList.resetStreams();
      productList.setLoadingState(LoadMoreStatus.INITIAL);
      _debounce = Timer(const Duration(milliseconds: 1000), () {
        productList.fetchProducts(
          shopId: widget.shops.id,
          context: context,
          strSearch: _searchQuery.text.trim(),
        );
      });
    }
  }

  CustomDialog _dialog = CustomDialog();
  Widget posintionedonImage() {
    final mediaQuery = MediaQuery.of(context).size;
    return Container(
      width: mediaQuery.width * 0.4,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CircleAvatar(
            radius: 15,
            backgroundColor: Colors.red[400],
            child: IconButton(
              iconSize: 15,
              onPressed: () async {
                if (Provider.of<SharedPref>(context, listen: false).token ==
                    null) {
                  return await _dialog.showOptionDialog(
                      context: context,
                      msg: 'للابلاغ يرجي التسجيل اولا',
                      okFun: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (c) => SignInScreen()));
                      },
                      okMsg: 'نعم',
                      cancelMsg: 'لا',
                      cancelFun: () {
                        return;
                      });
                } else {
                  CustomBottomSheet().show(
                      context: context,
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            RegisterTextField(
                              onChange: (v) {
                                Provider.of<ReportShopProvider>(context,
                                        listen: false)
                                    .details = v;
                              },
                              label: 'سبب الابلاغ',
                              icon: Icons.label,
                              type: TextInputType.text,
                            ),
                            SizedBox(height: 20),
                            CustomBtn(
                              text: 'تاكيد الابلاغ',
                              color: Colors.red,
                              onTap: () async {
                                Navigator.pop(context);
                                await Provider.of<ReportShopProvider>(context,
                                        listen: false)
                                    .reportShop(
                                        widget.shops.id,
                                        Provider.of<SharedPref>(context,
                                                listen: false)
                                            .token,
                                        context);
                              },
                              txtColor: Colors.white,
                            )
                          ],
                        ),
                      ));
                }
              },
              icon: Icon(
                Icons.priority_high,
                color: Colors.white,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 5.0, left: 5),
            child: CircleAvatar(
              radius: 15,
              backgroundColor: Colors.white60,
              child: IconButton(
                iconSize: 15,
                onPressed: () async {
                  _saveForm();
                },
                icon: Icon(
                  isFavourite ? Icons.favorite : Icons.favorite_border,
                  color:
                      isFavourite ? Colors.red : Color.fromRGBO(72, 83, 103, 1),
                ),
              ),
            ),
          ),
          CircleAvatar(
            radius: 15,
            backgroundColor: Colors.white60,
            child: IconButton(
              iconSize: 15,
              onPressed: () async {
                String _msg;
                StringBuffer _sb = new StringBuffer();
                setState(() {
                  _sb.write("اسم المتجر : ${widget.shops.name} \n");

                  _sb.write(shareLink);

                  _msg = _sb.toString();
                });
                if (widget.shops.photos != null) {
                  if (widget.shops.photos.length != 0) {
                    var request = await HttpClient()
                        .getUrl(Uri.parse(widget.shops.photos[0].photo));
                    var response = await request.close();
                    Uint8List bytes =
                        await consolidateHttpClientResponseBytes(response);
                    await Share.file(
                        'ESYS AMLOG', 'amlog.jpg', bytes, 'image/jpg',
                        text: _msg);
                  } else {
                    Share.text("title", _msg, 'text/plain');
                  }
                } else {
                  Share.text("title", _msg, 'text/plain');
                }
              },
              icon: Icon(
                Icons.share,
                color: Color.fromRGBO(72, 83, 103, 1),
              ),
            ),
          ),
        ],
      ),
    );
  }

  ProductCartCountProvider productCartCountProvider;
  String shareLink;

  ProductsFilterProvider prdouctFilter;

  Future<void> _createDynamicLink() async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://todaysguide.page.link',
      link: Uri.parse('https://todaysguide.page.link/0/${widget.shops.id}'),
      androidParameters: AndroidParameters(
        packageName: 'com.tqnee.todaysguide',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.tqnee.todaysguide',
        minimumVersion: "4",
      ),
    );

    Uri url;
    final ShortDynamicLink shortLink = await parameters.buildShortLink();
    url = shortLink.shortUrl;
    setState(() {
      shareLink = url.toString();
    });
  }

  _saveForm() async {
    if (Provider.of<SharedPref>(context, listen: false).token == null) {
      return await _dialog.showOptionDialog(
          context: context,
          msg: 'للاعجاب يرجي التسجيل اولا',
          okFun: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (c) => SignInScreen()));
          },
          okMsg: 'نعم',
          cancelMsg: 'لا',
          cancelFun: () {
            return;
          });
    } else {
      if (!isFavourite) {
        await Provider.of<AddAdToFavProvider>(context, listen: false)
            .addAdToFav(Provider.of<SharedPref>(context, listen: false).token,
                widget.shops.id, context);
        setState(() {
          isFavourite = true;
        });
      } else {
        await Provider.of<GetMyFavShopProvider>(context, listen: false)
            .deleteFavShop(context, widget.shops.id,
                Provider.of<SharedPref>(context, listen: false).token);
        setState(() {
          isFavourite = false;
        });
      }
    }
  }

  GetMyFavShopProvider getMyFavShopProvider;
  bool isInit = true;
  bool isFavourite = false;
  List<String> photos = [];
  @override
  void didChangeDependencies() async {
    if (isInit) {
      _searchQuery.addListener(_onSearchChange);
      _createDynamicLink();
      if (widget.shops.photos != null) {
        for (int i = 0; i < widget.shops.photos.length; i++) {
          photos.add(widget.shops.photos[i].photo);
        }
      }
      await Provider.of<ProductsFilterProvider>(context, listen: false)
          .getProductsFillter(shopId: widget.shops.id, categoryId: 0);
      prdouctFilter =
          Provider.of<ProductsFilterProvider>(context, listen: false);

      if (Provider.of<SharedPref>(context, listen: false).token != null) {
        await Provider.of<ShopViewProvider>(context, listen: false).shopView(
            Provider.of<SharedPref>(context, listen: false).token,
            context,
            widget.shops.id);
        await Provider.of<GetMyFavShopProvider>(context, listen: false)
            .getMyFavShop(
                Provider.of<SharedPref>(context, listen: false).token, context);
        getMyFavShopProvider =
            Provider.of<GetMyFavShopProvider>(context, listen: false);
        if (getMyFavShopProvider.favourite
                .indexWhere((e) => e.id == widget.shops.id) >=
            0) {
          setState(() {
            isFavourite = true;
          });
        } else {
          setState(() {
            isFavourite = false;
          });
        }
      }

      if (Provider.of<SharedPref>(context, listen: false).token != null) {
        await Provider.of<ProductCartCountProvider>(context, listen: false)
            .productCartCount(
                Provider.of<SharedPref>(context, listen: false).token, context);
        productCartCountProvider =
            Provider.of<ProductCartCountProvider>(context, listen: false);
      }

      isInit = false;
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _searchQuery.dispose();

    productList.resetStreams();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: AppBar(
          title: Text(
            widget.shops.name,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          centerTitle: true,
          leading: InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(Icons.arrow_back_ios),
          ),
          actions: [
            if (Provider.of<SharedPref>(context, listen: false).token != null)
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    IconButton(
                      onPressed: () async {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => CartDetails()));
                      },
                      icon: Icon(Icons.shopping_cart),
                    ),
                    if (productCartCountProvider != null &&
                        productCartCountProvider
                                .reportShopModel.data.productCartCount !=
                            0)
                      Positioned(
                        top: -1,
                        left: mediaQuery.width * 0.01,
                        child: CircleAvatar(
                          backgroundColor: Colors.red[400],
                          radius: 10,
                          child: Text(
                            '${productCartCountProvider.reportShopModel.data.productCartCount}',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
          ],
        ),
      ),
      body: Center(
        child: Container(
          alignment: Alignment.center,
          width: mediaQuery.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                flex: 4,
                child: Stack(
                  alignment: Alignment.center,
                  clipBehavior: Clip.none,
                  children: [
                    if (widget.shops.photos != null)
                      Stack(
                        // overflow: Overflow.visible,
                         children: [
                        CustomSliderImage(
                          photos: widget.shops.photos,
                        ),
                        Positioned(
                          child: posintionedonImage(),
                          left: 10,
                          top: 10,
                        )
                      ]),
                    if (widget.shops.photos == null)
                      Stack(
                        // overflow: Overflow.visible,
                         children: [
                        Container(
                          height: mediaQuery.height * 0.3,
                          width: mediaQuery.width,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/icon-001.png'))),
                        ),
                        Positioned(
                          child: posintionedonImage(),
                          left: 10,
                          top: 10,
                        )
                      ]),
                    Directionality(
                      textDirection: TextDirection.rtl,
                      child: Positioned(
                        bottom: mediaQuery.height * 0.04,
                        width: mediaQuery.width * 0.7,
                        child: Container(
                            height: mediaQuery.height * 0.05,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(201, 202, 207, 0.8),
                                borderRadius: BorderRadius.circular(25)),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                right: 8.0,
                              ),
                              child: Container(
                                alignment: Alignment.center,
                                width: mediaQuery.width * 0.7,
                                child: TextFormField(
                                  keyboardType: TextInputType.name,
                                  textCapitalization:
                                      TextCapitalization.sentences,
                                  textInputAction: TextInputAction.search,
                                  controller: _searchQuery.text == null
                                      ? null
                                      : _searchQuery,
                                  onChanged: (val) {},
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(top: 8.0),
                                    isDense: true,
                                    hintText: ' البحث في المتجر',
                                    suffixIcon: Container(
                                        decoration: BoxDecoration(
                                            color: Color.fromRGBO(
                                                234, 237, 244, 0.8),
                                            borderRadius: BorderRadius.only(
                                                bottomLeft: Radius.circular(25),
                                                topLeft: Radius.circular(25))),
                                        child: Consumer<FetchDataProvider>(
                                            builder: (context, snap, _) {
                                          if (_searchQuery.text.isNotEmpty) {
                                            return Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 8.0),
                                              child: IconButton(
                                                iconSize: 20,
                                                icon: Icon(
                                                  Icons.close,
                                                  color: Colors.green,
                                                ),
                                                onPressed: () {
                                                  WidgetsBinding.instance
                                                      .addPostFrameCallback(
                                                          (_) => _searchQuery
                                                              .clear());
                                                  // productList.resetStreams();
                                                  // productList.setLoadingState(
                                                  //     LoadMoreStatus.INITIAL);
                                                  // productList.fetchProducts(
                                                  //     context: context,
                                                  //     shopId: widget.shops.id);
                                                },
                                              ),
                                            );
                                          } else {
                                            return Icon(
                                              Icons.search,
                                              size: 20,
                                              color: Colors.green,
                                            );
                                          }
                                        })),
                                    border: InputBorder.none,
                                  ),
                                ),
                              ),
                            )),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 15,
              ),
              CategoriesFilter(
                page: _page,
                shopId: widget.shops.id,
              ),
              SizedBox(
                height: 20,
              ),
              Expanded(
                flex: 7,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: mediaQuery.width * 0.9,
                    child: ListView(
                      primary: false,
                      controller: _scrollController,
                      physics: const CustomScrollPhysics(),
                      shrinkWrap: true,
                      children: [
                        if (widget.shops.details != null)
                          Text(
                            ': وصف المتجر',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.bold),
                          ),
                        SizedBox(
                          height: 5,
                        ),
                        if (widget.shops.details != null)
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              widget.shops.details,
                              textAlign: TextAlign.right,
                              style:
                                  TextStyle(fontSize: 12, color: Colors.grey),
                            ),
                          ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          ': المنتجات',
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Directionality(
                            textDirection: TextDirection.rtl,
                            child: Consumer<FetchDataProvider>(
                                builder: (context, snap, _) {
                              if (snap.getLoadMoreStatus() ==
                                  LoadMoreStatus.INITIAL) {
                                return AppLoader();
                              } else if (snap.allProducts != null &&
                                  snap.allProducts.length > 0 &&
                                  snap.getLoadMoreStatus() !=
                                      LoadMoreStatus.INITIAL) {
                                return _buildList(
                                    snap.allProducts,
                                    snap.getLoadMoreStatus() ==
                                        LoadMoreStatus.LOADING);
                              } else if (snap.allProducts.length < 1) {
                                return Center(child: Text('لا يوجد منتحات'));
                              } else {
                                return Center(
                                  child: CircularProgressIndicator(),
                                );
                              }
                            }))
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildList(List<DataListView> items, bool isLoadMore) {
    return Column(
      children: [
        ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: items.length,
            physics: CustomScrollPhysics(),
            scrollDirection: Axis.vertical,
            itemBuilder: (context, i) {
              return ShopItem(
                name: items[i].name,
                orderWaiting: false,
                productsFilter: items[i],
                photo: items[i].photos[0].photo,
                id: items[i].id,
                price: items[i].price,
                available: items[i].available,
              );
            }),
        Visibility(
          child: Container(
            padding: EdgeInsets.all(5),
            height: 35.0,
            width: 35.0,
            child: Center(child: CircularProgressIndicator()),
          ),
          visible: isLoadMore,
        ),
      ],
    );
  }
}
