import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:todaysguide/src/provider/checkProvider.dart';
import 'package:todaysguide/src/provider/get/categoriesProvider.dart';
import 'package:todaysguide/src/provider/post/productsFilterProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/filtterCard.dart';

class CategoriesFilterById extends StatefulWidget {
  @override
  _CategoriesFilterByIdState createState() => _CategoriesFilterByIdState();
}

class _CategoriesFilterByIdState extends State<CategoriesFilterById> {
  int slected = 0;
  @override
  void initState() {
    Future.delayed(Duration(microseconds: 150), () {
      Provider.of<CategoriesProvider>(context, listen: false)
          .getcategories(context);
    });
    shopsProvider = Provider.of<ProductsFilterProvider>(context, listen: false);
    checkProvider = Provider.of<CheckProvider>(context, listen: false);
    categoriesProvider =
        Provider.of<CategoriesProvider>(context, listen: false);
    all = true;
    departMentId = 0;
    super.initState();
  }

  CheckProvider checkProvider;
  CategoriesProvider categoriesProvider;
  ProductsFilterProvider shopsProvider;
  bool all;
  int departMentId;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, right: 3),
      child: Container(
        height: 50,
        child: AnimationLimiter(
          child: ListView(
            reverse: true,
            scrollDirection: Axis.horizontal,
            children: [
              Filtter(
                onTap: () {
                  setState(() {
                    all = true;
                    departMentId = 0;
                  });
                  checkProvider.assignValueProduct(
                      products: shopsProvider.productsFilter);
                  for (int i = 0;
                      i < categoriesProvider.categories.length;
                      i++) {
                    categoriesProvider.categories[i].selected = false;
                  }
                },
                backgroundColor: all
                    ? Theme.of(context).primaryColor
                    : Theme.of(context).scaffoldBackgroundColor,
                title: 'الكل',
                elevation: all ? 3 : 0,
                titleColor: all ? Colors.white : Colors.black87,
              ),
              ListView.builder(
                  shrinkWrap: true,
                  reverse: true,
                  itemCount:
                      Provider.of<CategoriesProvider>(context, listen: true)
                          .categories
                          .length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (c, index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      delay: Duration(milliseconds: 400),
                      child: SlideAnimation(
                        duration: Duration(milliseconds: 400),
                        horizontalOffset: 50,
                        child: FadeInAnimation(
                          child: Filtter(
                            title: categoriesProvider.categories[index].name,
                            onTap: () {
                              setState(() {
                                all = false;
                                categoriesProvider.categories[index].selected =
                                    true;
                                for (int i = 0;
                                    i < categoriesProvider.categories.length;
                                    i++) {
                                  setState(() {
                                    if (i == index) {
                                      categoriesProvider
                                          .categories[i].selected = true;
                                      departMentId =
                                          categoriesProvider.categories[i].id;
                                    } else {
                                      Provider.of<CategoriesProvider>(context,
                                              listen: false)
                                          .categories[i]
                                          .selected = false;
                                    }
                                    checkProvider.assignValueProduct(
                                        products: shopsProvider.productsFilter
                                            .where((e) =>
                                                e.categoryId == departMentId)
                                            .toList());
                                  });
                                }
                              });
                            },
                            backgroundColor: Provider.of<CategoriesProvider>(
                                        context,
                                        listen: true)
                                    .categories[index]
                                    .selected
                                ? Theme.of(context).primaryColor
                                : null,
                            elevation: Provider.of<CategoriesProvider>(context,
                                        listen: true)
                                    .categories[index]
                                    .selected
                                ? 3
                                : null,
                            titleColor: Provider.of<CategoriesProvider>(context,
                                        listen: true)
                                    .categories[index]
                                    .selected
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                      ),
                    );
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
