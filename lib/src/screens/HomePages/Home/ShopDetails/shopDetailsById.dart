import 'dart:io';
import 'dart:typed_data';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Models/get/ShopByIdModel.dart';
import 'package:todaysguide/src/provider/checkProvider.dart';
import 'package:todaysguide/src/provider/get/getMyFavShopsProvider.dart';
import 'package:todaysguide/src/provider/get/getShopByIdProvider.dart';
import 'package:todaysguide/src/provider/get/productCartCountProvider.dart';
import 'package:todaysguide/src/provider/post/addAdToFavProvider.dart';
import 'package:todaysguide/src/provider/post/productsFilterProvider.dart';
import 'package:todaysguide/src/provider/post/reportShopProvider.dart';
import 'package:todaysguide/src/provider/post/shopViewProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/HomeWidget/searchProductsItem.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/shopItem.dart';
import 'package:todaysguide/src/screens/HomePages/Home/cartDetails/cartDetails.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/MainWidgets/customBtn.dart';
import 'package:todaysguide/src/screens/MainWidgets/customSliderImage.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_bottom_sheet.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';
import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';

import 'categoriesFilterById.dart';

class ShopDetailsById extends StatefulWidget {
  final Key key;
  final int id;
  ShopDetailsById({this.key, this.id});

  @override
  _ShopDetailsByIdState createState() => _ShopDetailsByIdState();
}

class _ShopDetailsByIdState extends State<ShopDetailsById> {
  CustomDialog _dialog = CustomDialog();
  Widget posintionedonImage() {
    final mediaQuery = MediaQuery.of(context).size;
    return Container(
      width: mediaQuery.width * 0.4,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CircleAvatar(
            radius: 15,
            backgroundColor: Colors.red[400],
            child: IconButton(
              iconSize: 15,
              onPressed: () async {
                if (Provider.of<SharedPref>(context, listen: false).token ==
                    null) {
                  return await _dialog.showOptionDialog(
                      context: context,
                      msg: 'للابلاغ يرجي التسجيل اولا',
                      okFun: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (c) => SignInScreen()));
                      },
                      okMsg: 'نعم',
                      cancelMsg: 'لا',
                      cancelFun: () {
                        return;
                      });
                } else {
                  CustomBottomSheet().show(
                      context: context,
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            RegisterTextField(
                              onChange: (v) {
                                Provider.of<ReportShopProvider>(context,
                                        listen: false)
                                    .details = v;
                              },
                              label: 'سبب الابلاغ',
                              icon: Icons.label,
                              type: TextInputType.text,
                            ),
                            SizedBox(height: 20),
                            CustomBtn(
                              text: 'تاكيد الابلاغ',
                              color: Colors.red,
                              onTap: () async {
                                Navigator.pop(context);
                                await Provider.of<ReportShopProvider>(context,
                                        listen: false)
                                    .reportShop(
                                        widget.id,
                                        Provider.of<SharedPref>(context,
                                                listen: false)
                                            .token,
                                        context);
                              },
                              txtColor: Colors.white,
                            )
                          ],
                        ),
                      ));
                }
              },
              icon: Icon(
                Icons.priority_high,
                color: Colors.white,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 5.0, left: 5),
            child: CircleAvatar(
              radius: 15,
              backgroundColor: Colors.white60,
              child: IconButton(
                iconSize: 15,
                onPressed: () async {
                  await _saveForm();
                },
                icon: Icon(
                  isFavourite ? Icons.favorite : Icons.favorite_border,
                  color:
                      isFavourite ? Colors.red : Color.fromRGBO(72, 83, 103, 1),
                ),
              ),
            ),
          ),
          CircleAvatar(
            radius: 15,
            backgroundColor: Colors.white60,
            child: IconButton(
              iconSize: 15,
              onPressed: () async {
                String _msg;
                StringBuffer _sb = new StringBuffer();
                setState(() {
                  _sb.write("اسم المتجر : ${shopById.restourant.data.name} \n");

                  _sb.write(shareLink);

                  _msg = _sb.toString();
                });
                if (shopById.restourant.data.photos != null) {
                  if (shopById.restourant.data.photos.length != 0) {
                    var request = await HttpClient().getUrl(
                        Uri.parse(shopById.restourant.data.photos[0].photo));
                    var response = await request.close();
                    Uint8List bytes =
                        await consolidateHttpClientResponseBytes(response);
                    await Share.file(
                        'ESYS AMLOG', 'amlog.jpg', bytes, 'image/jpg',
                        text: _msg);
                  } else {
                    Share.text("title", _msg, 'text/plain');
                  }
                } else {
                  Share.text("title", _msg, 'text/plain');
                }
              },
              icon: Icon(
                Icons.share,
                color: Color.fromRGBO(72, 83, 103, 1),
              ),
            ),
          ),
        ],
      ),
    );
  }

  ProductCartCountProvider productCartCountProvider;
  String shareLink;
  ShopsByIdProvider shopById;
  String category = 'الكل';
  int categoryId = 0;
  String query = "";
  TextEditingController _searchController = TextEditingController();
  ProductsFilterProvider prdouctFilter;

  Future<void> _createDynamicLink() async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://todaysguide.page.link',
      link: Uri.parse('https://todaysguide.page.link/0/${widget.id}'),
      androidParameters: AndroidParameters(
        packageName: 'com.tqnee.todaysguide',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.tqnee.todaysguide',
        minimumVersion: "4",
      ),
    );

    Uri url;
    final ShortDynamicLink shortLink = await parameters.buildShortLink();
    url = shortLink.shortUrl;
    setState(() {
      shareLink = url.toString();
    });
  }

  _saveForm() async {
    if (Provider.of<SharedPref>(context, listen: false).token == null) {
      return await _dialog.showOptionDialog(
          context: context,
          msg: 'للاعجاب يرجي التسجيل اولا',
          okFun: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (c) => SignInScreen()));
          },
          okMsg: 'نعم',
          cancelMsg: 'لا',
          cancelFun: () {
            return;
          });
    } else {
      if (!isFavourite) {
        await Provider.of<AddAdToFavProvider>(context, listen: false)
            .addAdToFav(Provider.of<SharedPref>(context, listen: false).token,
                widget.id, context);
        setState(() {
          isFavourite = true;
        });
      } else {
        await Provider.of<GetMyFavShopProvider>(context, listen: false)
            .deleteFavShop(context, widget.id,
                Provider.of<SharedPref>(context, listen: false).token);
        setState(() {
          isFavourite = false;
        });
      }
    }
  }

  CheckProvider checkProvider;
  List<Products> products;
  bool isInit = true;
  bool isFavourite = true;
  ShopByIdModel _model;
  List<String> photos = [];
  @override
  void didChangeDependencies() async {
    if (isInit) {
      _createDynamicLink();

      await Provider.of<ShopsByIdProvider>(context, listen: false)
          .getShops(widget.id,
              Provider.of<SharedPref>(context, listen: false).token, context)
          .then((res) {
        setState(() {
          _model = res;
        });
        if (_model.data.photos != null) {
          for (int i = 0; i < _model.data.photos.length; i++) {
            photos.add(_model.data.photos[i].photo);
          }
        }
      });
      if (Provider.of<SharedPref>(context, listen: false).token != null) {
        await Provider.of<ShopViewProvider>(context, listen: false).shopView(
            Provider.of<SharedPref>(context, listen: false).token,
            context,
            widget.id);
      }
      await Provider.of<ProductsFilterProvider>(context, listen: false)
          .getProductsFillter(shopId: widget.id, categoryId: 0);

      shopById = Provider.of<ShopsByIdProvider>(context, listen: false);
      prdouctFilter =
          Provider.of<ProductsFilterProvider>(context, listen: false);
      if (Provider.of<SharedPref>(context, listen: false).token != null) {
        await Provider.of<ProductCartCountProvider>(context, listen: false)
            .productCartCount(
                Provider.of<SharedPref>(context, listen: false).token, context);
        productCartCountProvider =
            Provider.of<ProductCartCountProvider>(context, listen: false);
      }
      products = prdouctFilter.productsFilter;
      checkProvider = Provider.of<CheckProvider>(context, listen: false);
      checkProvider.assignValueProduct(products: prdouctFilter.productsFilter);
      if (shopById.restourant.data.favorite == 0) {
        setState(() {
          isFavourite = false;
        });
      } else {
        setState(() {
          isFavourite = true;
        });
      }
      setState(() {
        isInit = false;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: isInit
            ? SpinKitThreeBounce(
                color: Colors.white,
                size: 22,
              )
            : AppBar(
                title: Text(
                  shopById.restourant.data.name,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                ),
                centerTitle: true,
                leading: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(Icons.arrow_back_ios),
                ),
                actions: [
                  if (Provider.of<SharedPref>(context, listen: false).token !=
                      null)
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          IconButton(
                            onPressed: () async {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => CartDetails()));
                            },
                            icon: Icon(Icons.shopping_cart),
                          ),
                          Visibility(
                            visible: productCartCountProvider
                                    .reportShopModel.data.productCartCount !=
                                0,
                            child: Positioned(
                              top: -1,
                              left: mediaQuery.width * 0.01,
                              child: CircleAvatar(
                                backgroundColor: Colors.red[400],
                                radius: 10,
                                child: Text(
                                  '${productCartCountProvider.reportShopModel.data.productCartCount}',
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                ],
              ),
      ),
      body: isInit
          ? Container(height: mediaQuery.height * 0.7, child: AppLoader())
          : Center(
              child: Container(
                alignment: Alignment.center,
                width: mediaQuery.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      flex: 4,
                      child: Stack(
                        alignment: Alignment.center,
                        clipBehavior: Clip.none,
                        children: [
                          if (shopById.restourant.data.photos != null)
                            Stack(
                              // overflow: Overflow.visible,
                             children: [
                              CustomSliderImage(
                                photos: shopById.restourant.data.photos,
                              ),
                              Positioned(
                                child: posintionedonImage(),
                                left: 10,
                                top: 10,
                              )
                            ]),
                          if (shopById.restourant.data.photos == null)
                            Stack(
                              
                              // overflow: Overflow.visible,
                               children: [
                              Container(
                                height: mediaQuery.height * 0.3,
                                width: mediaQuery.width,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/images/icon-001.png'))),
                              ),
                              Positioned(
                                child: posintionedonImage(),
                                left: 10,
                                top: 10,
                              )
                            ]),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: Positioned(
                              bottom: mediaQuery.height * 0.04,
                              width: mediaQuery.width * 0.7,
                              child: Container(
                                  height: mediaQuery.height * 0.05,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Color.fromRGBO(201, 202, 207, 0.8),
                                      borderRadius: BorderRadius.circular(25)),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 8.0,
                                    ),
                                    child: Container(
                                      alignment: Alignment.center,
                                      width: mediaQuery.width * 0.7,
                                      child: TextFormField(
                                        keyboardType: TextInputType.name,
                                        textCapitalization:
                                            TextCapitalization.sentences,
                                        textInputAction: TextInputAction.search,
                                        controller: _searchController,
                                        onChanged: (val) {
                                          checkProvider.query = val.trim();

                                          checkProvider.searchResult(
                                              searchResultsparmter:
                                                  prdouctFilter.productsFilter,
                                              controllerPamter:
                                                  checkProvider.query);
                                        },
                                        decoration: InputDecoration(
                                          contentPadding:
                                              EdgeInsets.only(top: 8.0),
                                          isDense: true,
                                          hintText: ' البحث في المتجر',
                                          suffixIcon: Container(
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      234, 237, 244, 0.8),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  25),
                                                          topLeft:
                                                              Radius.circular(
                                                                  25))),
                                              child: Consumer<CheckProvider>(
                                                  builder: (context, snap, _) {
                                                if (checkProvider != null &&
                                                    checkProvider.query != "") {
                                                  return Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            bottom: 8.0),
                                                    child: IconButton(
                                                      iconSize: 20,
                                                      icon: Icon(
                                                        Icons.close,
                                                        color: Colors.green,
                                                      ),
                                                      onPressed: () {
                                                        checkProvider
                                                            .searchResults
                                                            .clear();
                                                        WidgetsBinding.instance
                                                            .addPostFrameCallback((_) =>
                                                                _searchController
                                                                    .clear());
                                                        checkProvider.query =
                                                            "";
                                                        checkProvider.searchResult(
                                                            searchResultsparmter:
                                                                prdouctFilter
                                                                    .productsFilter,
                                                            controllerPamter:
                                                                checkProvider
                                                                    .query);
                                                      },
                                                    ),
                                                  );
                                                } else {
                                                  return Icon(
                                                    Icons.search,
                                                    size: 20,
                                                    color: Colors.green,
                                                  );
                                                }
                                              })),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  )),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    CategoriesFilterById(),
                    SizedBox(
                      height: 20,
                    ),
                    Expanded(
                      flex: 7,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: mediaQuery.width * 0.9,
                          child: ListView(
                            primary: false,
                            shrinkWrap: true,
                            children: [
                              if (shopById.restourant.data.details != null)
                                Text(
                                  ': وصف المتجر',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              SizedBox(
                                height: 5,
                              ),
                              if (shopById.restourant.data.details != null)
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    shopById.restourant.data.details,
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.grey),
                                  ),
                                ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                ': المنتجات',
                                textAlign: TextAlign.right,
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Consumer<CheckProvider>(
                                      builder: (context, snap, _) {
                                    if (snap.productsFilter == null ||
                                        (snap.productsFilter != null &&
                                            snap.productsFilter.length < 0)) {
                                      return AppLoader();
                                    } else if ((snap.productsFilter.length <
                                                1 &&
                                            snap.searchResults == null) ||
                                        (snap.searchResults != null &&
                                            snap.searchResults.length < 1 &&
                                            snap.query != "")) {
                                      return Center(
                                          child: Text('لا يوجد نتائج'));
                                    } else if (snap.query != "") {
                                      return SearchShopsIteams(
                                        suggteionList: snap.searchResults,
                                        query: snap.query,
                                      );
                                    } else {
                                      return ListView.builder(
                                        primary: false,
                                        shrinkWrap: true,
                                        itemCount: snap.productsFilter.length,
                                        itemBuilder: (context, i) => ShopItem(
                                          name: snap.productsFilter[i].name,
                                          orderWaiting: false,
                                          productsFilter:
                                              snap.productsFilter[i],
                                          photo: snap.productsFilter[i]
                                              .photos[0].photo,
                                          id: snap.productsFilter[i].id,
                                          price: snap.productsFilter[i].price,
                                          available:
                                              snap.productsFilter[i].available,
                                        ),
                                      );
                                    }
                                  }))
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
