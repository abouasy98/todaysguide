import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:todaysguide/src/provider/post/fetchbigDataProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/filtterCard.dart';

import '../../../../provider/get/categoriesProvider.dart';
import '../../../../provider/post/productsFilterProvider.dart';

// ignore: must_be_immutable
class CategoriesFilter extends StatefulWidget {
  int page;
  int shopId;
  CategoriesFilter({this.page, this.shopId});
  @override
  _CategoriesFilterState createState() => _CategoriesFilterState();
}

class _CategoriesFilterState extends State<CategoriesFilter> {
  FetchDataProvider productList;
  int slected = 0;
  @override
  void initState() {
    Future.delayed(Duration(microseconds: 150), () {
      Provider.of<CategoriesProvider>(context, listen: false)
          .getcategories(context);
    });
    shopsProvider = Provider.of<ProductsFilterProvider>(context, listen: false);
    productList = Provider.of<FetchDataProvider>(context, listen: false);
    categoriesProvider =
        Provider.of<CategoriesProvider>(context, listen: false);
    all = true;

    super.initState();
  }

  CategoriesProvider categoriesProvider;
  ProductsFilterProvider shopsProvider;
  bool all;

  @override
  Widget build(BuildContext context) {
    print(widget.shopId);
    return Padding(
      padding: const EdgeInsets.only(top: 10, right: 3),
      child: Container(
        height: 50,
        child: AnimationLimiter(
          child: ListView(
            scrollDirection: Axis.horizontal,
            reverse: true,
            children: [
              Filtter(
                onTap: () async {
                  productList.resetStreams();
                  await productList.setLoadingState(LoadMoreStatus.INITIAL);
                  setState(() {
                    all = true;
                    productList.categoryId = 0;
                  });

                  for (int i = 0;
                      i < categoriesProvider.categories.length;
                      i++) {
                    categoriesProvider.categories[i].selected = false;
                  }
                  await productList.fetchProducts(
                      context: context, shopId: widget.shopId);
                },
                backgroundColor: all
                    ? Theme.of(context).primaryColor
                    : Theme.of(context).scaffoldBackgroundColor,
                title: 'الكل',
                elevation: all ? 3 : 0,
                titleColor: all ? Colors.white : Colors.black87,
              ),
              ListView.builder(
                  shrinkWrap: true,
                  reverse: true,
                  itemCount:
                      Provider.of<CategoriesProvider>(context, listen: true)
                          .categories
                          .length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (c, index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      delay: Duration(milliseconds: 400),
                      child: SlideAnimation(
                        duration: Duration(milliseconds: 400),
                        horizontalOffset: 50,
                        child: FadeInAnimation(
                          child: Filtter(
                            title: categoriesProvider.categories[index].name,
                            onTap: () async {
                              productList.resetStreams();
 
                              await productList
                                  .setLoadingState(LoadMoreStatus.INITIAL);
                              setState(() {
                                all = false;
                                categoriesProvider.categories[index].selected =
                                    true;
                                for (int i = 0;
                                    i < categoriesProvider.categories.length;
                                    i++) {
                                  setState(() {
                                    if (i == index) {
                                      categoriesProvider
                                          .categories[i].selected = true;
                                      productList.categoryId =
                                          categoriesProvider.categories[i].id;
                                    } else {
                                      Provider.of<CategoriesProvider>(context,
                                              listen: false)
                                          .categories[i]
                                          .selected = false;
                                    }
                                  });
                                }
                              });
                              await productList.fetchProducts(
                                  context: context, shopId: widget.shopId);
                            },
                            backgroundColor: Provider.of<CategoriesProvider>(
                                        context,
                                        listen: true)
                                    .categories[index]
                                    .selected
                                ? Theme.of(context).primaryColor
                                : null,
                            elevation: Provider.of<CategoriesProvider>(context,
                                        listen: true)
                                    .categories[index]
                                    .selected
                                ? 3
                                : null,
                            titleColor: Provider.of<CategoriesProvider>(context,
                                        listen: true)
                                    .categories[index]
                                    .selected
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                      ),
                    );
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
