import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/shopDetailsById.dart';
import '../productDetails/productDetails.dart';

class ShopItem extends StatelessWidget {
  final String photo;
  final String name;
  final bool orderWaiting;
  final String price;
  final int available;
  final int id;
  final int quantity;
  final int type;

  final productsFilter;
  ShopItem(
      {this.name,
      this.photo,
      this.price,
      this.available,
      @required this.productsFilter,
      this.id,
      @required this.orderWaiting,
      this.quantity,
      this.type});
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        if (orderWaiting) {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ShopDetailsById(
                    id: id,
                  )));
        } else {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ProductDetails(
                    productsFilter: productsFilter,
                  )));
        }
      },
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Row(children: [
          Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15))),
            child: Container(
              alignment: Alignment.center,
              height: mediaQuery.height * 0.08,
              child: photo != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(15.0),
                      child: FadeInImage.assetNetwork(
                        image: photo,
                        width: mediaQuery.width * 0.2,
                        fadeInCurve: Curves.bounceInOut,
                        
                        fadeInDuration: Duration(seconds: 2),
                        placeholder: "assets/images/newLogo.png",
                        imageCacheHeight: 500,
                        imageCacheWidth: 500,
                        fit: BoxFit.fill,
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage('assets/images/newLogo.png'),
                          fit: BoxFit.fill),
                    )),
            ),
          ),
          Container(
            width: mediaQuery.width * 0.65,
            child: ListTile(
              //  isThreeLine: true,
              leading: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: mediaQuery.width * 0.3,
                    alignment: Alignment.topRight,
                    child: Text(
                      name,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.right,
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  if (type != 2)
                    Container(
                      width: mediaQuery.width * 0.35,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text('عدد المتاح : ',
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 10)),
                          Text(
                            '$available منتج',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 10,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                ],
              ),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    width: mediaQuery.width * 0.2,
                    alignment: Alignment.topLeft,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '${NumberFormat.currency(decimalDigits: 0).parse(price)}',
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(width: 3),
                        Text(
                          'SR',
                          style: TextStyle(color: Colors.grey, fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  if (type == 2)
                    Container(
                        width: mediaQuery.width * 0.25,
                        height: mediaQuery.height * 0.04,
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(246, 247, 251, 1),
                            borderRadius: BorderRadius.circular(20)),
                        child: Center(child: Text("التفاصيل")))
                  else
                    Text("")
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
