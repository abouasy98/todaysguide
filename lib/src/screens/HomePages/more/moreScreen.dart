import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/DriverProvider/availabiltyProvider.dart';
import 'package:todaysguide/src/provider/auth/logOutProvider.dart';
import 'package:todaysguide/src/provider/get/setting.dart';
import 'package:todaysguide/src/screens/DriverHome/OurService/ourService.dart';
import 'package:todaysguide/src/screens/DriverHome/editDriverScreen.dart';
import 'package:todaysguide/src/screens/HomePages/more/internal/shopHours.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_option_card.dart';
import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';
import 'package:todaysguide/src/screens/StoreHome/editLocationStore.dart';
import 'package:todaysguide/src/screens/StoreHome/editSlider.dart';
import 'package:todaysguide/src/screens/StoreHome/editStoreProfile.dart';
import 'package:url_launcher/url_launcher.dart';
import 'internal/Wallet/wallet.dart';
import 'internal/about.dart';
import 'internal/completeOrders.dart';
import 'internal/editData/editProfile.dart';
import 'internal/historyScreen.dart';
import 'internal/terms.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;
  SettingProvider settingProvider;
  CustomOptionCard _optionCard = CustomOptionCard();
  _sendWhatsApp() async {
    var url =
        "https://wa.me/+${settingProvider.userDataModel.data.phoneNumber}";
    await canLaunch(url) ? launch(url) : print('No WhatsAPP');
  }

  CustomDialog _dialog = CustomDialog();
  SharedPreferences _prefs;
  _getShared() async {
    var _instance = await SharedPreferences.getInstance();
    setState(() {
      _prefs = _instance;
    });
    settingProvider = Provider.of<SettingProvider>(context, listen: false);
  }

  @override
  void initState() {
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });
      print('The device Token is :' + _deviceToken);
    });

    _getShared();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Theme.of(context).primaryColor,
      statusBarBrightness: Brightness.dark,
    ));

    return Scaffold(
      backgroundColor: Color.fromRGBO(246, 247, 251, 20),
      appBar: AppBar(
        title: Text("الاعدادات"),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token != null,
            child: _optionCard.optionCard(
                onTap: () {
                  if (_prefs.getInt('type') == 1) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => EditProfile()));
                  } else if (_prefs.getInt('type') == 2) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EditDriverProfile()));
                  } else if (_prefs.getInt('type') == 3) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EditStoreProfile()));
                  }
                },
                label: "تعديل حسابي ",
                //  'تعديل حسابي ',
                icon: "assets/images/03.png"),
          ),
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token != null &&
                    Provider.of<SharedPref>(context, listen: false).type == 3,
            child: _optionCard.optionCard(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EditLocationStore())),
                label: "تعديل موقع المتجر",
                //  'عن التطبيق   ',
                icon: "assets/images/16.png"),
          ),
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token != null &&
                    Provider.of<SharedPref>(context, listen: false).type == 3,
            child: _optionCard.optionCard(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EditSlider())),
                label: "تعديل السلايدر",
                //  'عن التطبيق   ',
                icon: "assets/images/16.png"),
          ),
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token != null &&
                    Provider.of<SharedPref>(context, listen: false).type == 3,
            child: _optionCard.optionCard(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ShopHours())),
                label: "مواعيد العمل",
                //  'عن التطبيق   ',
                icon: "assets/images/16.png"),
          ),
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token != null &&
                    Provider.of<SharedPref>(context, listen: false).type == 1,
            child: _optionCard.optionCard(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CompleteOrders())),
                label: "مدفوعاتي",
                icon: "assets/images/06.png"),
          ),
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token != null &&
                    Provider.of<SharedPref>(context, listen: false).type != 1,
            child: _optionCard.optionCard(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MyWallet())),
                label: "المحفظة",
                icon: "assets/images/18.png"),
          ),
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token != null,
            child: _optionCard.optionCard(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OurService())),
                label: "خدماتي ",
                //  'تعديل حسابي ',
                icon: "assets/images/08.png"),
          ),
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token != null,
            child: _optionCard.optionCard(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HistoryScreen())),
                label: "سجل العمليات",
                icon: "assets/images/10.png"),
          ),
          _optionCard.optionCard(
            icon: "assets/images/12.png",
            onTap: _sendWhatsApp,
            label: 'تواصل معنا',
          ),
          _optionCard.optionCard(
            onTap: () {
              Theme.of(context).platform != TargetPlatform.iOS
                  ? shareTheApp()
                  : shareTheAppIOS();
            },
            label: "مشاركة التطبيق",
            // 'مشاركة التطبيق',
            icon: "assets/images/14.png",
          ),
          _optionCard.optionCard(
            icon: "assets/images/16.png",
            onTap: () => Navigator.push(
                context, MaterialPageRoute(builder: (context) => TermsApp())),
            label: "الشروط و الاحكام",
            //  'الشروط و الاحكام',
          ),
          _optionCard.optionCard(
              onTap: () => Navigator.push(
                  context, MaterialPageRoute(builder: (context) => AboutApp())),
              label: "عن التطبيق",
              //  'عن التطبيق   ',
              icon: "assets/images/18.png"),
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token != null,
            child: _optionCard.optionCard(
              icon: "assets/images/20.png",
              onTap: () {
                _dialog.showOptionDialog(
                    context: context,
                    msg: 'تسجيل الخروج ؟',
                    okFun: () async {
                      if (Provider.of<SharedPref>(context, listen: false)
                                  .type ==
                              2 &&
                          Provider.of<SharedPref>(context, listen: false)
                                  .avalable ==
                              1) {
                       await Provider.of<AvailabilityProvider>(context,
                                listen: false)
                            .changeAvailable(
                                available: 0,
                                context: context,
                                token: Provider.of<SharedPref>(context,
                                        listen: false)
                                    .token);
                      }

                      await Provider.of<LogOutProvider>(context, listen: false)
                          .logOut(
                              _deviceToken,
                              context,
                              Provider.of<SharedPref>(context, listen: false)
                                  .token);
                      await _prefs.clear();
                      Navigator.pop(context, true);
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SignInScreen(),
                        ),
                        (Route<dynamic> route) => false,
                      );
                    },
                    okMsg: 'نعم',
                    cancelMsg: 'لا',
                    cancelFun: () {
                      return;
                    });
              },
              label: "خروج",
              //  'خروج',
            ),
          ),
          Visibility(
            visible:
                Provider.of<SharedPref>(context, listen: false).token == null,
            child: _optionCard.optionCard(
              label: 'تسجيل دخول',
              icon: "assets/images/20.png",
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SignInScreen()));
              },
            ),
          ),
        ],
      ),
    );
  }

  shareTheApp() async {
    try {
      String _msg;
      StringBuffer _sb = new StringBuffer();
      setState(() {
        _sb.write("لتنزيل التطبيق اضغط على الرابط\n");
        _sb.write(
            "https://play.google.com/store/apps/details?id=com.tqnee.todaysguide");
        _msg = _sb.toString();
      });
      final ByteData bytes = await rootBundle.load('assets/images/newLogo.png');
      await Share.file(
          'esys image', 'esys.png', bytes.buffer.asUint8List(), 'image/png',
          text: _msg);
    } catch (e) {
      print('error: $e');
    }
  }

  shareTheAppIOS() async {
    try {
      String _msg;
      StringBuffer _sb = new StringBuffer();
      setState(() {
        _sb.write("لتنزيل التطبيق اضغط على الرابط\n");
        _sb.write(
            "https://apps.apple.com/us/app/%D8%AF%D9%84%D9%8A%D9%84-%D8%A7%D9%84%D9%8A%D9%88%D9%85/id1552264740");
        _msg = _sb.toString();
      });
      final ByteData bytes = await rootBundle.load('assets/images/newLogo.png');
      await Share.file(
          'esys image', 'esys.png', bytes.buffer.asUint8List(), 'image/png',
          text: _msg);
    } catch (e) {
      print('error: $e');
    }
  }
}
