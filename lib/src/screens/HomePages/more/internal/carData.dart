import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';

import 'widget/carDocument.dart';

class EditCarData extends StatefulWidget {
  @override
  _EditCarDataState createState() => _EditCarDataState();
}

class _EditCarDataState extends State<EditCarData> {
 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(
          "تعديل بيانات السيارة",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          CarDocuments(
            token: Provider.of<SharedPref>(context, listen: false).token,
          )
        ],
      ),
    );
  }
}
