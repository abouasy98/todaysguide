import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/post/ClientOrderProvider.dart';
import 'package:todaysguide/src/screens/DriverHome/HomePage/widget/total_card.dart';
import 'package:todaysguide/src/screens/HomePages/more/internal/widget/CompleteOrderCard.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';

class CompleteOrders extends StatefulWidget {
  @override
  _CompleteOrdersState createState() => _CompleteOrdersState();
}

class _CompleteOrdersState extends State<CompleteOrders> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text("مدفوعاتي"),
            centerTitle: true,
            elevation: 0,
          ),
          body: FutureBuilder(
              future: Provider.of<ClientOrdersProvider>(context, listen: false)
                  .getClientOrders(
                      Provider.of<SharedPref>(context, listen: false).token,
                      2,
                      context),
              builder: (c, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return AppLoader();
                  default:
                    if (snapshot.hasError)
                      return Text('Error: ${snapshot.error}');
                    else {
                      double total = 0;
                      if (snapshot.data.data != null)
                        for (int i = 0; i < snapshot.data.data.length; i++) {
                          total += double.parse(
                                  snapshot.data.data[i].orderPrice ?? 0) +
                              double.parse(snapshot.data.data[i].price ?? "0");
                        }
                      return snapshot.data.data == null
                          ? Center(
                              child: Text("لا يوجد مدفوعات"),
                            )
                          : ListView(
                              children: [
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height / 1.5,
                                  child: ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: snapshot.data.data.length,
                                      itemBuilder: (c, index) => CompleteOrder(
                                            driverCommitions:
                                                snapshot.data.data[index],
                                          )),
                                ),
                                TotalCard(
                                  total: "$total",
                                  title: 'إجمالي العمولات المدفوعة',
                                )
                              ],
                            );
                    }
                }
              }),
        ));
  }
}
