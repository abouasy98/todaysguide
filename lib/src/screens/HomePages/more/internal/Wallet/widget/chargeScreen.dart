import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/chargeOnlineProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/loader_btn.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';
import '../bankPayment.dart';

class ChargeScreen extends StatefulWidget {
  @override
  _ChargeScreenState createState() => _ChargeScreenState();
}

class _ChargeScreenState extends State<ChargeScreen> {
  String price;
  final _form = GlobalKey<FormState>();
  bool autoError = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "شحن المحفظة",
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          leading: InkWell(
            onTap: () => Navigator.pop(context),
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          ),
        ),
        body: Form(
            key: _form,
            autovalidateMode: autoError ? AutovalidateMode.always : AutovalidateMode.disabled,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  RegisterTextField(
                    onChange: (v) {
                      price = v;
                    },
                    label: 'قيمة الشحن',
                    icon: Icons.attach_money,
                    type: TextInputType.number,
                  ),
                  SizedBox(height: 20),
                  LoaderButton(
                    load: false,
                    text: 'شحن الآن',
                    color: Theme.of(context).primaryColor,
                    onTap: () {
                      setState(() {
                        autoError = true;
                      });
                      final isValid = _form.currentState.validate();
                      if (!isValid) {
                        return;
                      }
                      _form.currentState.save();
                      // paymentBloc.add(Click());

                      Provider.of<ChargeOnlineProvider>(context, listen: false)
                          .subscribe(
                              Provider.of<SharedPref>(context, listen: false)
                                  .token,
                              price,
                              context);
                    },
                    txtColor: Colors.white,
                  ),
                  SizedBox(height: 20),
                  LoaderButton(
                    load: false,
                    text: 'الشحن عن طريق تحويل للحساب البنكي',
                    color: Theme.of(context).primaryColor,
                    onTap: () {
                      setState(() {
                        autoError = true;
                      });
                      final isValid = _form.currentState.validate();
                      if (!isValid) {
                        return;
                      }
                      _form.currentState.save();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (c) => BankingPay(
                                    price: price,
                                  )));
                    },
                    txtColor: Colors.white,
                  ),
                ],
              ),
            )));
  }
}
