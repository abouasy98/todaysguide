import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/changeData/changePasswordProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/buttonSignIn.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_secure_text_field.dart';

class EditPassword extends StatefulWidget {
  @override
  _EditPasswordState createState() => _EditPasswordState();
}

class _EditPasswordState extends State<EditPassword> {

  @override
  void initState() {
  
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ListView(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          children: <Widget>[
         
            RegisterSecureTextField(
              icon: Icons.lock,
              label: "كلمة المرور",
              onChange: (v) {
                Provider.of<ChangePasswordProvider>(context, listen: false)
                    .oldPassword = v;
              },
            ),
            SizedBox(height: 20),
            RegisterSecureTextField(
              icon: Icons.lock,
              label: 'كلمة المرور الجديده',
              onChange: (v) {
                Provider.of<ChangePasswordProvider>(context, listen: false)
                    .password = v;
              },
            ),
            SizedBox(height: 20),
            RegisterSecureTextField(
              icon: Icons.lock,
              label: 'تاكيد كلمة المرور الجديده',
              onChange: (v) {
                Provider.of<ChangePasswordProvider>(context, listen: false)
                    .passwordConfirmation = v;
              },
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: SignInButton(
                txtColor: Colors.white,
                onPressSignIn: () {
                  Provider.of<ChangePasswordProvider>(context, listen: false)
                      .changePassword(Provider.of<SharedPref>(context, listen: false).token, context);
                },
                btnWidth: MediaQuery.of(context).size.width - 40,
                btnHeight: 45,
                btnColor: Theme.of(context).primaryColor,
                buttonText: 'تعديل',
              ),
            ),
            SizedBox(
              height: 40,
            ),
          ],
        ),
      ],
    );
  }
}
