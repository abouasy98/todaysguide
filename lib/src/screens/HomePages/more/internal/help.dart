import 'package:flutter/material.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_app_bar.dart';
import 'package:todaysguide/src/screens/MainWidgets/details_text_field_no_img%20copy.dart';
import 'package:todaysguide/src/screens/MainWidgets/loader_btn.dart';
import 'package:todaysguide/src/screens/MainWidgets/register_text_field.dart';

class HelpCenter extends StatefulWidget {
  @override
  _HelpCenterState createState() => _HelpCenterState();
}

class _HelpCenterState extends State<HelpCenter> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          iconData: Icons.arrow_back_ios,
          label: 'مركز المساعدة',
          onTap: () => Navigator.pop(context),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          RegisterTextField(
            label: 'عنوان الرسالة',
            type: TextInputType.text,
            onChange: (v) {},
            icon: Icons.label,
          ),
          SizedBox(height: 20),
          DetailsTextFieldNoImg(
            onChange: () {},
            label: 'مُحتوى الرسالة',
          ),
          Padding(
            padding: EdgeInsets.all(50),
            child: LoaderButton(
              onTap: () {},
              load: false,
              text: 'إرسال',
              color: Theme.of(context).primaryColor,
              txtColor: Colors.white,
            ),
          )
        ],
      ),
      //  BlocListener<HelpBloc, AppState>(
      //   bloc: helpBloc,
      //   listener: (_, state) {
      //     if (state is Done) {
      //       CustomAlert().toast(context: context, title: 'تم إرسال رسالتك للإدارة');
      //      Provider.of<SharedPref>(context,listen: false).type == 1 ?
      //       Navigator.pushReplacement(
      //           context, MaterialPageRoute(builder: (_) => MainPage(
      //             index: 2,
      //           ))):
      //           Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => MainPageDriver(index: 1,)));
      //     }
      //   },
      //   child: BlocBuilder<HelpBloc, AppState>(
      //     bloc: helpBloc,
      //     builder: (_, state) {
      //       return
      //     },
      //   ),
      // ),
    );
  }
}
