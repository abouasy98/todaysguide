import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/post/ClientOrderProvider.dart';
import 'package:todaysguide/src/screens/DriverHome/DriverOrder/widget/orderCard.dart';
import 'package:todaysguide/src/screens/HomePages/Home/activeOrderDetails.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';

class MyOrder extends StatefulWidget {
  @override
  _MyOrderState createState() => _MyOrderState();
}

class _MyOrderState extends State<MyOrder> {
  @override
  void initState() {
    Provider.of<ClientOrdersProvider>(context, listen: false).getClientOrders(
        Provider.of<SharedPref>(context, listen: false).token, 1, context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: DefaultTabController(
          initialIndex: 0,
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              title: Text("طلباتي"),
              centerTitle: true,
              elevation: 0,
              bottom: PreferredSize(
                preferredSize: Size.fromHeight(60),
                child: Container(
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TabBar(
                        unselectedLabelColor: Colors.black,
                        indicatorSize: TabBarIndicatorSize.label,
                        indicator: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Theme.of(context).primaryColor,
                        ),
                        tabs: [
                          Tab(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(
                                    color: Theme.of(context).primaryColor,
                                    width: 1),
                              ),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text("الطلبات المكتملة"),
                              ),
                            ),
                          ),
                          Tab(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(
                                    color: Theme.of(context).primaryColor,
                                    width: 1),
                              ),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text("الطلبات النشطة"),
                              ),
                            ),
                          ),
                        ]),
                  ),
                ),
              ),
            ),
            body: Provider.of<SharedPref>(context, listen: false).token == null
            ? Center(
                child: Row(
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    "لمشاهدة الطلبات يرجي التسجيل اولا",
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (c) => SignInScreen()));
                    },
                    child: Text(
                      "تسجيل الدخول",
                      style: TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ))
            : TabBarView(children: [
              FutureBuilder(
                  future: Provider.of<ClientOrdersProvider>(context,
                          listen: false)
                      .getClientOrders(
                          Provider.of<SharedPref>(context, listen: false).token,
                          2,
                          context),
                  builder: (c, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:
                        return AppLoader();
                      default:
                        if (snapshot.hasError)
                          return Text('Error: ${snapshot.error}');
                        else
                          return snapshot.data.data == null
                              ? Center(
                                  child: Text("لا يوجد طلبات"),
                                )
                              : ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: snapshot.data.data.length,
                                  itemBuilder: (c, index) => OrderCard(
                                        onTap: () {
                                              Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (c) =>
                                                      ActiveOrderDetails(
                                                        id: snapshot.data
                                                            .data[index].id,
                                                            type: 2,
                                                      )));
                                        },
                                        createdAt: snapshot
                                            .data.data[index].createdAt
                                            .toString()
                                            .substring(0, 10),
                                        image: snapshot
                                                .data.data[index].shopPhoto ??
                                            "",
                                        orderNum: snapshot.data.data[index].id
                                            .toString(),
                                        placeName:
                                            snapshot.data.data[index].shop,
                                        price:
                                            snapshot.data.data[index].totalPrice ??
                                                "0",
                                      ));
                    }
                  }),
              FutureBuilder(
                  future: Provider.of<ClientOrdersProvider>(context,
                          listen: false)
                      .getClientOrders(
                          Provider.of<SharedPref>(context, listen: false).token,
                          1,
                          context),
                  builder: (c, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:
                        return AppLoader();
                      default:
                        if (snapshot.hasError)
                          return Text('Error: ${snapshot.error}');
                        else
                          return snapshot.data.data == null
                              ? Center(
                                  child: Text("لا يوجد طلبات"),
                                )
                              : ListView.builder(
                                
                                  shrinkWrap: true,
                                  itemCount: snapshot.data.data.length,
                                  itemBuilder: (c, index) => OrderCard(
                                    type: 1,
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (c) =>
                                                      ActiveOrderDetails(
                                                        id: snapshot.data
                                                            .data[index].id,
                                                      )));

                                        },
                                        createdAt: snapshot
                                            .data.data[index].createdAt
                                            .toString()
                                            .substring(0, 10),
                                        image: snapshot
                                                .data.data[index].shopPhoto ??
                                            "",
                                        orderNum: snapshot.data.data[index].id
                                            .toString(),
                                        placeName:
                                            snapshot.data.data[index].shop,
                                        price:
                                            snapshot.data.data[index].totalPrice ??
                                                "0",
                                      ));
                    }
                  })
            ]),
          )),
    );
  }
}
