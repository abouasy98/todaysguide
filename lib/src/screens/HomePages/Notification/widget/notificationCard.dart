import 'package:flutter/material.dart';

import 'package:todaysguide/src/Models/get/notification.dart';

class NotificationCard extends StatefulWidget {
  final NotificationItem notification;

  const NotificationCard({Key key, this.notification}) : super(key: key);

  @override
  _NotificationCardState createState() => _NotificationCardState();
}

class _NotificationCardState extends State<NotificationCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right:8.0,left:8),
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(15)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,

                      children: [
                        Text(widget.notification.title,style: TextStyle(fontWeight: FontWeight.bold),),
                        Text(widget.notification.message,style: TextStyle(color: Colors.grey),)
                      ],
                    )
                  ],
                )
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: <Widget>[

                //       Text(
                //        "${intl.DateFormat("yyyy-MM-dd").format(widget.notification.createdAt)}",
                //         style: TextStyle(color: Colors.grey),
                //         textAlign: TextAlign.left,
                //       ),
                //       Padding(
                //         padding: const EdgeInsets.only(right:8.0),
                //         child: Text(widget.notification.title,style: TextStyle(fontWeight: FontWeight.bold),),
                //       ),
                //     ],
                //   ),
                // ),
                // Padding(
                //   padding: EdgeInsets.only(right: 10, left: 10),
                //   child: Container(
                //     width: 150,
                //     child: Text(
                //       widget.notification.message,
                //       style: TextStyle(fontSize: 15),
                //       textDirection: TextDirection.rtl,
                //     ),
                //   ),
                // ),
                // SizedBox(height: 5),
                // Divider(
                //   height: 1,
                //   color: Colors.grey,
                // )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
