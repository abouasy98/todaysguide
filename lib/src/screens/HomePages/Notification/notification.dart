import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/get/deleteNotificationProvider.dart';
import 'package:todaysguide/src/provider/get/notificationProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Notification/widget/notificationCard.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';

class AppNotification extends StatefulWidget {
  @override
  _AppNotificationState createState() => _AppNotificationState();
}

class _AppNotificationState extends State<AppNotification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(246, 247, 251, 20),
      appBar: AppBar(
        title: Text(
          "الاشعارات",
        ),
        centerTitle: true,
        leading: Icon(null),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Provider.of<SharedPref>(context, listen: false).token == null
            ? Center(
                child: Row(
                textDirection: TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text(
                    "لمشاهدة الاشعارات يرجي التسجيل اولا",
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (c) => SignInScreen()));
                    },
                    child: Text(
                      "تسجيل الدخول",
                      style: TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ))
            : FutureBuilder(
                future: Provider.of<NotoficationProvider>(context,
                        listen: false)
                    .getNotification(
                        Provider.of<SharedPref>(context, listen: false).token,
                        context),
                builder: (c, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return AppLoader();
                    default:
                      if (snapshot.hasError)
                        return Center(child: Text('Error: ${snapshot.error}'));
                      else
                        return snapshot.data.data == null
                            ? Center(
                                child: Text("لا يوجد اشعارات"),
                              )
                            : ListView.builder(
                                shrinkWrap: true,
                                itemCount: Provider.of<NotoficationProvider>(
                                        context,
                                        listen: false)
                                    .notfications
                                    .length,
                                itemBuilder: (c, index) => Slidable(
                                      actionPane: SlidableDrawerActionPane(),
                                      actionExtentRatio: 0.25,
                                      child: NotificationCard(
                                        notification:
                                            Provider.of<NotoficationProvider>(
                                                    context,
                                                    listen: false)
                                                .notfications[index],
                                      ),
                                      actions: <Widget>[
                                        InkWell(
                                          onTap: () {
                                            Provider.of<DeletNotificationProvider>(
                                                    context,
                                                    listen: false)
                                                .deletNot(
                                                    Provider.of<SharedPref>(
                                                            context,
                                                            listen: false)
                                                        .token,
                                                    Provider.of<NotoficationProvider>(
                                                            context,
                                                            listen: false)
                                                        .notfications[index]
                                                        .id,
                                                    context)
                                                .then((v) {
                                              setState(() {});
                                              // _getShared();
                                            });
                                          },
                                          child: Material(
                                            shape: CircleBorder(),
                                            color: Colors.redAccent,
                                            child: Padding(
                                              padding: const EdgeInsets.all(15),
                                              child: Icon(Icons.delete,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        )
                                      ],
                                    ));
                  }
                }),
      ),
    );
  }
}
