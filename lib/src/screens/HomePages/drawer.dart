import 'package:cached_network_image/cached_network_image.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/auth/logOutProvider.dart';
import 'package:todaysguide/src/provider/get/setting.dart';
import 'package:todaysguide/src/screens/DriverHome/OurService/ourService.dart';
import 'package:todaysguide/src/screens/HomePages/more/internal/about.dart';
import 'package:todaysguide/src/screens/HomePages/more/internal/terms.dart';
import 'package:todaysguide/src/screens/MainWidgets/app_loader.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_option_card.dart';
import 'package:todaysguide/src/screens/Registeration/sign_in_screen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'more/internal/Wallet/wallet.dart';
import 'more/internal/completeOrders.dart';
import 'more/internal/editData/editProfile.dart';
import 'more/internal/historyScreen.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  CustomDialog _dialog = CustomDialog();
  CustomOptionCard _optionCard = CustomOptionCard();
  SharedPreferences _preferences;
  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;
  SettingProvider settingProvider;
  bool loaded = true;
  @override
  void initState() {
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });
      print('The device Token is :' + _deviceToken);
    });
    _getShared();
    super.initState();
  }

  _sendWhatsApp() async {
    var url =
        "https://wa.me/+${settingProvider.userDataModel.data.phoneNumber}";
    await canLaunch(url) ? launch(url) : print('No WhatsAPP');
  }

  SharedPreferences _prefs;
  _getShared() async {
    var _instance = await SharedPreferences.getInstance();
    setState(() {
      _prefs = _instance;
    });
    settingProvider = Provider.of<SettingProvider>(context, listen: false);
  }

  Widget drawerIcon({String text, Function func, IconData icon}) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Column(children: <Widget>[
        InkWell(
          onTap: func,
          child: ListTile(
            title: Text(
              text,
              style: TextStyle(color: Colors.black, fontSize: 20),
            ),
            leading: Icon(
              icon,
              color: Colors.blueAccent,
            ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 18,
              color: Colors.black,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            right: 15,
            left: 15,
          ),
          child: Divider(
            color: Colors.grey[500],
          ),
        ),
      ]),
    );
  }

  @override
  void didChangeDependencies() async {
    if (loaded) {
      var _instance = await SharedPreferences.getInstance();
      setState(() {
        _preferences = _instance;
      });
      setState(() {
        loaded = false;
      });
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return loaded
        ? AppLoader()
        : Container(
            child: Drawer(
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: ListView(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.08,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CircleAvatar(
                            radius: 35,
                            backgroundImage:
                                _preferences.getString('photo') != null
                                    ? CachedNetworkImageProvider(
                                        _preferences.getString('photo'),
                                      )
                                    : AssetImage(
                                        'assets/images/avatar.jpeg',
                                      ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.height * 0.02,
                          ),
                          Column(
                                 mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                _preferences.getString('name') != null
                                    ? 'اهلا بك ${_preferences.getString('name')}'
                                    : 'اهلا بك',
                                style:
                                    TextStyle(color: Colors.black, fontSize: 20),
                              ),
                              Visibility(
                                visible: Provider.of<SharedPref>(context,
                                            listen: false)
                                        .token !=
                                    null,
                                child: Text(
                                    _preferences.getString('phone') != null
                                        ? _preferences.getString('phone')
                                        : 'رقم الهاتف',
                                    style: TextStyle(color: Colors.grey)),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    // drawerIcon(
                    //     text: "مفضلاتي",
                    //     func: () {
                    //       Navigator.of(context).push(MaterialPageRoute(
                    //           builder: (context) => Favourites()));
                    //     },
                    //     icon: Icons.favorite),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                              .token !=
                          null,
                      child: _optionCard.optionCard(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EditProfile())),
                          label: "تعديل حسابي ",
                          //  'تعديل حسابي ',
                          icon: "assets/images/03.png"),
                    ),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                                  .token !=
                              null &&
                          Provider.of<SharedPref>(context, listen: false)
                                  .type ==
                              1,
                      child: _optionCard.optionCard(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CompleteOrders())),
                          label: "مدفوعاتي",
                          icon: "assets/images/06.png"),
                    ),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                                  .token !=
                              null &&
                          Provider.of<SharedPref>(context, listen: false)
                                  .type !=
                              1,
                      child: _optionCard.optionCard(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyWallet())),
                          label: "المحفظة",
                          icon: "assets/images/18.png"),
                    ),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                              .token !=
                          null,
                      child: _optionCard.optionCard(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OurService())),
                          label: "خدماتي ",
                          //  'تعديل حسابي ',
                          icon: "assets/images/08.png"),
                    ),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                              .token !=
                          null,
                      child: _optionCard.optionCard(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HistoryScreen())),
                          label: "سجل العمليات",
                          icon: "assets/images/10.png"),
                    ),
                    _optionCard.optionCard(
                      icon: "assets/images/12.png",
                      onTap: _sendWhatsApp,
                      label: 'تواصل معنا',
                    ),
                    _optionCard.optionCard(
                      onTap: () {
                        Theme.of(context).platform != TargetPlatform.iOS
                            ? shareTheApp()
                            : shareTheAppIOS();
                      },
                      label: "مشاركة التطبيق",
                      // 'مشاركة التطبيق',
                      icon: "assets/images/14.png",
                    ),
                    _optionCard.optionCard(
                      icon: "assets/images/16.png",
                      onTap: () => Navigator.push(context,
                          MaterialPageRoute(builder: (context) => TermsApp())),
                      label: "الشروط و الاحكام",
                      //  'الشروط و الاحكام',
                    ),
                    _optionCard.optionCard(
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AboutApp())),
                        label: "عن التطبيق",
                        //  'عن التطبيق   ',
                        icon: "assets/images/18.png"),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                              .token !=
                          null,
                      child: _optionCard.optionCard(
                        icon: "assets/images/20.png",
                        onTap: () {
                          _dialog.showOptionDialog(
                              context: context,
                              msg: 'تسجيل الخروج ؟',
                              okFun: () async {
                                await Provider.of<LogOutProvider>(context,
                                        listen: false)
                                    .logOut(
                                        _deviceToken,
                                        context,
                                        Provider.of<SharedPref>(context,
                                                listen: false)
                                            .token);
                                await _prefs.clear();
                                Navigator.pop(context, true);
                                Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => SignInScreen(),
                                  ),
                                  (Route<dynamic> route) => false,
                                );
                              },
                              okMsg: 'نعم',
                              cancelMsg: 'لا',
                              cancelFun: () {
                                return;
                              });
                        },
                        label: "خروج",
                        //  'خروج',
                      ),
                    ),
                    Visibility(
                      visible: Provider.of<SharedPref>(context, listen: false)
                              .token ==
                          null,
                      child: _optionCard.optionCard(
                        label: 'تسجيل دخول',
                        icon: "assets/images/20.png",
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignInScreen()));
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }

  shareTheApp() async {
    try {
      String _msg;
      StringBuffer _sb = new StringBuffer();
      setState(() {
        _sb.write("لتنزيل التطبيق اضغط على الرابط\n");
        _sb.write("");
        _msg = _sb.toString();
      });
      final ByteData bytes = await rootBundle.load('assets/images/newLogo.png');
      await Share.file(
          'esys image', 'esys.png', bytes.buffer.asUint8List(), 'image/png',
          text: _msg);
    } catch (e) {
      print('error: $e');
    }
  }

  shareTheAppIOS() async {
    try {
      String _msg;
      StringBuffer _sb = new StringBuffer();
      setState(() {
        _sb.write("لتنزيل التطبيق اضغط على الرابط\n");
        _sb.write("");
        _msg = _sb.toString();
      });
      final ByteData bytes = await rootBundle.load('assets/images/newLogo.png');
      await Share.file(
          'esys image', 'esys.png', bytes.buffer.asUint8List(), 'image/png',
          text: _msg);
    } catch (e) {
      print('error: $e');
    }
  }
}
