import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/get/setting.dart';
import 'package:todaysguide/src/provider/post/ClientOrderProvider.dart';
import 'package:todaysguide/src/screens/DriverHome/OurService/ServiceScreen.dart';
import 'package:todaysguide/src/screens/HomePages/Notification/notification.dart';
import 'package:todaysguide/src/screens/HomePages/more/moreScreen.dart';
import 'Home/ShopDetails/shopDetailsById.dart';
import 'Home/home.dart';
import 'Home/orderWaitingDriver/orderWaitingDriver.dart';
import 'Home/productDetails/productDetailsbyId.dart';
import 'MyOrder/myorderScreen.dart';

class MainPage extends StatefulWidget {
  final int index;
  MainPage({this.index});
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  bool isInit = true;
  ClientOrdersProvider clientOrdersProvider;
  getShared() async {
    await Provider.of<ClientOrdersProvider>(context, listen: false)
        .getClientOrders(
            Provider.of<SharedPref>(context, listen: false).token, 0, context);
    clientOrdersProvider =
        Provider.of<ClientOrdersProvider>(context, listen: false);
  }

  bool orderPage = false;
  int _selectedpage;
  int _selectedpage2;
  List<Map<String, Object>> pages;
  List<Map<String, Object>> pages2;
  @override
  void initState() {
    super.initState();

    _initDynamicLinks();
    pages = [
      {
        'page': MoreScreen(),
      },
      {
        'page': MyOrder(),
      },
      {
        'page': Home(),
      },
      {
        'page': ServiceScreen(),
      },
      {
        'page': AppNotification(),
      },
    ];
    _selectedpage = 2;
    pages2 = [
      {
        'page': MoreScreen(),
      },
      {
        'page': OrderWaitingDriver(),
      },
      {
        'page': Home(),
      },
      {
        'page': ServiceScreen(),
      },
      {
        'page': AppNotification(),
      },
    ];
    _selectedpage2 = 1;
    selectedpage();
  }

  void selectedpage() {
    if (widget.index != null) {
      setState(() {
        _selectedpage = widget.index;
      });
    }
  }

  void selectedpage2() {
    if (widget.index != null) {
      setState(() {
        _selectedpage2 = widget.index;
      });
    }
  }

  @override
  void didChangeDependencies() async {
    if (isInit) {
      await Provider.of<SettingProvider>(context, listen: false)
          .getUserData(context);
    
      if (Provider.of<SharedPref>(context, listen: false).token != null) {
        await getShared();
      }
      if (Provider.of<SharedPref>(context, listen: false).token == null) {
        setState(() {
          orderPage = false;
        });
        selectedpage();
      } else if (clientOrdersProvider.clientOrders.length == 0 &&
          Provider.of<SharedPref>(context, listen: false).token != null) {
        setState(() {
          orderPage = false;
        });
        selectedpage();
      } else if (clientOrdersProvider.clientOrders.length == 1 &&
          Provider.of<SharedPref>(context, listen: false).token != null) {
        setState(() {
          orderPage = true;
        });
        selectedpage2();
      }
      setState(() {
        isInit = false;
      });
    }
    super.didChangeDependencies();
  }

  void _initDynamicLinks() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;

    if (deepLink != null) {
      print(' ---------- ' + deepLink.path);
      String _state = deepLink.path.substring(1);
      print('state >>>>> ' + _state);

      String _value = _state.substring(2);
      print('value >>>>> ' + _value);
      if (_state.contains("0/")) {
        print('state >>>>>>>>>>>>>> "Shop"' + _state);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ShopDetailsById(
                  key: UniqueKey(),
                  id: int.parse(_value),
                )));
      } else {
        print('state >>>>>>>>>>>>>>>>> "product" ' + _state);
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => ProductDetailsById(
                  key: UniqueKey(),
                  id: int.parse(_value),
                )));
      }
    } else {}

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;
      if (deepLink != null) {
        print(' ---------- ' + deepLink.path);
        String _state = deepLink.path.substring(1);
        print('state >>>>> ' + _state);

        String _value = _state.substring(2);
        print('value >>>>> ' + _value);
        if (_state.contains("0/")) {
          print('state >>>>>>>>>>>>>> "Shop"' + _state);
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ShopDetailsById(
                    key: UniqueKey(),
                    id: int.parse(_value),
                  )));
        } else {
          print('state >>>>>>>>>>>>>>>>> "product" ' + _state);
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ProductDetailsById(
                    key: UniqueKey(),
                    id: int.parse(_value),
                  )));
        }
      } else {}
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body:
            //  isInit
            //     ? AppLoader()
            //     :
            !orderPage
                ? pages[_selectedpage]['page']
                : pages2[_selectedpage2]['page'],
        bottomNavigationBar: Card(
          elevation: 10,
          margin: EdgeInsets.only(bottom: 0.0),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40), topRight: Radius.circular(40))),
          child: BottomNavigationBar(
            elevation: 10,
            backgroundColor: Colors.white,
            onTap: (position) {
              !orderPage
                  ? setState(() {
                      _selectedpage = position;
                    })
                  : setState(() {
                      _selectedpage2 = position;
                    });
            },
            currentIndex: !orderPage ? _selectedpage : _selectedpage2,
            type: BottomNavigationBarType.fixed,
            selectedItemColor: Theme.of(context).primaryColor,
            selectedLabelStyle:
                TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
            unselectedLabelStyle:
                TextStyle(fontSize: 10, fontWeight: FontWeight.bold),
            items: [
              BottomNavigationBarItem(
                label: "حسابي",
                activeIcon: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: ImageIcon(
                      AssetImage("assets/icons/user.png"),
                      color: Theme.of(context).primaryColor,
                      size: 20,
                    ),
                  ),
                ),
                icon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/user.png"),
                    size: 20,
                  ),
                ),
              ),
              BottomNavigationBarItem(
                label: "طلباتي",
                activeIcon: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: ImageIcon(
                      AssetImage("assets/icons/request.png"),
                      color: Theme.of(context).primaryColor,
                      size: 20,
                    ),
                  ),
                ),
                icon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ImageIcon(
                    AssetImage(
                      "assets/icons/request.png",
                    ),
                    size: 20,
                  ),
                ),
              ),
              BottomNavigationBarItem(
                label: "الرئيسية",
                activeIcon: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: ImageIcon(
                      AssetImage("assets/icons/home.png"),
                      color: Theme.of(context).primaryColor,
                      size: 20,
                    ),
                  ),
                ),
                icon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/home.png"),
                    size: 20,
                  ),
                ),
              ),
              BottomNavigationBarItem(
                label: "دليل الخدمات",
                activeIcon: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: ImageIcon(
                      AssetImage("assets/icons/00000-00.png"),
                      color: Theme.of(context).primaryColor,
                      size: 20,
                    ),
                  ),
                ),
                icon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/00000-00.png"),
                    size: 20,
                  ),
                ),
              ),
              BottomNavigationBarItem(
                label: "الاشعارات",
                activeIcon: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(30)),
                  child: Center(
                    child: ImageIcon(
                      AssetImage("assets/icons/alarm.png"),
                      color: Theme.of(context).primaryColor,
                      size: 20,
                    ),
                  ),
                ),
                icon: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ImageIcon(
                    AssetImage("assets/icons/alarm.png"),
                    size: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
        floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}
