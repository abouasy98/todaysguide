import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Repository/firebaseNotifications.dart';
import 'package:todaysguide/src/provider/get/getUserDataProvider.dart';
import 'package:todaysguide/src/provider/get/setting.dart';
import 'package:todaysguide/src/provider/getMapImageProvider.dart';
import 'package:todaysguide/src/provider/post/deviceTokenProvider.dart';
import 'package:todaysguide/src/screens/DriverHome/mainPageDriver.dart';
import 'package:todaysguide/src/screens/HomePages/main_page.dart';
import 'package:todaysguide/src/screens/Intro/waitingScreen.dart';
import 'package:todaysguide/src/screens/MainWidgets/image_bg.dart';
import 'package:todaysguide/src/screens/StoreHome/mainPageStore.dart';

class Splash extends StatefulWidget {
  final GlobalKey<NavigatorState> navigator;

  const Splash({Key key, this.navigator}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with SingleTickerProviderStateMixin {
  AnimationController controller;

  Animation animation;
  final _fcm = FirebaseMessaging();
  String fcmToken = "DEFAULT_TOKEN";
  SharedPreferences _prefs;

  _getShared() async {
    _prefs = await SharedPreferences.getInstance();
    if (_prefs.get("deviceToken") == null) {
      _fcm.getToken().then((response) {
        if (response != null) {
          setState(() {
            fcmToken = response;
          });
          Provider.of<DeviceTokenProvider>(context, listen: false)
              .getToken(fcmToken, context)
              .then((res) {
            if (res.code == 200) {
              _prefs.setString("deviceToken", fcmToken);
              print('FCM: ' + fcmToken);
            }
          });
        }
      });
    }
    Provider.of<SharedPref>(context, listen: false).getSharedHelper(_prefs);
    if (_prefs.get("token") != null) getUserData();
    print('Api _ Token=${_prefs.get("token")}');
  }

  getUserData() async {
    await Provider.of<GetUserDataProvider>(context, listen: false)
        .getUserData(_prefs.get("token"), context)
        .then((res) {
      if (res.code == 401) {
        print('----- clear ----');
        _prefs.remove("name");
        _prefs.remove("phone");
        _prefs.remove("token");
        _prefs.remove("id");
        _prefs.remove("active");
        _prefs.remove("photo");
      }
    });
  }

  Future<Timer> _loadData() async {
    return Timer(Duration(seconds: 3), () {
      print("shop type >> ${_prefs.getInt('type')}");

      if (_prefs.getInt('type') == null) {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => MainPage(),
          ),
          (Route<dynamic> route) => false,
        );
      } else if (_prefs.getInt('type') == 1) {
        // Navigator.of(context).pushAndRemoveUntil(
        //     MaterialPageRoute(builder: (_) => MainPage()),
        //     (Route<dynamic> route) => false);
        // Navigator.pushAndRemoveUntil(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) => MainPageDriver(),
        //   ),
        //   (Route<dynamic> route) => false,
        // );
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (_) => MainPage()),
            (Route<dynamic> route) => false);
        //  Navigator.pushAndRemoveUntil(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) => MainPageDriver(),
        //   ),
        //   (Route<dynamic> route) => false,
        //);
      } else if (_prefs.getInt('type') == 2) {
        if (_prefs.get('active') == 1) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => MainPageDriver(),
            ),
            (Route<dynamic> route) => false,
          );
        } else {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => WaitingAccepting(),
            ),
            (Route<dynamic> route) => false,
          );
        }
      } else if (_prefs.getInt('type') == 3) {
        if (_prefs.get('active') == 1) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => MainPageStore(),
            ),
            (Route<dynamic> route) => false,
          );
        } else {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => WaitingAccepting(),
            ),
            (Route<dynamic> route) => false,
          );
        }
      }
    });
  }

  @override
  void initState() {
    Provider.of<MapHelper>(context, listen: false).getLocation();
    FirebaseNotifications().setUpFirebase(widget.navigator);
    _getShared();
    Provider.of<SettingProvider>(context, listen: false).getUserData(context);
    Provider.of<GetMapImage>(context, listen: false).setCustomMapPin();
    _loadData();
    super.initState();
  }

  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      body: Stack(
        children: <Widget>[
          ImageBG(),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(""),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/images/newLogo.png',
                      height: 200,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      Provider.of<SettingProvider>(context, listen: true)
                              .splashMsg ??
                          "",
                      style: TextStyle(
                          fontSize: 15,
                          color: Color.fromRGBO(156, 222, 255, 1)),
                    )
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
