import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/provider/auth/logOutProvider.dart';
import 'package:todaysguide/src/provider/get/setting.dart';
import 'package:todaysguide/src/screens/Intro/splash_screen.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';

class WaitingAccepting extends StatefulWidget {
  @override
  _WaitingAcceptingState createState() => _WaitingAcceptingState();
}

class _WaitingAcceptingState extends State<WaitingAccepting> {
  SettingProvider settingProvider;
  SharedPreferences _prefs;
  _getShared() async {
    var _instance = await SharedPreferences.getInstance();
    setState(() {
      _prefs = _instance;
    });
    settingProvider = Provider.of<SettingProvider>(context, listen: false);
  }

  CustomDialog _dialog = CustomDialog();
  @override
  void initState() {
    _fcm.getToken().then((response) {
      setState(() {
        _deviceToken = response;
      });
      print('The device Token is :' + _deviceToken);
    });

    _getShared();
    super.initState();
  }

  FirebaseMessaging _fcm = FirebaseMessaging();
  String _deviceToken;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          // ImageBG(),
          Center(
            child: Text("تم ارسال البيانات وفي انتظار الرد عليها من الادارة"),
          ),
          Positioned(
            top: 40,
            left: 20,
            child: InkWell(
              onTap: () async {
                _dialog.showOptionDialog(
                    context: context,
                    msg: 'تسجيل الخروج ؟',
                    okFun: () async {
                      await Provider.of<LogOutProvider>(context, listen: false)
                          .logOut(
                              _deviceToken,
                              context,
                              Provider.of<SharedPref>(context, listen: false)
                                  .token);
                      await _prefs.clear();
                      Navigator.pop(context, true);
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Splash(),
                        ),
                        (Route<dynamic> route) => false,
                      );
                    },
                    okMsg: 'نعم',
                    cancelMsg: 'لا',
                    cancelFun: () {
                      return;
                    });
              },
              child: Row(
                children: <Widget>[
                  Directionality(
                    textDirection: TextDirection.ltr,
                    child: IconButton(
                      onPressed: () {
                        _dialog.showOptionDialog(
                            context: context,
                            msg: 'تسجيل الخروج ؟',
                            okFun: () async {
                              await Provider.of<LogOutProvider>(context,
                                      listen: false)
                                  .logOut(
                                      _deviceToken,
                                      context,
                                      Provider.of<SharedPref>(context,
                                              listen: false)
                                          .token);
                              await _prefs.clear();
                              Navigator.pop(context, true);
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Splash(),
                                ),
                                (Route<dynamic> route) => false,
                              );
                            },
                            okMsg: 'نعم',
                            cancelMsg: 'لا',
                            cancelFun: () {
                              return;
                            });
                      },
                      icon: Icon(
                        FontAwesomeIcons.signOutAlt,
                        color: Colors.black87,
                      ),
                    ),
                  ),
                  Text("تسجيل خروج"),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
