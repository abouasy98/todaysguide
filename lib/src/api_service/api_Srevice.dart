import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:todaysguide/src/Models/post/fetchDataModel.dart';

import 'package:todaysguide/src/Repository/networkUtlis.dart';

class APIServices {
  Future<List<DataListView>> getProducts({
    int pageNumber,
    BuildContext context,
    int categoryId,
    String name,
    int shopId,
  }) async {
    FetchDataModel data;

    NetworkUtil _utils = new NetworkUtil();

    FormData formData = FormData.fromMap({
      "shop_id": shopId,
      "category_id": categoryId,
      "page": pageNumber,
      "name": name
    });
    var response = await _utils.post(
        name != null ? "search-product" : "products-fillters", context,
        body: formData);
    List<DataListView> data2;
    if (response.statusCode == 200) {
      //   print(response.data);
      data = FetchDataModel.fromJson(response.data);
      data2 = data.data.data
          .map((i) => DataListView(
              available: i.available,
              category: i.category,
              id: i.id,
              categoryId: i.categoryId,
              createdAt: i.createdAt,
              details: i.details,
              name: i.name,
              photos: i.photos,
              price: i.price,
              shop: i.shop,
              shopId: i.shopId))
          .toList();

      return data2;
    } else {
      //  data = FetchDataModel.fromJson(response.data);
      print(response.data);
      return data2;
    }
  }
}
