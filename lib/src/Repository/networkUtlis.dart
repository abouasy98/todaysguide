import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Helpers/Widgets/Connection/check_connection_screen.dart';
import 'package:todaysguide/src/screens/Intro/splash_screen.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';

class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  Dio dio = Dio();

  Future<Response> get(String url, BuildContext context, {Map headers}) async {
    var response;
    try {
      dio.options.baseUrl = "https://dalel-alyoom.com/api/v1/";
      response = await dio.get(url, options: Options(headers: headers));
    } on DioError catch (e) {
      if (e.response != null) {
        response = e.response;
        print("response: " + e.response.toString());
      } else {}
    }
    if (response == null) {
      final result = await Connectivity().checkConnectivity();
      if (result == ConnectivityResult.none) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (_) => CheckConnectionScreen(
                      state: false,
                    )),
            (Route<dynamic> route) => false);
      } else {
        CustomAlert().toast(
            context: context, title: "هناك خطا يرجي اعادة المحاولة لاحقا");
      }
    }
    print("object >>>> $response" );
    return response == null ? null : handleResponse(response, context);
  }

  Future<Response> post(String url, BuildContext context,
      {Map headers, FormData body, encoding}) async {
    var response;
    dio.options.baseUrl = "https://dalel-alyoom.com/api/v1/";
    try {
      response = await dio.post(url,
          data: body,
          options: Options(headers: headers, requestEncoder: encoding));
    } on DioError catch (e) {
      if (e.response != null) {
        response = e.response;
        print("response: " + e.response.toString());
      } else {}
    }
    if (response == null) {
      final result = await Connectivity().checkConnectivity();
      if (result == ConnectivityResult.none) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (_) => CheckConnectionScreen(
                      state: false,
                    )),
            (Route<dynamic> route) => false);
      } else {
        CustomAlert().toast(
            context: context, title: "هناك خطا يرجي اعادة المحاولة لاحقا");
      }
    } else {}
    return response == null ? null : handleResponse(response, context);
  }

  Response handleResponse(Response response, BuildContext context) {
    final int statusCode = response.statusCode;
    print("response: " + response.toString());
    Future.delayed(Duration(milliseconds: 1), () async {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      if (response.data['error'] != null) {
        print('ssdsdsdsd yup');
        if (response.data['error'][0]['key'] == 'token'&&_prefs.getString('token')!=null) {
          Fluttertoast.showToast(
              msg:
                  'انتهت جلسة التسجيل الخاصة بكم, يرجى إعادة التسجيل مرة أخرى');
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (ctx) => Splash(),
              ),
              (route) => false);
          await _prefs.clear().then((value) => print('done'));
          return;
        }
      }
    });
    if (statusCode >= 200 && statusCode < 300) {
      return response;
    } else {
      return response;
    }
  }
}
