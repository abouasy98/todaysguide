import 'dart:math';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/Models/post/postServiceModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class PostServicesProvider with ChangeNotifier {
  List<Datum> _myServices = [];

  List<Datum> get myServices {
    return [..._myServices];
  }

  NetworkUtil _utils = new NetworkUtil();
  PostServiceModel driverOrdersModel;
  Future<PostServiceModel> getMyServices(
      String token, int filterId, BuildContext context) async {
    final List<Datum> loadedOrders = [];

    Map<String, String> headers = {"Authorization": "Bearer $token"};
    FormData formData = FormData.fromMap({
      "service_category_id": 0,
      "latitude":
          Provider.of<MapHelper>(context, listen: false).position.latitude !=
                  null
              ? Provider.of<MapHelper>(context, listen: false).position.latitude
              : 30.7981684,
      "longitude": Provider.of<MapHelper>(context, listen: false)
                  .position
                  .longitude !=
              null
          ? Provider.of<MapHelper>(context, listen: false).position.longitude
          : 31.0067622
    });
    Response response = await _utils.post("services", context,
        headers: headers, body: formData);
    if (response.statusCode == 200) {
      print("get my-services sucsseful");

      driverOrdersModel = PostServiceModel.fromJson(response.data);
      driverOrdersModel.data.forEach((e) {
        if (filterId == null) {
          loadedOrders.add(Datum(
              id: e.id,
              details: e.details,
              name: e.name,
              phoneNumber: e.phoneNumber,
              photo: e.photo,
              serviceCategory: e.serviceCategory,
              serviceCategoryId: e.serviceCategoryId,
              state: e.state,
              createdAt: e.createdAt,
              latitude: e.latitude,
              longitude: e.longitude,
              user: e.user,
              userId: e.userId,
              distance: calculateDistance(
                  Provider.of<MapHelper>(context, listen: false).position !=
                          null
                      ? Provider.of<MapHelper>(context, listen: false)
                          .position
                          .latitude
                      : 30.7982574,
                  Provider.of<MapHelper>(context, listen: false).position !=
                          null
                      ? Provider.of<MapHelper>(context, listen: false)
                          .position
                          .longitude
                      : 31.0066411,
                  double.parse(e.latitude),
                  double.parse(e.longitude))));
        } else if (filterId == e.serviceCategoryId) {
          loadedOrders.add(Datum(
              id: e.id,
              details: e.details,
              name: e.name,
              phoneNumber: e.phoneNumber,
              photo: e.photo,
              serviceCategory: e.serviceCategory,
              serviceCategoryId: e.serviceCategoryId,
              state: e.state,
              createdAt: e.createdAt,
              latitude: e.latitude,
              longitude: e.longitude,
              user: e.user,
              userId: e.userId,
              distance: calculateDistance(
                     Provider.of<MapHelper>(context, listen: false).position !=
                          null
                      ? Provider.of<MapHelper>(context, listen: false)
                          .position
                          .latitude
                      : 30.7982574,
                  Provider.of<MapHelper>(context, listen: false).position !=
                          null
                      ? Provider.of<MapHelper>(context, listen: false)
                          .position
                          .longitude
                      : 31.0066411,
                  double.parse(e.latitude),
                  double.parse(e.longitude))));
        }
      });
      _myServices = loadedOrders.reversed.toList();
      notifyListeners();
      return PostServiceModel.fromJson(response.data);
    } else {
      print("error get my-services data");
      driverOrdersModel = PostServiceModel.fromJson(response.data);

      _myServices = loadedOrders.reversed.toList();
      notifyListeners();
      return PostServiceModel.fromJson(response.data);
    }
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a)) ?? 0;
  }
}
