import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/Models/CreateServiceModle.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_progress_dialog.dart';

class EditServiceProvider with ChangeNotifier {
  String details;
  String placeName;
  String phoneNumber;
  String serviceCategoryId;
  String longitude;
  String latitude;

  File photo;
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;

  CreateServiceModel _model;
  setNull() {
    details = null;
    photo = null;
    placeName = null;
    phoneNumber = null;
    serviceCategoryId = null;
    longitude = null;
    latitude = null;

    notifyListeners();
  }

  createService(String token, int id, BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    FormData formData = FormData.fromMap({
      "name": placeName,
      "details": details,
      "service_category_id": serviceCategoryId,
      "longitude": Provider.of<MapHelper>(context,listen: false).position.longitude!=null?Provider.of<MapHelper>(context,listen: false).position.longitude:31.0067622,
      "latitude": Provider.of<MapHelper>(context,listen: false).position.latitude!=null?Provider.of<MapHelper>(context,listen: false).position.latitude:30.7981684,
      "photo": photo == null ? null : await MultipartFile.fromFile(photo.path),
      "phone_number": phoneNumber,
    });

    Response response = await _utils.post("edit-service/$id", context,
        body: formData, headers: headers);

    if (response.statusCode == 200) {
      print("edit-service sucsseful");

      _model = CreateServiceModel.fromJson(response.data);
    } else {
      print("error edit-service");
      _model = CreateServiceModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        CustomAlert().toast(context: context, title: "تم تعديل الخدمة بنجاح");
        Navigator.pop(context,true);
      });
      setNull();
      notifyListeners();
    } else {
      print('error edit-service');
      _model = CreateServiceModel.fromJson(response.data);
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        CustomAlert().toast(context: context, title: _model.error[0].value);
      });
    }
    notifyListeners();
  }
}
