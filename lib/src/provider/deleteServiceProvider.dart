import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:todaysguide/src/Models/deleteServiceModle.dart';
// import 'package:todaysguide/src/Models/deleteNotificationModle.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';

class DeleteServiceProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  DeleteServiceModel deleteNotificationModel;
  CustomDialog dialog = CustomDialog();

  deletNot(String token, int id, BuildContext context) async {
    Map<String, String> headers = {
      'Authorization': 'Bearer $token',
    };
    FormData formData = FormData.fromMap({});

    Response response = await _utils.post("delete-service/$id", context,
        body: formData, headers: headers);
    if (response.statusCode == 200) {
      print("add delete-service sucsseful");
      deleteNotificationModel = DeleteServiceModel.fromJson(response.data);
    } else {
      print("error delete-service data");
      deleteNotificationModel = DeleteServiceModel.fromJson(response.data);
    }
    if (deleteNotificationModel.code == 200) {
      print("done");
      Fluttertoast.showToast(
          msg: "حذف",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 1,
          fontSize: 16.0);

      return true;
    } else {
      print("done");
      print('error delete-service');
      dialog.showErrorDialog(
        btnOnPress: () {},
        context: context,
        msg: "هناك خطا اعد المحاولة",
        ok: "موافق",
      );
    }
  }
}
