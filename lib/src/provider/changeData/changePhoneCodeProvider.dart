import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Models/auth/phoneVerificationModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_progress_dialog.dart';

class ChangePhoneCodeProvider with ChangeNotifier {
  String phone;
  String code;

  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  PhoneVerificationModel _model;
  SharedPreferences _prefs;
  changePhoneCode(String token, BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      //     "X-localization": localization.currentLanguage.toString(),
      "Authorization": "Bearer $token"
    };
    FormData formData = FormData.fromMap({
      "phone_number": phone,
      "code": code,
      "country_code":966,
    });

    Response response = await _utils.post("check-code-change-phone",context,
        body: formData, headers: headers);
    // if (response == null) {
    //   Future.delayed(Duration(seconds: 1), () {
    //     customProgressDialog.hidePr();
    //     print('error check-code-change-phone');
    //     dialog.showWarningDialog(
    //       btnOnPress: () {},
    //       context: context,
    //       msg: "من فضلك تأكد من وجود إتصال بالإنترنت",
    //     );
    //   });

    //   return;
    // }
    if (response.statusCode == 200) {
      print("check-code-change-phone sucsseful");
      _model = PhoneVerificationModel.fromJson(response.data);
    } else {
      print("error check-code-change-phone");
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      _model = PhoneVerificationModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      _prefs = await SharedPreferences.getInstance();
      print('success check-code-change-phone');
      _prefs.setString("phone", phone);

      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showSuccessDialog(
          btnOnPress: () {
            Navigator.pop(context);
            Navigator.pop(context);
          },
          context: context,
          msg: "تم تغير رقم الهاتف بنجاح",
          btnMsg: "موافق",
        );
      });
    } else {
      print('error check-code-change-phone');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {},
          context: context,
          msg: _model.error[0].value,
          ok: "موافق",
        );
      });
    }
    notifyListeners();
  }
}
