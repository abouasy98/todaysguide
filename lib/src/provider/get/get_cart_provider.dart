import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Models/post/empty_cart_model.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:dio/dio.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_progress_dialog.dart';
import './../../models/get/get_cart_model.dart';

class GetCartProvider with ChangeNotifier {
  List<Carts> _carts = [];

  List<Carts> get carts {
    return [..._carts];
  }

  CustomDialog _dialog = CustomDialog();
  CustomProgressDialog _customProgressDialog;
  EmptyCartModel _emptyModel;
  EmptyCartModel deleteAdModel;
  NetworkUtil _util = NetworkUtil();
  GetCartModel _model;
  ProgressDialog _pr;
  List<Datum> myCartItems;
  Future<void> emptyCart(
    BuildContext context,
  ) async {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    _pr = ProgressDialog(context);
    _customProgressDialog = CustomProgressDialog(
      context: context,
      pr: _pr,
    );
    String apiToken = Provider.of<SharedPref>(context, listen: false).token;
    Map<String, dynamic> headers = {
      HttpHeaders.authorizationHeader: 'Bearer $apiToken',
    };
    _customProgressDialog.showProgressDialog();
    _customProgressDialog.showPr();
    Response response = await _util.get(
      'delete-all',
      context,
      headers: headers,
    );
    if (response == null) {
      await _customProgressDialog.hidePr();
      await _dialog.showErrorDialog(
          msg: 'يرجى التحقق من الاتصال بالانترنت',
          context: context,
          ok: 'موافق',
          btnOnPress: () {});
      return;
    }
    if (response.statusCode == 200) {
      _emptyModel = EmptyCartModel.fromJson(response.data);

      //   await Provider.of<GetCartProvider>(context, listen: false).zeroItem();
      carts.clear();
      _model = null;

      notifyListeners();
      await _customProgressDialog.hidePr();
      await _dialog.showSuccessDialog(
        btnMsg: 'موافق',
        btnOnPress: () {},
        context: context,
        msg: _emptyModel.data.value,
      );
      // if (!fromOrderCart) {
      //   Navigator.of(context).pushReplacement(
      //     MaterialPageRoute(
      //       builder: (ctx) => CartDetails(),
      //     ),
      //   );
      // }
      //  notifyListeners();
    } else {
      _emptyModel = EmptyCartModel.fromJson(response.data);
      notifyListeners();
      await _customProgressDialog.hidePr();
      await _dialog.showErrorDialog(
        msg: _model.error[0].value,
        btnOnPress: () {},
        ok: 'موافق',
        context: context,
      );
    }
  }

  clear() {
    _model = null;
    notifyListeners();
  }

  deleteCart(BuildContext context, int id, String token) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response =
        await _util.get("delete/$id", context, headers: headers);

    final exitingDataIndex = _carts.indexWhere((prod) => prod.id == id);

    _carts.removeAt(exitingDataIndex);
    print(_carts.length);
    if (response.statusCode == 200) {
      notifyListeners();
      deleteAdModel = EmptyCartModel.fromJson(response.data);
    } else {
      //   _carts.insert(exitingDataIndex, exitingData);

      print("error get remove-shop-favorite data");
      deleteAdModel = EmptyCartModel.fromJson(response.data);
    }
    if (deleteAdModel.code == 200) {
      CustomAlert().toast(
        context: context,
        title: deleteAdModel.data.value,
      );
    } else {
      print('error remove_shop_favrouit');
      CustomAlert().toast(
        context: context,
        title: deleteAdModel.error[0].value,
      );
    }
    notifyListeners();
  }

  Future<GetCartModel> getCart(BuildContext context) async {
    final List<Carts> cartsList = [];
    //  SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = Provider.of<SharedPref>(context, listen: false).token;
    Map<String, String> headers = {"Authorization": "Bearer $apiToken "};
    Response response = await _util.get(
      'get-cart',
      context,
      headers: headers,
    );
    if (response.statusCode == 200) {
      _model = GetCartModel.fromJson(response.data);
      //  myCartItems = _model.data;
      _model.data.forEach((e) {
        cartsList.add(Carts(
          createdAt: e.createdAt,
          id: e.id,
          orderId: e.orderId,
          photos: e.photos,
          price: e.price,
          productId: e.productId,
          productName: e.productName,
          quantity: e.quantity,
          shopId: e.shopId,
          shopName: e.shopName,
          state: e.state,
          userId: e.userId,
          userName: e.userName,
        ));
      });
      _carts = cartsList.reversed.toList();
      notifyListeners();
      return _model;
    } else {
      // _cartItemCount = 0;
      _model = GetCartModel.fromJson(response.data);

      _carts = cartsList.reversed.toList();
      notifyListeners();
      return GetCartModel.fromJson(response.data);
    }
  }
}

class Carts {
  Carts({
    @required this.id,
    @required this.createdAt,
    this.orderId,
    this.photos,
    this.price,
    this.productId,
    this.productName,
    this.quantity,
    this.shopId,
    this.shopName,
    this.state,
    this.userId,
    this.userName,
  });

  int id;
  int productId;
  String productName;
  List<Photo> photos;
  int userId;
  String userName;
  int shopId;
  String shopName;
  dynamic orderId;
  String price;
  int quantity;
  String state;
  DateTime createdAt;
}
