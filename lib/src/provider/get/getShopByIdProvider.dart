import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/get/ShopByIdModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class ShopsByIdProvider with ChangeNotifier {
  List<ShopsById> _shopsById = [];

  List<ShopsById> get shopsById {
    return [..._shopsById];
  }

  NetworkUtil _utils = new NetworkUtil();
  ShopByIdModel restourant;
  Future<ShopByIdModel> getShops(int id,String token,BuildContext context) async {
    final List<ShopsById> loaded = [];
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Future.delayed(Duration(microseconds: 150), () {
      restourant = null;
      notifyListeners();
    });
    Response response = await _utils.get("get-shop-by-id/$id",context,headers: headers);

    if (response.statusCode == 200) {
      print("get shop By Id sucsseful");

      restourant = ShopByIdModel.fromJson(response.data);

      _shopsById.forEach((e) {
        loaded.add(ShopsById(
            id: e.id,
            name: e.name,
            photo: e.photo,
            department: e.department,
            departmentId: e.departmentId,
            viewCount: e.viewCount,
            latitude: e.latitude,
            longitude: e.longitude,
            active: e.active,
            address: e.address,
            city: e.city,
            cityId: e.cityId,
            commissionStatus: e.commissionStatus,
            countryCode: e.countryCode,
            favorite: e.favorite,
            phoneNumber: e.phoneNumber,
            productNumber: e.productNumber,
            region: e.region,
            regionId: e.regionId,
            type: e.type,
            photos: e.photos));
      });
      _shopsById = loaded.toList();
      notifyListeners();
      return ShopByIdModel.fromJson(response.data);
    } else {
      restourant = ShopByIdModel.fromJson(response.data);
      _shopsById = loaded.toList();

      notifyListeners();

      print("error get shop By Id  data");
      return ShopByIdModel.fromJson(response.data);
    }
  }
}

class ShopsById {
  ShopsById(
      {@required this.id,
      @required this.name,
      @required this.longitude,
      @required this.latitude,
      @required this.departmentId,
      @required this.department,
      @required this.photo,
      this.productNumber,
      this.viewCount,
      this.active,
      @required this.photos,
      this.address,
      this.city,
      this.cityId,
      this.commissionStatus,
      this.countryCode,
      this.favorite,
      this.phoneNumber,
      this.region,
      this.regionId,
      this.type});

  int id;
  String name;
  String phoneNumber;
  int countryCode;
  int productNumber;
  String photo;
  List<Photo> photos;
  int active;
  int type;
  String longitude;
  String latitude;
  String address;
  int viewCount;
  int cityId;
  String city;
  int regionId;
  String region;
  int departmentId;
  String department;
  int commissionStatus;
  int favorite;
}
