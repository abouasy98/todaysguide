import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/get/getOrderByIdModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class GetOrderByIdProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  List<ProductsCart> _orderById = [];

  List<ProductsCart> get orderById {
    return [..._orderById];
  }

  GetOrderByIdModel getOrderByIdModel;
  Future<GetOrderByIdModel> getOrderById(
      BuildContext context, int id, String token) async {
    final List<ProductsCart> loaded = [];
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response =
        await _utils.get("get-order-by-id/$id", context, headers: headers);
    if (response.statusCode == 200) {
      print("get get-order-by-id sucsseful");
      getOrderByIdModel = GetOrderByIdModel.fromJson(response.data);
      _orderById.forEach((e) {
        loaded.add(ProductsCart(
          createdAt: e.createdAt,
          id: e.id,
          orderId: e.orderId,
          photos: e.photos,
          price: e.price,
          productId: e.productId,
          productName: e.productName,
          quantity: e.quantity,
          shopId: e.shopId,
          shopName: e.shopName,
          state: e.state,
          userId: e.userId,
          userName: e.userName,
        ));
      });
      _orderById = loaded.toList();
      return getOrderByIdModel;
    } else {
      getOrderByIdModel = GetOrderByIdModel.fromJson(response.data);
      _orderById = loaded.toList();
      print("error get-order-by-id");
      return getOrderByIdModel;
    }
  }
}
