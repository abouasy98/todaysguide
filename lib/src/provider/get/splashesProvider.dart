
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/get/splashesModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class SplashesProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<SplashesModel> getSplashes(BuildContext context) async {
    Map<String, String> headers = {};
    Response response = await _utils.get("splashes", context,headers: headers);
    if (response.statusCode == 200) {
      print("get get Splashes data sucsseful");
      return SplashesModel.fromJson(response.data);
    } else {
      print("error get Splashes News data");
      return SplashesModel.fromJson(response.data);
    }
  }
}
