import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/post/deletPlaceModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';

class DeletePlaceProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  DeletePlaceDataModel deletePlaceModel;
  CustomDialog dialog = CustomDialog();

  deletePlace(
    String token,
    String id,
    BuildContext context,
  ) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};

    Response response = await _utils.post("delete-place/$id", context,headers: headers);
    // if (response == null) {
    //   print('error rate');
    //   dialog.showWarningDialog(
    //     btnOnPress: () {},
    //     context: context,
    //     msg:"من فضلك تأكد من وجود إتصال بالإنترنت",
    //   );
    // }
    if (response.statusCode == 200) {
      print("get delete-place sucsseful");
      deletePlaceModel = DeletePlaceDataModel.fromJson(response.data);
    } else {
      print("error get delete-place data");
      deletePlaceModel = DeletePlaceDataModel.fromJson(response.data);
    }
    if (deletePlaceModel.code == 200) {
      CustomAlert().toast(
        context: context,
        title: "تم حذف المكان",
      );
      // Fluttertoast.showToast(
      //     msg: "تم حذف المكان",
      //     toastLength: Toast.LENGTH_LONG,
      //     timeInSecForIosWeb: 1,
      //     fontSize: 16.0);
      return true;
    } else {
      print('error confirmed');
      CustomAlert().toast(
        context: context,
        title: deletePlaceModel.error[0].value,
      );
      // Fluttertoast.showToast(
      //     msg: localization.text("error"),
      //     toastLength: Toast.LENGTH_LONG,
      //     timeInSecForIosWeb: 1,
      //     fontSize: 16.0);
    }
    notifyListeners();
  }
}
