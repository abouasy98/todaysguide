import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Models/get/getUserDataModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class GetUserDataProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  GetUserDataModel userDataModel;
  Future<GetUserDataModel> getUserData(
      String token, BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response =
        await _utils.get("user-data", context, headers: headers);
    if (response.statusCode == 200) {
      print("get user-data sucsseful");

      userDataModel = GetUserDataModel.fromJson(response.data);
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      //      "id": 100,
      // "name": "Driver1",
      // "phone_number": "1129780969",
      // "country_code": 20,
      // "photo": "https://dalel-alyoom.com/uploads/users/photo_1609207531.png",
      // "active": 1,
      // "type": 2,
      // "car_type": 1,
      // "city_id": 11,
      // "city": "المجمعة",
      // "region_id": 6,
      // "region": "الرياض",
      // "identity": "https://dalel-alyoom.com/uploads/drivers/document/identity/photo_1609207531.png",
      // "license": "https://dalel-alyoom.com/uploads/drivers/document/license/photo_1609207531.png",
      // "car_form": "https://dalel-alyoom.com/uploads/drivers/document/car_form/photo_1609207531.png",
      // "commission_status": 0,
      // "driver_price": null,
      // "api_token": "10000NkcU31rdpe",
      // "created_at": "2020-12-29"
      print('done');
      _prefs.setInt('id', userDataModel.data.id);
      _prefs.setString('name', userDataModel.data.name);
      _prefs.setString('phone', userDataModel.data.phoneNumber);
      _prefs.setInt('countrycode', userDataModel.data.countryCode);
      _prefs.setString('photo', userDataModel.data.photo);
      _prefs.setInt('active', userDataModel.data.active);
      _prefs.setInt('type', userDataModel.data.type);
      _prefs.setString('city', userDataModel.data.city);
      _prefs.setInt('cityId', userDataModel.data.cityId);
      _prefs.setString('driver_price', userDataModel.data.driverPrice);
      _prefs.setInt('regionId', userDataModel.data.regionId);
      _prefs.setInt('commission_status', userDataModel.data.commissionStatus);
      _prefs.setString('token', userDataModel.data.apiToken);
      _prefs.setString('identity', userDataModel.data.identity);
      _prefs.setString('license', userDataModel.data.license);
      _prefs.setString('car_form', userDataModel.data.carForm);
      _prefs.setString('region', userDataModel.data.region);
      _prefs.setString('token', userDataModel.data.apiToken);
      _prefs.setInt('department_id', userDataModel.data.departmentId);
      _prefs.setInt('cartype', userDataModel.data.carType);
      _prefs.setInt('products_number', userDataModel.data.productsNumber);
      _prefs.setInt('viewcount', userDataModel.data.viewCount);
      _prefs.setString('address', userDataModel.data.address);
      _prefs.setString('department', userDataModel.data.department);
      _prefs.setString('latitude', userDataModel.data.latitude);
      _prefs.setString('longitude', userDataModel.data.longitude);
      _prefs.setString('department', userDataModel.data.department);
      _prefs.setInt('region_id', userDataModel.data.regionId);
    _prefs.setString('workTimeStart', userDataModel.data.openTime);
      _prefs.setString('workTimeEnd', userDataModel.data.closeTime);
      notifyListeners();
      return GetUserDataModel.fromJson(response.data);
    } else {
      print("error get user-data data");
      return GetUserDataModel.fromJson(response.data);
    }
  }
}
