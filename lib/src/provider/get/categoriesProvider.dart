import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/get/CategoriesModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';


class CategoriesProvider with ChangeNotifier {
  List<Categories> _categories = [];

  List<Categories> get categories {
    return [..._categories];
  }

  NetworkUtil _utils = new NetworkUtil();
  CategoriesModel categoriesModel;
  Future<CategoriesModel> getcategories(BuildContext context) async {
    final List<Categories> loadedCountries = [];

    Map<String, String> headers = {};
    Response response = await _utils.get("categories",context, headers: headers);
    if (response.statusCode == 200) {
      print("get categories data sucsseful");
         categoriesModel =  CategoriesModel.fromJson(response.data);
      categoriesModel.data.forEach((e) {
        loadedCountries.add(Categories(
            createdAt: e.createdAt, id: e.id, name: e.name, selected: false));
      });

      _categories = loadedCountries.reversed.toList();
      notifyListeners();
      return CategoriesModel.fromJson(response.data);
    } else {
      print("error get categories data");
      return CategoriesModel.fromJson(response.data);
    }
  }
}


class Categories {
  Categories(
      {@required this.id,
      @required this.name,
      @required this.createdAt,
      this.selected=false});

  int id;
  String name;

  bool selected;
  DateTime createdAt;
}
