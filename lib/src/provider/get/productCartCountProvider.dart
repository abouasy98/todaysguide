import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/get/productCartCountModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class ProductCartCountProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  ProductCartCountModel reportShopModel;

  Future<ProductCartCountModel> productCartCount(
 String token, BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};

 
    Response response =
        await _utils.get("product-cart-count", context, headers: headers);
    if (response.statusCode == 200) {
      print("get report-shop sucsseful");

      reportShopModel = ProductCartCountModel.fromJson(response.data);

      notifyListeners();
      return reportShopModel;
    } else {
      print("error post report-shop data");
      return ProductCartCountModel.fromJson(response.data);
    }
  }
}
