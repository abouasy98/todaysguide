import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:todaysguide/src/Models/get/ClearBagModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';

class DeleteBagOrderProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  CleanBagModel clearBagModel;
  CustomDialog dialog = CustomDialog();

  deleteOrder(String token,String id, BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};

    Response response = await _utils.get("delete/$id",context, headers: headers);
    // if (response == null) {
    //   print('error delete');
    //   dialog.showWarningDialog(
    //     btnOnPress: () {},
    //     context: context,
    //     msg:"من فضلك تأكد من وجود إتصال بالإنترنت",
    //   );
    // }
    if (response.statusCode == 200) {
      print("get delete sucsseful");
      clearBagModel = CleanBagModel.fromJson(response.data);
    } else {
      print("error get delete data");
      clearBagModel = CleanBagModel.fromJson(response.data);
    }
    if (clearBagModel.code == 200) {
      Fluttertoast.showToast(
          msg: "تم الحذف من السلة",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 1,
          fontSize: 16.0);
          return true;
    } else {
      print('error confirmed');
      Fluttertoast.showToast(
          msg: clearBagModel.error[0].value,
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1,
          fontSize: 16.0,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.black,
          textColor: Colors.white
          
          );
    }
    notifyListeners();
  }
}
