import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/get/productByIdModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class ProductsByIdProvider with ChangeNotifier {
  List<ProductsById> _productsById = [];

  List<ProductsById> get productsById {
    return [..._productsById];
  }

  NetworkUtil _utils = new NetworkUtil();
  ProductByIdModel restourant;
  Future<ProductByIdModel> getShops(int id, String token,BuildContext context) async {
    final List<ProductsById> loaded = [];
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Future.delayed(Duration(microseconds: 150), () {
      restourant = null;
      notifyListeners();
    });
    Response response = await _utils.get("product-by-id/$id", context,headers: headers);

    if (response.statusCode == 200) {
      print("get products  By Id sucsseful");

      restourant = ProductByIdModel.fromJson(response.data);

      _productsById.forEach((e) {
        loaded.add(ProductsById(
            id: e.id,
            name: e.name,
            available: e.available,
            category: e.category,
            categoryId: e.categoryId,
            price: e.price,
            shop: e.shop,
            shopId: e.shopId,
            phoneNumber: e.phoneNumber,
            photos: e.photos));
      });
      _productsById = loaded.toList();
      notifyListeners();
      return ProductByIdModel.fromJson(response.data);
    } else {
      restourant = ProductByIdModel.fromJson(response.data);
      _productsById = loaded.toList();

      notifyListeners();

      print("error get products By Id  data");
      return ProductByIdModel.fromJson(response.data);
    }
  }
}

class ProductsById {
  ProductsById({
    @required this.id,
    @required this.name,
    @required this.photos,
    this.available,
    this.category,
    this.categoryId,
    this.phoneNumber,
    this.price,
    this.shop,
    this.shopId,
  });

  int id;
  String name;
  String phoneNumber;
  List<Photo> photos;
  String price;
  int available;
  int shopId;
  String shop;
  int categoryId;
  String category;
}
