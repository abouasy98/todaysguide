import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/get/notification.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class NotoficationProvider with ChangeNotifier {
  List<NotificationItem> _notifications = [];

  List<NotificationItem> get notfications {
    return [..._notifications];
  }

  NetworkUtil _utils = new NetworkUtil();
  NotificationModel notificationModel;
  bool error = false;
  void removeItem(int productId) {
    _notifications.removeAt(productId);
    print(_notifications.length);
    notifyListeners();
  }

  Future<NotificationModel> getNotification(
      String token, BuildContext context) async {
    final List<NotificationItem> loadedNotifications = [];
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response =
        await _utils.get("list-notifications", context, headers: headers);
    if (response.statusCode == 200) {
      print("get notification data sucsseful");
      notificationModel = NotificationModel.fromJson(response.data);
      notificationModel.data.forEach((element) {
        loadedNotifications.add(NotificationItem(
          id: element.id,
          type: element.type,
          message: element.message,
          offerId: element.offerId,
          orderId: element.orderId,
          paymentId: element.paymentId,
          title: element.title,
          createdAt: element.createdAt,
        ));
      });
      _notifications = loadedNotifications.reversed.toList();
      error = false;
      notifyListeners();
      return NotificationModel.fromJson(response.data);
    } else {
      print("error get notification data");
      return NotificationModel.fromJson(response.data);
    }
  }
}
