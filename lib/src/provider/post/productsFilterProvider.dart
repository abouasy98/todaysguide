import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/post/productsFilterModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class ProductsFilterProvider with ChangeNotifier {
  List<Products> _productsFilter = [];

  List<Products> get productsFilter {
    return [..._productsFilter];
  }

  NetworkUtil _utils = new NetworkUtil();
  ProductsFillter products;
  Future<ProductsFillter> getProductsFillter({
    int shopId,
    int categoryId,
    BuildContext context
  }) async {
    final List<Products> loaded = [];
    Future.delayed(Duration(microseconds: 150), () {
      products = null;
      notifyListeners();
    });

    FormData formData =
        FormData.fromMap({"shop_id": shopId, "category_id": categoryId});

    Response response =
        await _utils.post("products-fillter", context,body: formData);

    if (response.statusCode == 200) {
      print("get products-fillter sucsseful");

      products = ProductsFillter.fromJson(response.data);

      products.data.forEach((e) {
        loaded.add(Products(
            id: e.id,
            name: e.name,
            available: e.available,
            category: e.category,
            categoryId: e.categoryId,
            details: e.details,
            price: e.price,
            shop: e.shop,
            shopId: e.shopId,
            photos: e.photos));
      });
      _productsFilter = loaded.toList();
      notifyListeners();
      return ProductsFillter.fromJson(response.data);
    } else {
      products = ProductsFillter.fromJson(response.data);
      _productsFilter = loaded.toList();

      notifyListeners();

      print("error get products-fillter data");
      return ProductsFillter.fromJson(response.data);
    }
  }
}

class Products {
  Products(
      {@required this.id,
      @required this.name,
      @required this.photos,
      this.available,
      this.category,
      this.categoryId,
      this.details,
      this.price,
      this.shop,
      this.shopId});

  int id;
  String name;
  String details;
  List<Photo> photos;
  String price;
  int available;
  int shopId;
  String shop;
  int categoryId;
  String category;
}
