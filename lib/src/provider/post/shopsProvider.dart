import 'dart:math';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/Models/post/shopsModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class ShopsProvider with ChangeNotifier {
  List<Shops> _shops = [];

  List<Shops> get shops {
    return [..._shops];
  }

  NetworkUtil _utils = new NetworkUtil();
  ShopsModel restourant;
  Future<ShopsModel> getShops(
      int id, String search, BuildContext context) async {
    final List<Shops> loaded = [];
    Future.delayed(Duration(microseconds: 150), () {
      restourant = null;
      notifyListeners();
    });

    Map<String, String> headers = {};
    FormData formData = FormData.fromMap({
      "latitude":
          Provider.of<MapHelper>(context, listen: false).position != null
              ? Provider.of<MapHelper>(context, listen: false).position.latitude
              : 24.418915604601022,
      "longitude": Provider.of<MapHelper>(context, listen: false).position !=
              null
          ? Provider.of<MapHelper>(context, listen: false).position.longitude
          : 39.646886250038214,
      "department_id": id
    });

    Response response =
        await _utils.post("shops", context, body: formData, headers: headers);

    if (response.statusCode == 200) {
      print("get shops sucsseful");

      restourant = ShopsModel.fromJson(response.data);
      if (search != null)
        restourant.data.forEach((e) {
          if (e.name.contains(search)) {
            loaded.add(Shops(
                id: e.id,
                name: e.name,
                photo: e.photo,
                department: e.department,
                departmentId: e.departmentId,
                distance: calculateDistance(
                        Provider.of<MapHelper>(context, listen: false)
                                    .position !=
                                null
                            ? Provider.of<MapHelper>(context, listen: false)
                                .position
                                .latitude
                            : 24.418915604601022,
                        Provider.of<MapHelper>(context, listen: false)
                                    .position !=
                                null
                            ? Provider.of<MapHelper>(context, listen: false)
                                .position
                                .longitude
                            : 39.646886250038214,
                        double.parse(e.latitude),
                        double.parse(e.longitude))
                    .round(),
                latitude: e.latitude,
                longitude: e.longitude,
                photos: e.photos));
          }
        });
      else
        restourant.data.forEach((e) {
          loaded.add(Shops(
              id: e.id,
              name: e.name,
              photo: e.photo,
              department: e.department,
              departmentId: e.departmentId,
              viewCount: e.viewCount,
              distance: calculateDistance(
                      Provider.of<MapHelper>(context, listen: false).position !=
                              null
                          ? Provider.of<MapHelper>(context, listen: false)
                              .position
                              .latitude
                          : 24.48250235674493,
                      Provider.of<MapHelper>(context, listen: false).position !=
                              null
                          ? Provider.of<MapHelper>(context, listen: false)
                              .position
                              .longitude
                          :  39.63270100774933,
                      double.parse(e.latitude),
                      double.parse(e.longitude))
                  .round(),
              latitude: e.latitude,
              open: e.open,
              productsNumber: e.productsNumber,
              longitude: e.longitude,
              photos: e.photos));
        });
      _shops = loaded.toList();
      notifyListeners();
      return ShopsModel.fromJson(response.data);
    } else {
      restourant = ShopsModel.fromJson(response.data);
      _shops = loaded.toList();

      notifyListeners();

      print("error get shops data");
      return ShopsModel.fromJson(response.data);
    }
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a)) ?? 0;
  }
}
