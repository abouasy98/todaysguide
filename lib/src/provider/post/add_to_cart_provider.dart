import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/sharedPref_helper.dart';
import 'package:todaysguide/src/Models/post/addToCartModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:dio/dio.dart';

enum CartOptions {
  Add,
  Edit,
}

class AddToCartProvider with ChangeNotifier {
  NetworkUtil _util = NetworkUtil();
  CustomDialog _dialog = CustomDialog();
  AddToCartModel _model;
  //CustomProgressDialog _customProgressDialog;
  // List<int> mainAdditions = [];
  // List<int> moreAdditions = [];

  // int totalPrice;
  //String notes = '';

  Future<void> addToCart(
      BuildContext context, int productId, int quantity) async {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = Provider.of<SharedPref>(context, listen: false).token;
    Map<String, dynamic> headers = {
      HttpHeaders.authorizationHeader: 'Bearer $apiToken',
    };
    Map<String, dynamic> map = {
      'product_id': productId,
      'quantity': quantity,
    };
    // if (notes.trim().isNotEmpty) {
    //   map.addAll(
    //     {
    //       'notes': notes,
    //     },
    //   );
    // }
    // if (mainAdditions.isNotEmpty) {
    //   for (var i = 0; i < mainAdditions.length; i++) {
    //     print('hi dd ${mainAdditions[i]}');
    //     map.addAll(
    //       {
    //         'main_additions[$i]': mainAdditions[i],
    //       },
    //     );
    //   }
    // }
    // if (moreAdditions.isNotEmpty) {
    //   for (var i = 0; i < moreAdditions.length; i++) {
    //     map.addAll(
    //       {
    //         'more_additions[$i]': moreAdditions[i],
    //       },
    //     );
    //   }
    // }

    FormData data = FormData.fromMap(map);
    // _customProgressDialog.showProgressDialog();
    // _customProgressDialog.showPr();
    // Response response = await _util.post(
    //   options == CartOptions.Add ? 'add-to-cart' : 'add-or-update-cart',
    //   headers: headers,
    //   body: data,
    // );
    Response response = await _util.post(
      'add-to-cart',
      context,
      headers: headers,
      body: data,
    );
    // if (response == null) {
    //   await _customProgressDialog.hidePr();
    //   _dialog.showWarningDialog(
    //     btnOnPress: () {},
    //     context: context,
    //     msg: "من فضلك تأكد من وجود إتصال بالإنترنت",
    //   );
    //   return;
    // }
    if (response.statusCode == 200) {
      _model = AddToCartModel.fromJson(response.data);
      notifyListeners();
      //  await _customProgressDialog.hidePr();
      CustomAlert().toast(context: context, title: 'تم اعتماد طلبك');
    } else {
      _model = AddToCartModel.fromJson(response.data);
      notifyListeners();
      // await _customProgressDialog.hidePr();
      await _dialog.showErrorDialog(
        context: context,
        msg: _model.error[0].value,
        ok: 'موافق',
        btnOnPress: () {},
      );
    }
  }
}
