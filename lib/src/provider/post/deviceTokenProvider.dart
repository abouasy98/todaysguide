import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/post/deviceTokenModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';


class DeviceTokenProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<DeviceTokenModel> getToken(
    String token,
    BuildContext context
  ) async {
    Map<String, String> headers = {};
    FormData formData = FormData.fromMap({
      "device_token": token,
    });

    Response response =
        await _utils.post("device-token",context, body: formData, headers: headers);
    if (response.statusCode == 200) {
      print("get token sucsseful");
      return DeviceTokenModel.fromJson(response.data);
    } else {
      print("error get token");
      return DeviceTokenModel.fromJson(response.data);
    }
  }
}
