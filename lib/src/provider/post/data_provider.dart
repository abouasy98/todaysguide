// import 'package:flutter/cupertino.dart';

// import 'package:todaysguide/src/Models/post/productsFilterModel.dart';

// import 'fetchbigDataProvider.dart';

// enum LoadMoreStatus { LOADING, STABLE }

// class DataProvider with ChangeNotifier {
//   APIService _apiService;
//   ProductsFillter _dataFetcher;
//   int totalPages = 0;
//   int pageSize = 25;

//   List<Products> get allUsers => _dataFetcher.data;
//   double get totalRecords => _dataFetcher.totalRecords.toDouble();

//   LoadMoreStatus _loadMoreStatus = LoadMoreStatus.STABLE;
//   getLoadMoreStatus() => _loadMoreStatus;

//   DataProvider() {
//     _initStreams();
//   }

//   void _initStreams() {
//     _apiService = APIService();
//     _dataFetcher = ProductsFillter();
//   }

//   void resetStreams() {
//     _initStreams();
//   }

//   fetchAllUsers(
//       BuildContext context, int shopId, int categryId, pageNumber) async {
//     if ((totalPages == 0) || pageNumber <= totalPages) {
//       ProductsFillter itemModel =
//           await _apiService.getData(context, shopId, categryId, pageNumber);
//       if (_dataFetcher.data == null) {
//         totalPages = ((itemModel.totalRecords - 1) / pageSize).ceil();
//         _dataFetcher = itemModel;
//       } else {
//         _dataFetcher.data.addAll(itemModel.data);
//         _dataFetcher = _dataFetcher;

//         // One load more is done will make it status as stable.
//         setLoadingState(LoadMoreStatus.STABLE);
//       }

//       notifyListeners();
//     }

//     if (pageNumber > totalPages) {
//       // One load more is done will make it status as stable.
//       setLoadingState(LoadMoreStatus.STABLE);
//       notifyListeners();
//     }
//   }

//   setLoadingState(LoadMoreStatus loadMoreStatus) {
//     _loadMoreStatus = loadMoreStatus;
//     notifyListeners();
//   }
// }
