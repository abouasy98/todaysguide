import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/post/clientFinishOrderModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/HomePages/more/internal/Wallet/online_payment.dart';

class ClientFinishOrderProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  ClientFinishOrderModel _model;
  clientFinishOder(String token, int id, int type, BuildContext context) async {
    FormData formData = FormData.fromMap({"finish": type});

    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response = await _utils.post("client-finish-order/$id", context,
        headers: headers, body: formData);
    if (response.statusCode == 200) {
      print("get make_ad_favrouit sucsseful");
      _model = ClientFinishOrderModel.fromJson(response.data);
    } else {
      print("error get make_ad_favrouit data");
      _model = ClientFinishOrderModel.fromJson(response.data);
    }
    if (response.statusCode == 200) {
      _model = ClientFinishOrderModel.fromJson(response.data);
      String url = _model.data.paymentUrl;
      await Navigator.of(context).push(MaterialPageRoute(
          builder: (ctx) => OnlinePaymentScreen(
                key: ValueKey(_model.data.key),
                url: url,
              )));
    }
    notifyListeners();
  }
}
