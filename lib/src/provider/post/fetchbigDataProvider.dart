import 'package:flutter/cupertino.dart';
import 'package:todaysguide/src/Models/post/fetchDataModel.dart';

import 'package:todaysguide/src/api_service/api_Srevice.dart';

class SortBy {
  String value;
  String text;
  String sortOrder;

  SortBy(this.value, this.text, this.sortOrder);
}

enum LoadMoreStatus { INITIAL, STABLE, LOADING }

class FetchDataProvider with ChangeNotifier {
  APIServices _apiServices;
  List<DataListView> _productList;
  int pageSize = 10;
  int categoryId;
  int pageNumber = 1;
  List<DataListView> get allProducts => _productList;

  double get totalRecords => _productList.length.toDouble();

  LoadMoreStatus _loadMoreStatus = LoadMoreStatus.STABLE;

  getLoadMoreStatus() => _loadMoreStatus;

  fetchDataProvider() {
  }

  void resetStreams() {
    pageNumber = 1;
    categoryId = 0;
    _apiServices = APIServices();
    _productList = [];
  }

  setLoadingState(LoadMoreStatus loadMoreStatus) {
    _loadMoreStatus = loadMoreStatus;
    notifyListeners();
  }

  setSortOrder(SortBy sortBy) {
    notifyListeners();
  }

  fetchProducts(
 {
    int shopId,
    String strSearch,
    BuildContext context,
  }) async {
    List<DataListView> itemModel = await _apiServices.getProducts(
      pageNumber: pageNumber,
      categoryId: categoryId,
      context: context,
      shopId: shopId,
      name: strSearch,
    );
    if (itemModel != null && itemModel.length > 0) {
      _productList.addAll(itemModel);
    }
    setLoadingState(LoadMoreStatus.STABLE);
    notifyListeners();
  }
}
