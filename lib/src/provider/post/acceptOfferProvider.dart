import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/post/acceptOfferModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/HomePages/main_page.dart';

class AcceptOfferProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  AcceptOfferModel acceptOfferModel;
  Future<AcceptOfferModel> acceptOffer(
      int id,  String token, BuildContext context) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
 
    Response response = await _utils.post("accept-offer/$id", context,
        headers: headers, );
    if (response.statusCode == 200) {
      print("get accept-offer sucsseful");

      acceptOfferModel = AcceptOfferModel.fromJson(response.data);
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (_) => MainPage(index: 1,)),
            (Route<dynamic> route) => false);
      notifyListeners();
      return AcceptOfferModel.fromJson(response.data);
    } else {
      print("error post accept-offer data");
      return AcceptOfferModel.fromJson(response.data);
    }
  }
}
