import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/post/shopViewModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class ShopViewProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<ShopViewModel> shopView(
      String token, BuildContext context, int id) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};

    Response response =
        await _utils.get("shop-view/$id", context, headers: headers);
    if (response.statusCode == 200) {
      print("get shop-view sucsseful");
      return ShopViewModel.fromJson(response.data);
    } else {
      print("error shop-view token");
      return ShopViewModel.fromJson(response.data);
    }
  }
}
