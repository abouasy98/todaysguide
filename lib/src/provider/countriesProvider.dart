import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/countriesModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/labeled_bottom_sheet.dart';

class CountriesProvider with ChangeNotifier {
  List<Countries> _countries = [];

  List  <Countries> get getcountries {
    return [..._countries];
  }

  List<BottomSheetModel> _bottomSheet = [];

  List<BottomSheetModel> get bottomSheet {
    return [..._bottomSheet];
  }




  NetworkUtil _utils = new NetworkUtil();
  CountriesModel countries;
  Future<CountriesModel> getCountries(BuildContext context)async {
    final List<Countries> loadedCountries = [];
       final List<BottomSheetModel> loadedSheetModel = [];
    Map<String, String> headers = {
      //    "X-localization": localization.currentLanguage.toString()
    };
    Response response = await _utils.get("countries",context, headers: headers);
    if (response.statusCode == 200) {
      print("get countries sucsseful");

      countries = CountriesModel.fromJson(response.data);
  countries.data.forEach((e) {
        loadedSheetModel.add(
            BottomSheetModel(id: e.id, name: e.name, realID: e.id.toString(),countrycode: e.phonecode));
      });
      _bottomSheet = loadedSheetModel.reversed.toList();
      countries.data.forEach((element) {
        loadedCountries.add(Countries(
            dateTime: element.createdAt,
            phonecode:element.phonecode,
            id: element.id.toString(),
            name: element.name));
      });
      _countries = loadedCountries.reversed.toList();
      notifyListeners();
      return CountriesModel.fromJson(response.data);
    } else {
      print("error get countries data");
      return CountriesModel.fromJson(response.data);
    }
  }
}

class Countries {
  final String id;
  final int phonecode;
  final String name;
  final DateTime dateTime;

  Countries({@required this.id, @required this.name, @required this.dateTime,this.phonecode});
}
