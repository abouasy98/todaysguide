import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/helpCenterModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
class HelpCenterProvider with ChangeNotifier {
  int phone;
  String token;
  NetworkUtil _utils = new NetworkUtil();
  HelpCenterModel callModel;
  Future<HelpCenterModel> getPhone(BuildContext context) async {
      Map<String, String> headers = {
      'Authorization': 'Bearer $token',
    };
    Response response = await _utils.get("settings", context,headers: headers);
    if (response.statusCode == 200) {
      print("get settings sucsseful");

      callModel = HelpCenterModel.fromJson(response.data);
      phone = callModel.data.phoneNumber;
      notifyListeners();
      return HelpCenterModel.fromJson(response.data);
    } else {
      print("error get settings data");
      return HelpCenterModel.fromJson(response.data);
    }
  }
}
