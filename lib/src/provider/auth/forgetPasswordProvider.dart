import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Models/auth/forgetPasswordModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_progress_dialog.dart';
import 'package:todaysguide/src/screens/Registeration/confirmCode.dart';
import 'confirmResetCodeProvider.dart';

class ForgetPasswordProvider with ChangeNotifier {
  String phone;
  int countryCode;
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;

  ForgetPasswordModel model;
  forgetPassword(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
    //  "X-localization": localization.currentLanguage.toString()
    };
    FormData formData = FormData.fromMap({
      "phone_number": phone,
      "country_code":966,
    });

    Response response =
        await _utils.post("forget-password",context, body: formData, headers: headers);
    // if (response == null) {
    //   print('error forgetPassword');
    //   Future.delayed(Duration(seconds: 1), () {
    //     customProgressDialog.hidePr();
    //     dialog.showWarningDialog(
    //       btnOnPress: () {},
    //       context: context,
    //       msg: "من فضلك تأكد من وجود إتصال بالإنترنت",
    //     );
    //   });

    //   return;
    // }
    if (response.statusCode == 200) {
      print("forget_password sucsseful");
      model = ForgetPasswordModel.fromJson(response.data);
    } else {
      print("error forget_password");
      model = ForgetPasswordModel.fromJson(response.data);
    }
    if (model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      Provider.of<ConfirmResetCodeProvider>(context, listen: false).phone =
          phone;

      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => ConfirmCode(
                stateOfVerificationCode: 2,
              )));
    } else {
      print('error forgetPassword');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {},
          context: context,
          msg: model.error[0].value,
          ok: "موافق",
        );
      });
    }
    notifyListeners();
  }
}
