import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Models/auth/phoneVerificationModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/provider/auth/signUpProvider.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_progress_dialog.dart';
import 'package:todaysguide/src/screens/Registeration/userType.dart';

class PhoneVerificationProvider with ChangeNotifier {

  String phone;
  String code;

  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;
  PhoneVerificationModel _model;
  phoneVerification(BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      // "X-localization": localization.currentLanguage.toString()
    };
    FormData formData = FormData.fromMap({
      "phone_number": phone,
      "code": code,
      "country_code": 966,
    });

    Response response = await _utils.post("phone-verification",context,
        body: formData, headers: headers);
    // if (response == null) {
    //   Future.delayed(Duration(seconds: 1), () {
    //     customProgressDialog.hidePr();
    //     print('error phone_verification');
    //     dialog.showWarningDialog(
    //       btnOnPress: () {},
    //       context: context,
    //       msg: "من فضلك تأكد من وجود إتصال بالإنترنت",
    //     );
    //   });

    //   return;
    // }
    if (response.statusCode == 200) {
      print("phone_verification sucsseful");
      _model = PhoneVerificationModel.fromJson(response.data);
    } else {
      print("error phone_verification");
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      _model = PhoneVerificationModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      print('success phone_verification');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      Provider.of<SignUpProvider>(context, listen: false).phone = phone;
     
      print(
          'fqewwwwwwwww${Provider.of<SignUpProvider>(context, listen: false).phone}');
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => UserType()));
    } else {
      print('error phone_verification');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
          btnOnPress: () {},
          context: context,
          msg: _model.error[0].value,
          ok: "موافق",
        );
      });
    }
    notifyListeners();
  }
}
