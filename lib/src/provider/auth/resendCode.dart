import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/auth/registerMobileModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
class ResendCodeProvider with ChangeNotifier {
  String phone;
  
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  RegisterMobileModel model;

  resendCode(BuildContext context) async {

    Map<String, String> headers = {
  //    "X-localization": localization.currentLanguage.toString()
    };
    FormData formData = FormData.fromMap({
      "phone_number": phone,
           "country_code":966,
    });

    Response response =
        await _utils.post("resend-code", context,body: formData, headers: headers);
   
    if (response.statusCode == 200) {
      print("resend-code sucsseful");
      model = RegisterMobileModel.fromJson(response.data);
    } else {
      print("error resend-code");
      model = RegisterMobileModel.fromJson(response.data);
    }
    if (model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
      });

    } else {
      print('error resend-code');

    }
    notifyListeners();
  }
}
