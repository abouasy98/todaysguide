import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Models/auth/signUpModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/DriverHome/mainPageDriver.dart';
import 'package:todaysguide/src/screens/HomePages/main_page.dart';
import 'package:todaysguide/src/screens/Intro/waitingScreen.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_progress_dialog.dart';
import 'package:todaysguide/src/screens/StoreHome/mainPageStore.dart';

class SignUpProvider with ChangeNotifier {
  String phone;
  String name;
  String address;
  String password;
  String passwordConfirmation;
  String cityId;
  int carType;
  int price;
  String departmentId;
  String details;
  File photo;
  File identity;
  File license;
  File carForm;
  String longitude;
  String open;
  String close;
  String latitude;
  int type;
  NetworkUtil _utils = new NetworkUtil();
  CustomDialog dialog = CustomDialog();
  CustomProgressDialog customProgressDialog;
  ProgressDialog pr;

  SignUpModel _model;
  SharedPreferences _prefs;
  signUp(String token, BuildContext context) async {
    customProgressDialog = CustomProgressDialog(context: context, pr: pr);
    customProgressDialog.showProgressDialog();
    customProgressDialog.showPr();
    Map<String, String> headers = {
      // "X-localization": localization.currentLanguage.toString(),
    };
    FormData formData = FormData.fromMap({
      "type": type,
      "phone_number": phone,
      "name": name,
      "price": price,
      "photo": photo == null ? null : await MultipartFile.fromFile(photo.path),
      "password": password,
      "password_confirmation": passwordConfirmation,
      "device_token": token,
      "address": address,
      "city_id": cityId,
      "identity":
          identity == null ? null : await MultipartFile.fromFile(identity.path),
      "license":
          license == null ? null : await MultipartFile.fromFile(license.path),
      "car_form":
          carForm == null ? null : await MultipartFile.fromFile(carForm.path),
      "country_code": 966,
      "car_type": carType,
      "department_id": departmentId,
      "longitude": longitude!=null?longitude:31.0066411,
      "latitude": latitude!=null?latitude:30.7982574,
      "open_time": open,
      "close_time": close,
      "details": details,
    });

    Response response = await _utils.post("register", context,
        body: formData, headers: headers);
    // if (response == null) {
    //   print('error register res == null');
    //   Future.delayed(Duration(seconds: 1), () {
    //     customProgressDialog.hidePr();
    //     dialog.showWarningDialog(
    //       btnOnPress: () {},
    //       context: context,
    //       msg: "من فضلك تأكد من وجود إتصال بالإنترنت",
    //     );
    //   });

    //   return;
    // }
    if (response.statusCode == 200) {
      print("register sucsseful");
      _model = SignUpModel.fromJson(response.data);
    } else {
      print("error register");
      _model = SignUpModel.fromJson(response.data);
    }
    if (_model.code == 200) {
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
      });
      _prefs = await SharedPreferences.getInstance();
      print('done');
      print("${_model.data.name}...................");
      print("${_model.data.apiToken}...................");
      print("${_model.data.id}...................");
      print("${_model.data.active}...................");
      _prefs.setInt('id', _model.data.id);
      _prefs.setString('name', _model.data.name);
      _prefs.setString('phone', _model.data.phoneNumber);
      _prefs.setString('token', _model.data.apiToken);
      _prefs.setString('address', _model.data.address);
      _prefs.setString('photo', _model.data.photo);
      _prefs.setString('car_form', _model.data.carForm);
      _prefs.setString('license', _model.data.license);
      _prefs.setString('identity', _model.data.identity);
      _prefs.setInt('active', _model.data.active);
      _prefs.setInt('type', _model.data.type);
      _prefs.setString('longitude', _model.data.longitude);
      _prefs.setString('latitude', _model.data.latitude);
      _prefs.setInt('cityId', _model.data.cityId);
      _prefs.setString('city', _model.data.city);
      _prefs.setInt('regionId', _model.data.regionId);
      _prefs.setString('region', _model.data.region);
      _prefs.setInt('department_id', _model.data.departmentId);
      _prefs.setString('department', _model.data.department);
      _prefs.setString('driverPrice', _model.data.driverPrice);
      _prefs.setInt('products_number', _model.data.productsNumber);
      _prefs.setInt('commission_status', _model.data.commissionStatus);
      _prefs.setInt('cartype', _model.data.carType);
    _prefs.setString('workTimeStart', _model.data.openTime);
      _prefs.setString('workTimeEnd', _model.data.closeTime);
      //----------------------car Data-------------------//
      if (_model.data.type == 1) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (_) => MainPage()),
            (Route<dynamic> route) => false);
      } else if (_model.data.type == 2) {
        if (_prefs.get('active') == 0) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => WaitingAccepting(),
            ),
            (Route<dynamic> route) => false,
          );
        } else {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => MainPageDriver(),
            ),
            (Route<dynamic> route) => false,
          );
        }
      } else if (_model.data.type == 3) {
        if (_prefs.get('active') == 0) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => WaitingAccepting(),
            ),
            (Route<dynamic> route) => false,
          );
        } else {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => MainPageStore(),
            ),
            (Route<dynamic> route) => false,
          );
        }
      }
      return _prefs;
    } else {
      print('error register');
      Future.delayed(Duration(seconds: 1), () {
        customProgressDialog.hidePr();
        dialog.showErrorDialog(
            btnOnPress: () {},
            context: context,
            msg: _model.error[0].value,
            ok: "موافق",
            code: _model.code);
      });
    }
    notifyListeners();
  }
}
