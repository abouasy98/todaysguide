import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/servicesCatogoryModle.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/labeled_bottom_sheet.dart';

class ServicesCatogoryProvider with ChangeNotifier {
  List<ServicesCatogor> _services = [];

  List<ServicesCatogor> get services {
    return [..._services];
  }

  List<BottomSheetModel> _bottomSheet = [];

  List<BottomSheetModel> get bottomSheet {
    return [..._bottomSheet];
  }

  NetworkUtil _utils = new NetworkUtil();
  ServicesCatogoryModel regionModel;
  Future<ServicesCatogoryModel> getServicesCatogory(BuildContext context) async {
    final List<ServicesCatogor> loaded = [];
    final List<BottomSheetModel> loadedSheetModel = [];

    Map<String, String> headers = {};
    Response response = await _utils.get("service-categories", context, headers: headers);
    if (response.statusCode == 200) {
      print("get service-categories sucsseful");

      regionModel = ServicesCatogoryModel.fromJson(response.data);
      regionModel.data.forEach((e) {
        loadedSheetModel.add(
            BottomSheetModel(id: e.id, name: e.name, realID: e.id.toString()));
      });
      _bottomSheet = loadedSheetModel.reversed.toList();

      regionModel.data.forEach((e) {
        loaded.add(ServicesCatogor(
          createdAt: e.createdAt,
          id: e.id,
          name: e.name,
        ));
      });
      _services = loaded.reversed.toList();
      notifyListeners();
      return ServicesCatogoryModel.fromJson(response.data);
    } else {
      print("error get service-categories data");
      return ServicesCatogoryModel.fromJson(response.data);
    }
  }
}
