import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/Models/get/MyServicesModle.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class MyServicesProvider with ChangeNotifier {
  List<MyServicesItem> _myServices = [];

  List<MyServicesItem> get myServices {
    return [..._myServices];
  }


  NetworkUtil _utils = new NetworkUtil();
  MyServicesModel driverOrdersModel;
  Future<MyServicesModel> getMyServices(
      String token, int filterId, BuildContext context) async {
    final List<MyServicesItem> loadedOrders = [];

    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response =
        await _utils.get("my-services", context, headers: headers);
    if (response.statusCode == 200) {
      print("get my-services sucsseful");

      driverOrdersModel = MyServicesModel.fromJson(response.data);
      driverOrdersModel.data.forEach((e) {
        if (filterId == null) {
          loadedOrders.add(MyServicesItem(
              id: e.id,
              details: e.details,
              name: e.name,
              phoneNumber: e.phoneNumber,
              photo: e.photo,
              serviceCategory: e.serviceCategory,
              serviceCategoryId: e.serviceCategoryId,
              state: e.state,
              createdAt: e.createdAt,
              latitude: e.latitude,
              longitude: e.longitude,
              user: e.user,
              userId: e.userId,
              distance: calculateDistance(
                    Provider.of<MapHelper>(context, listen: false).position !=
                          null
                      ? Provider.of<MapHelper>(context, listen: false)
                          .position
                          .latitude
                      : 30.7982574,
                  Provider.of<MapHelper>(context, listen: false).position !=
                          null
                      ? Provider.of<MapHelper>(context, listen: false)
                          .position
                          .longitude
                      : 31.0066411,
                  double.parse(e.latitude),
                  double.parse(e.longitude)) ?? 0.0));
        } else if (filterId == e.serviceCategoryId) {
          loadedOrders.add(MyServicesItem(
              id: e.id,
              details: e.details,
              name: e.name,
              phoneNumber: e.phoneNumber,
              photo: e.photo,
              serviceCategory: e.serviceCategory,
              serviceCategoryId: e.serviceCategoryId,
              state: e.state,
              createdAt: e.createdAt,
              latitude: e.latitude,
              longitude: e.longitude,
              user: e.user,
              userId: e.userId,
              distance: calculateDistance(
                Provider.of<MapHelper>(context, listen: false).position !=
                          null
                      ? Provider.of<MapHelper>(context, listen: false)
                          .position
                          .latitude
                      : 30.7982574,
                  Provider.of<MapHelper>(context, listen: false).position !=
                          null
                      ? Provider.of<MapHelper>(context, listen: false)
                          .position
                          .longitude
                      : 31.0066411,
                  double.parse(e.latitude),
                  double.parse(e.longitude))));
        }
      });
      _myServices = loadedOrders.reversed.toList();
      notifyListeners();
      return MyServicesModel.fromJson(response.data);
    } else {
      print("error get my-services data");
      driverOrdersModel = MyServicesModel.fromJson(response.data);

      _myServices = loadedOrders.reversed.toList();
      notifyListeners();
      return MyServicesModel.fromJson(response.data);
    }
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
        // try{}catch(e){}
        print("distant == ${12742 * asin(sqrt(a))}");
    return 12742 * asin(sqrt(a)) ?? 0;
  }
}
