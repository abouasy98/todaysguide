import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/DriverModel/driverFinishOrderModle.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';

class DriverFinishOrderProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  DriverFinishOrderModel finishOrderModle;
  finishOrder({String token, int orderId, BuildContext context}) async {
    Map<String, String> headers = {"Authorization": "Bearer $token"};
    Response response = await _utils.get(
      "driver-finish-order/$orderId",
      context,
      headers: headers,
    );

    if (response.statusCode == 200) {
      print("get driver-finish-order sucsseful");
      finishOrderModle = DriverFinishOrderModel.fromJson(response.data);
    } else {
      print("error get driver-finish-order data");
      finishOrderModle = DriverFinishOrderModel.fromJson(response.data);
    }
    if (finishOrderModle.code == 200) {
      CustomAlert().toast(context: context, title: "تم أنهاء الطلب بنجاح");
      Navigator.pop(context);
    } else {
      print('error driver-finish-order');
      CustomAlert().toast(
        context: context,
        title: finishOrderModle.error[0].value,
      );
    }
  }
}
