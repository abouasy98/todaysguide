import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todaysguide/src/Helpers/map_helper.dart';
import 'package:todaysguide/src/Models/DriverModel/avilabiltyModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';
import 'package:todaysguide/src/screens/DriverHome/HomePage/request_location_screen.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_alert.dart';
import 'package:todaysguide/src/screens/MainWidgets/custom_new_dialog.dart';

class AvailabilityProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();
  AvailabilityModle avalableModel;
  var isEnabled;
  CustomDialog dialog = CustomDialog();
  SharedPreferences _prefs;
  changeAvailable({String token, int available, BuildContext context}) async {
    if (available == 1) {
      isEnabled = await getUserLocation(context);
    } else {
      isEnabled = false;
    }

    if (isEnabled) {
      print("availableh555$available");
      FormData formData = FormData.fromMap({
        "availability": available,
        "latitude":
            Provider.of<MapHelper>(context, listen: false).position.latitude,
        "longitude":
            Provider.of<MapHelper>(context, listen: false).position.longitude,
      });
      Map<String, String> headers = {"Authorization": "Bearer $token"};
      Response response = await _utils.post(
        "availability",
        context,
        body: formData,
        headers: headers,
      );

      if (response.statusCode == 200) {
        print("get availability sucsseful");
        avalableModel = AvailabilityModle.fromJson(response.data);
      } else {
        print("error get availability data");
        avalableModel = AvailabilityModle.fromJson(response.data);
      }
      if (avalableModel.code == 200) {
        if (avalableModel.data.availability == "1") {
          // CustomAlert()
          //     .toast(context: context, title: "تم تغير الحالة الي متاح بنجاح");
        } else
          CustomAlert().toast(
              context: context, title: "تم تغير الحالة الي غير متاح بنجاح");
        _prefs = await SharedPreferences.getInstance();
        _prefs.setInt('avalable', int.parse(avalableModel.data.availability));
        return avalableModel.data.availability;
      } else {
        print('error confirmed');
        CustomAlert().toast(
          context: context,
          title: avalableModel.error[0].value,
        );
      }
    } else {
      print("availableh777$available");
      FormData formData = FormData.fromMap({
        "availability": 0,
        "latitude": Provider.of<MapHelper>(context, listen: false).position !=
                null
            ? Provider.of<MapHelper>(context, listen: false).position.latitude
            : 24.418915604601022,
        "longitude": Provider.of<MapHelper>(context, listen: false).position !=
                null
            ? Provider.of<MapHelper>(context, listen: false).position.longitude
            : 39.646886250038214,
      });
      Map<String, String> headers = {"Authorization": "Bearer $token"};
      Response response = await _utils.post(
        "availability",
        context,
        body: formData,
        headers: headers,
      );

      if (response.statusCode == 200) {
        print("get availability sucsseful");
        avalableModel = AvailabilityModle.fromJson(response.data);
      } else {
        print("error get availability data");
        avalableModel = AvailabilityModle.fromJson(response.data);
      }
      if (avalableModel.code == 200) {
        if (avalableModel.data.availability == "1") {
          // CustomAlert()
          //     .toast(context: context, title: "تم تغير الحالة الي متاح بنجاح");
        } else
          CustomAlert().toast(
              context: context, title: "تم تغير الحالة الي غير متاح بنجاح");
        _prefs = await SharedPreferences.getInstance();
        _prefs.setInt('avalable', int.parse(avalableModel.data.availability));
        return avalableModel.data.availability;
      } else {
        print('error confirmed');
        CustomAlert().toast(
          context: context,
          title: avalableModel.error[0].value,
        );
      }
    }
  }

  Future<bool> getUserLocation(BuildContext context) async {
    if (!await Geolocator.isLocationServiceEnabled()) {
      print('nit ');
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (c) => RequestLocationScreen()),
        (route) => false,
      );

      return false;
    }
    print('ss');

    LocationPermission permission = await Geolocator.checkPermission();
    print('ss');

    if (permission == LocationPermission.deniedForever) {
      await Geolocator.openAppSettings();
      return false;
    }
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied ||
          permission == LocationPermission.deniedForever) {
        return false;
      }
    }
    return true;
  }
}
