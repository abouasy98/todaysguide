import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todaysguide/src/Models/getServiceByIdModel.dart';
import 'package:todaysguide/src/Repository/networkUtlis.dart';

class GetServiceByIdProvider with ChangeNotifier {
  NetworkUtil _utils = new NetworkUtil();

  Future<GetServiceByIdModel> getSplashes(BuildContext context, int id) async {
    Map<String, String> headers = {};
    Response response =
        await _utils.get("get-service-by-id/$id", context, headers: headers);
    if (response.statusCode == 200) {
      print("get get Splashes data sucsseful");
      return GetServiceByIdModel.fromJson(response.data);
    } else {
      print("error get Splashes News data");
      return GetServiceByIdModel.fromJson(response.data);
    }
  }
}
