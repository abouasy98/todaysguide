import 'package:flutter/material.dart';

import '../Models/post/shopsModel.dart';
import 'post/productsFilterProvider.dart';

enum FilterType {
  NumberOfProducts,
  Nearest,
  MostViews,
}

class CheckProvider with ChangeNotifier {
  List<Products> productsFilter = [];
  List<Shops> shopsFilter = [];
  List<Products> searchResults;
  FilterType filterType = FilterType.Nearest;

  List<Products> suggteionList;

  String query = "";
  void assignValueProduct({List<Products> products}) {
    productsFilter.clear();
    productsFilter = products;

    notifyListeners();
  }

  Future assignValueShops({List<Shops> shops}) async {
    shopsFilter = shops;

    notifyListeners();
  }

  void sortShops() {
    if (filterType == FilterType.NumberOfProducts && shopsFilter.isNotEmpty) {
      shopsFilter.sort(
        (shop1, shop2) => shop2.productsNumber.compareTo(
          shop1.productsNumber,
        ),
      );
    } else if (filterType == FilterType.Nearest && shopsFilter.isNotEmpty) {
      shopsFilter.sort(
        (shop1, shop2) => shop1.distance.compareTo(
          shop2.distance,
        ),
      );
    } else if (filterType == FilterType.MostViews && shopsFilter.isNotEmpty) {
      shopsFilter.sort(
        (shop1, shop2) => shop2.viewCount.compareTo(
          shop1.viewCount,
        ),
      );
    }
    notifyListeners();
  }

  void searchResult({
    List<Products> searchResultsparmter,
    String controllerPamter,
  }) {
    searchResults = searchResultsparmter;

    searchResults = searchResultsparmter.where((Products user) {
      String getUserName = user.name.toLowerCase();
      query = controllerPamter.toLowerCase();

      bool matchUserName = getUserName.contains(query);

      return (matchUserName);
    }).toList();

    notifyListeners();
  }
}
