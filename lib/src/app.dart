import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:todaysguide/src/provider/changeData/changePasswordProvider.dart';
import 'package:todaysguide/src/provider/changeData/changePhoneCodeProvider.dart';
import 'package:todaysguide/src/provider/changeData/changePhoneProvider.dart';
import 'package:todaysguide/src/provider/changeData/editAcountProvider.dart';
import 'package:todaysguide/src/provider/checkProvider.dart';
import 'package:todaysguide/src/provider/countriesProvider.dart';
import 'package:todaysguide/src/provider/deleteShopPhoto.Provider.dart';
import 'package:todaysguide/src/provider/get/RegionsProvider.dart';
import 'package:todaysguide/src/provider/get/categoriesProvider.dart';
import 'package:todaysguide/src/provider/get/citiesProvider.dart';
import 'package:todaysguide/src/provider/get/deleteNotificationProvider.dart';
import 'package:todaysguide/src/provider/get/departmentsProvider.dart';
import 'package:todaysguide/src/provider/get/getMyFavShopsProvider.dart';
import 'package:todaysguide/src/provider/get/getOrderByIdProvider.dart';
import 'package:todaysguide/src/provider/get/getShopByIdProvider.dart';
import 'package:todaysguide/src/provider/get/getUserDataProvider.dart';
import 'package:todaysguide/src/provider/get/get_cart_provider.dart';
import 'package:todaysguide/src/provider/get/notificationProvider.dart';
import 'package:todaysguide/src/provider/get/offersProvider.dart';
import 'package:todaysguide/src/provider/get/productByIdProvider.dart';
import 'package:todaysguide/src/provider/get/productCartCountProvider.dart';
import 'package:todaysguide/src/provider/get/setting.dart';
import 'package:todaysguide/src/provider/get/splashesProvider.dart';
import 'package:todaysguide/src/provider/post/CancelOrderOffer.dart';
import 'package:todaysguide/src/provider/post/ClientFinishOrderProvider.dart';
import 'package:todaysguide/src/provider/post/ClientOrderProvider.dart';
import 'package:todaysguide/src/provider/post/acceptOfferProvider.dart';
import 'package:todaysguide/src/provider/post/addAdToFavProvider.dart';
import 'package:todaysguide/src/provider/post/add_to_cart_provider.dart';
import 'package:todaysguide/src/provider/post/deviceTokenProvider.dart';
import 'package:todaysguide/src/provider/post/fetchbigDataProvider.dart';
import 'package:todaysguide/src/provider/post/post_current_order_provider.dart';
import 'package:todaysguide/src/provider/post/productsFilterProvider.dart';
import 'package:todaysguide/src/provider/post/reportProductProvider.dart';
import 'package:todaysguide/src/provider/post/reportShopProvider.dart';
import 'package:todaysguide/src/provider/post/search_provider.dart';
import 'package:todaysguide/src/provider/post/shopViewProvider.dart';
import 'package:todaysguide/src/provider/post/shopsProvider.dart';
import 'package:todaysguide/src/provider/postServiceProvider.dart';
import 'package:todaysguide/src/screens/HomePages/Home/ShopDetails/shopDetails.dart';
import 'package:todaysguide/src/screens/Intro/splash_screen.dart';
import 'Helpers/map_helper.dart';
import 'Helpers/sharedPref_helper.dart';
import 'provider/DriverProvider/DriverOrderProvider.dart';
import 'provider/DriverProvider/MyServicesProvider.dart';
import 'provider/DriverProvider/availabiltyProvider.dart';
import 'provider/DriverProvider/driverCommitionProvider.dart';
import 'provider/DriverProvider/driverFinishOrderProvider.dart';
import 'provider/DriverProvider/editCarDataProvider.dart';
import 'provider/Shop/MyProductProvider.dart';
import 'provider/Shop/deleteProductProvider.dart';
import 'provider/Shop/shopHoursProvider.dart';
import 'provider/Shop/shopOrdersProvider.dart';
import 'provider/aboutUsProvider.dart';
import 'provider/auth/confirmResetCodeProvider.dart';
import 'provider/auth/forgetPasswordProvider.dart';
import 'provider/auth/logOutProvider.dart';
import 'provider/auth/loginProvider.dart';
import 'provider/auth/phoneVerificationProvider.dart';
import 'provider/auth/registerMobileProvider.dart';
import 'provider/auth/resendCode.dart';
import 'provider/auth/resetPasswordProvider.dart';
import 'provider/auth/signUpProvider.dart';
import 'provider/Shop/createProductProvider.dart';
import 'provider/chargeBankProvider.dart';
import 'provider/chargeOnlineProvider.dart';
import 'provider/createServiceProvider.dart';
import 'provider/deletePhotoProvider.dart';
import 'provider/deleteServiceProvider.dart';
import 'provider/Shop/editProductProvider.dart';
import 'provider/editServiceProvider.dart';
import 'provider/getMapImageProvider.dart';
import 'provider/Shop/productCatogoryProvider.dart';
import 'provider/historyProvider.dart';
import 'provider/myChargeProvider.dart';
import 'provider/servicesCatogoryProvider.dart';
import 'provider/termsProvider.dart';
import 'provider/walletRequstProvider.dart';

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> navigator;

  const MyApp({Key key, this.navigator}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => SharedPref()),
        ChangeNotifierProvider(create: (_) => LoginProvider()),
        ChangeNotifierProvider(create: (_) => RegisterMobileProvider()),
        ChangeNotifierProvider(create: (_) => PhoneVerificationProvider()),
        ChangeNotifierProvider(create: (_) => SignUpProvider()),
        ChangeNotifierProvider(create: (_) => RegionsProvider()),
        ChangeNotifierProvider(create: (_) => DeviceTokenProvider()),
        ChangeNotifierProvider(create: (_) => ResendCodeProvider()),
        ChangeNotifierProvider(create: (_) => CountriesProvider()),
        ChangeNotifierProvider(create: (_) => CitiesProvider()),
        ChangeNotifierProvider(create: (_) => ShopsByIdProvider()),
        ChangeNotifierProvider(create: (_) => GetMyFavShopProvider()),
        ChangeNotifierProvider(create: (_) => ProductsFilterProvider()),
        ChangeNotifierProvider(create: (_) => ShopsProvider()),
        ChangeNotifierProvider(create: (_) => ConfirmResetCodeProvider()),
        ChangeNotifierProvider(create: (_) => DepartMentProvider()),
        ChangeNotifierProvider(create: (_) => ForgetPasswordProvider()),
        ChangeNotifierProvider(create: (_) => ResetPasswordProvider()),
        ChangeNotifierProvider(create: (_) => AboutUsProvider()),
        ChangeNotifierProvider(create: (_) => TermsProvider()),
        ChangeNotifierProvider(create: (_) => ChangePasswordProvider()),
        ChangeNotifierProvider(create: (_) => EditUserDataProvider()),
        ChangeNotifierProvider(create: (_) => ChangeMobileProvider()),
        ChangeNotifierProvider(create: (_) => ChangePhoneCodeProvider()),
        ChangeNotifierProvider(create: (_) => MapHelper()),
        ChangeNotifierProvider(create: (_) => GetMapImage()),
        ChangeNotifierProvider(create: (_) => SplashesProvider()),
        ChangeNotifierProvider(create: (_) => CategoriesProvider()),
        ChangeNotifierProvider(create: (_) => CategoriesProvider()),
        ChangeNotifierProvider(create: (_) => AddAdToFavProvider()),
        ChangeNotifierProvider(create: (_) => NotoficationProvider()),
        ChangeNotifierProvider(create: (_) => GetUserDataProvider()),
        ChangeNotifierProvider(create: (_) => ProductsByIdProvider()),
        ChangeNotifierProvider(create: (_) => SettingProvider()),
        ChangeNotifierProvider(create: (_) => ReportShopProvider()),
        ChangeNotifierProvider(create: (_) => ReportProductProvider()),
        ChangeNotifierProvider(create: (_) => AddToCartProvider()),
        ChangeNotifierProvider(create: (_) => LogOutProvider()),
        ChangeNotifierProvider(create: (_) => GetCartProvider()),
        ChangeNotifierProvider(create: (_) => PostCurrentOrderProvider()),
        ChangeNotifierProvider(create: (_) => AvailabilityProvider()),
        ChangeNotifierProvider(create: (_) => DriverOrdersProvider()),
        ChangeNotifierProvider(create: (_) => ClientOrdersProvider()),
        ChangeNotifierProvider(create: (_) => OffersProvider()),
        ChangeNotifierProvider(create: (_) => CancelOrderProvider()),
        ChangeNotifierProvider(create: (_) => GetOrderByIdProvider()),
        ChangeNotifierProvider(create: (_) => ClientFinishOrderProvider()),
        ChangeNotifierProvider(create: (_) => SearchProvider()),
        ChangeNotifierProvider(create: (_) => PostServicesProvider()),
        ChangeNotifierProvider(create: (_) => ProductCartCountProvider()),
        ChangeNotifierProvider(create: (_) => AcceptOfferProvider()),
        ChangeNotifierProvider(create: (_) => ShopViewProvider()),
        ChangeNotifierProvider(create: (_) => DriverCommitionsProvider()),
        ChangeNotifierProvider(create: (_) => MyServicesProvider()),
        ChangeNotifierProvider(create: (_) => ServicesCatogoryProvider()),
        ChangeNotifierProvider(create: (_) => CreateServiceProvider()),
        ChangeNotifierProvider(create: (_) => DeletNotificationProvider()),
        ChangeNotifierProvider(create: (_) => DeleteServiceProvider()),
        ChangeNotifierProvider(create: (_) => EditServiceProvider()),
        ChangeNotifierProvider(create: (_) => ShopProuctProvider()),
        ChangeNotifierProvider(create: (_) => DeleteProductProvider()),
        ChangeNotifierProvider(create: (_) => ProductCategoryProvider()),
        ChangeNotifierProvider(create: (_) => CreateProductProvider()),
        ChangeNotifierProvider(create: (_) => EditProductProvider()),
        ChangeNotifierProvider(create: (_) => DeletePhotoProvider()),
        ChangeNotifierProvider(create: (_) => ShopOrderProvider()),
        ChangeNotifierProvider(create: (_) => HistoryProvider()),
        ChangeNotifierProvider(create: (_) => MyChargeProvider()),
        ChangeNotifierProvider(create: (_) => ChargeBankProvider()),
        ChangeNotifierProvider(create: (_) => ChargeOnlineProvider()),
        ChangeNotifierProvider(create: (_) => DriverFinishOrderProvider()),
        ChangeNotifierProvider(create: (_) => ShopHoursProvider()),
        ChangeNotifierProvider(create: (_) => EditCarDataProvider()),
        ChangeNotifierProvider(create: (_) => WalletRequestProvider()),
        ChangeNotifierProvider(create: (_) => DeleteShopPhotoProvider()),
        ChangeNotifierProvider(create: (_) => CheckProvider()),
        ChangeNotifierProvider(create: (_) => FetchDataProvider(),child: ShopDetails(),),
      ],
      //عاصي
      child: MaterialApp(
        navigatorKey: navigator,
        debugShowCheckedModeBanner: false,
        title: "دليل اليوم",
        theme: ThemeData(
            accentColor: Color.fromRGBO(44, 104, 119, 1.0),
            primaryColor: Color.fromRGBO(63, 99, 237, 1.0),
            fontFamily: "cairo"),
        home: Splash(
            // navigator: navigator,
            ),
      ),
    );
  }
}
